#include "EndpointModule.h"
#include "TouchController.h"

typedef enum {
  TRANSMISSION_CMD_BASIC_SET,
  TRANSMISSION_CMD_START_LVL_CHANGE,
  TRANSMISSION_CMD_DIMMER_START_LVL_CHANGE,
  TRANSMISSION_CMD_STOP_LVL_CHANGE,
  TRANSMISSION_CMD_CENTRAL_SCENE,
  TRANSMISSION_CMD_START_STOP_LVL_CHANGE
} TransmissionCmd_t;

extern void wc_prepareAGITransmission(touchButton_t btn, EndpointInfo_t* ep,TransmissionCmd_t cmd);
