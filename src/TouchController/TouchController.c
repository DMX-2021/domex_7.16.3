#include "cpt212b.h"
#include "TouchController.h"
#include "config_app.h"
#include "SwTimer.h"
#include "AppTimer.h"
#include "EndpointModule.h"

#define TIMER_PERIOD                        (50)

//! Button event durations (milliseconds)
#define TOUCH_BTN_SHORT_PRESS_TIME          (350)
#define TOUCH_BTN_BSHORT_PRESS_TIME          (550)
#define TOUCH_BTN_SHORT_HOLD_TIME           (1750)
#define TOUCH_BTN_MEDIUM_HOLD_TIME          (5000)
#define TOUCH_BTN_LONG_HOLD_TIME            (15000)

typedef enum
{
  TOUCH_BTN_RELEASE_STATE,
  TOUCH_BTN_PUSH_STATE,
  TOUCH_BTN_SHORT_PRESS_STATE,
  TOUCH_BTN_BSHORT_PRESS_STATE,
  TOUCH_BTN_SHORT_HOLD_STATE,
  TOUCH_BTN_MEDIUM_HOLD_STATE,
  TOUCH_BTN_LONG_HOLD_STATE
} touchButtonState_t;


typedef struct
{
  touchButtonState_t state;
  uint32_t pushTime;
} touchButtonHandle;

//! Button timer's variables
static SSwTimer buttonTimerHandle = {0};

static touchButtonHandle buttonList[(char)TOUCH_BUTTON_MAX] = { 0 };
static volatile uint32_t buttonTimerValue = 0;

static touchButtonHandler_t buttonHandler = NULL;
static void* buttonHandlerArgs = NULL;

static touchProximityHandler_t proximityHandler = NULL;
static void* proximityHandlerArgs = NULL;

void executeButtonHandler(touchButton_t btn, touchButtonEvent_t evt, void* args) {
  if (buttonHandler)
    buttonHandler(btn, evt, args);
}

void vButtonTimerCallback(SSwTimer* pTimer)
{
  UNUSED(pTimer);
  bool allButtonsReleased = true;
  touchButtonHandle *btn;
//  touchButtonHandle *Tempbtn;

  buttonTimerValue += TIMER_PERIOD;    //! Increase timer value

  for (int buttonNum = TOUCH_BUTTON_1; buttonNum < TOUCH_BUTTON_MAX; ++buttonNum) {
    btn = &buttonList[buttonNum];

    switch (btn->state) {
      case TOUCH_BTN_PUSH_STATE:
        if (TOUCH_BTN_SHORT_PRESS_TIME <= (buttonTimerValue - btn->pushTime)) {
          btn->state = TOUCH_BTN_SHORT_PRESS_STATE;
          executeButtonHandler(buttonNum, TOUCH_BTN_SHORT_PRESS_EVENT, buttonHandlerArgs);
        }
        break;
        //shay
      case TOUCH_BTN_SHORT_PRESS_STATE:
        if ( TOUCH_BTN_BSHORT_PRESS_TIME  <= (buttonTimerValue - btn->pushTime)) {
          btn->state = TOUCH_BTN_BSHORT_PRESS_STATE;
          executeButtonHandler(buttonNum, TOUCH_BTN_BSHORT_PRESS_EVENT, buttonHandlerArgs);
        }
        break;
        //shay
      case TOUCH_BTN_BSHORT_PRESS_STATE:
              if ( TOUCH_BTN_SHORT_HOLD_TIME<= (buttonTimerValue - btn->pushTime)) {
                btn->state = TOUCH_BTN_SHORT_HOLD_STATE;
                executeButtonHandler(buttonNum, TOUCH_BTN_SHORT_HOLD_EVENT, buttonHandlerArgs);
              }
        break;
      case TOUCH_BTN_SHORT_HOLD_STATE:
        if (TOUCH_BTN_MEDIUM_HOLD_TIME <= (buttonTimerValue - btn->pushTime)) {
          btn->state = TOUCH_BTN_MEDIUM_HOLD_STATE;
          executeButtonHandler(buttonNum, TOUCH_BTN_MEDIUM_HOLD_EVENT, buttonHandlerArgs);
        }
        break;

      case TOUCH_BTN_MEDIUM_HOLD_STATE:
        if (TOUCH_BTN_LONG_HOLD_TIME <= (buttonTimerValue - btn->pushTime)) {
          btn->state = TOUCH_BTN_LONG_HOLD_STATE;
          executeButtonHandler(buttonNum, TOUCH_BTN_LONG_HOLD_EVENT, buttonHandlerArgs);
        }
        break;

      case TOUCH_BTN_RELEASE_STATE:
        break;
      default:
        //! Nothing to do here
        break;
    }

    //! Checks if all buttons has been released
    if (btn->state != TOUCH_BTN_RELEASE_STATE)
      allButtonsReleased = false;
  }

  /** There's no need to keep the button timer running if all
   *  buttons has been released
   */
  if (allButtonsReleased) {
//    TimerStop(&buttonTimerHandle);
    buttonTimerValue = 0;
  }
}

static touchButton_t csIndexToTouchButton(cpt212b_cs_index_t csIndex)
{
  switch (csIndex) {
    case CS_INDEX_BUTTON_1: return TOUCH_BUTTON_1;
    case CS_INDEX_BUTTON_2: return TOUCH_BUTTON_2;
    case CS_INDEX_BUTTON_3: return TOUCH_BUTTON_3;
    case CS_INDEX_BUTTON_4: return TOUCH_BUTTON_4;
    case CS_INDEX_BUTTON_5: return TOUCH_BUTTON_5;
    case CS_INDEX_BUTTON_6: return TOUCH_BUTTON_6;
    case CS_INDEX_BUTTON_7: return TOUCH_BUTTON_7;
    case CS_INDEX_BUTTON_8: return TOUCH_BUTTON_8;
    case CS_INDEX_BUTTON_9: return TOUCH_BUTTON_9;
    default:
      return TOUCH_BUTTON_NONE;
  }
}

/**
 * @brief Handler of events from cpt212b low level driver
 *
 * @param evt - Event from cpt212b
 */
void cpt212bHandler(cpt212b_evt_t *evt)
{
  if (evt->event == ERROR_EVENT) {
    return;
  }

  touchButton_t button = csIndexToTouchButton(evt->data.cs_index);
  if (button == TOUCH_BUTTON_NONE || button >= TOUCH_BUTTON_MAX)
    return;

  touchButtonHandle *buttonHandle;

  for (char ScanButton=(char)TOUCH_BUTTON_1;ScanButton<(char)TOUCH_BUTTON_MAX;ScanButton++)
    {
      buttonHandle =  &buttonList[ScanButton];
      buttonHandle->state = TOUCH_BTN_RELEASE_STATE;
//      executeButtonHandler(ScanButton, TOUCH_BTN_RELEASE_EVENT, buttonHandlerArgs);
    }
  buttonHandle= &buttonList[button];

  switch (evt->event) {
    case TOUCH_EVENT:
      //! Starts timer if it was stopped
      if (TimerIsActive(&buttonTimerHandle) == false)
        if (TimerStart(&buttonTimerHandle, TIMER_PERIOD) == ESWTIMER_STATUS_FAILED) {
        }

      buttonHandle->pushTime = buttonTimerValue;

//      if (buttonHandle->state != TOUCH_BTN_RELEASE_STATE)
//        break;

      executeButtonHandler(button, TOUCH_BTN_PUSH_EVENT, buttonHandlerArgs);
      buttonHandle->state = TOUCH_BTN_PUSH_STATE;
      break;

    case RELEASE_EVENT:
      buttonTimerValue = 0;
      executeButtonHandler(button, TOUCH_BTN_RELEASE_EVENT, buttonHandlerArgs);
      break;

    case PROXIMITY_EVENT:
      if (proximityHandler)
        proximityHandler(proximityHandlerArgs);
      break;

    default:
      break;
  }
}

void Touch_RegisterButtonHandler(touchButtonHandler_t handler, void *args)
{
  static bool isTimerCreated = false;

  if (!handler) {
    return;
  }

  if (!isTimerCreated) {
    isTimerCreated = AppTimerRegister(&buttonTimerHandle, true, vButtonTimerCallback);
    cpt212b_reg_evt_handler(cpt212bHandler);
  }

  buttonHandler = handler;
  buttonHandlerArgs = args;
}

/**
 * @brief Register handler for proximity event
 *
 * @param handler - pointer to handler
 * @param args - arguments
 */
void Touch_RegisterProximityHandler(touchProximityHandler_t handler, void *args)
{
  if (!handler) {
    return;
  }

  proximityHandler = handler;
  proximityHandlerArgs = args;
  cpt212b_reg_evt_handler(cpt212bHandler);
}

unsigned short Touch_Button2CSmask(touchButton_t button)
{
  switch (button) {
    case TOUCH_BUTTON_1: return (1 << CS_INDEX_BUTTON_1);
    case TOUCH_BUTTON_2: return (1 << CS_INDEX_BUTTON_2);
    case TOUCH_BUTTON_3: return (1 << CS_INDEX_BUTTON_3);
    case TOUCH_BUTTON_4: return (1 << CS_INDEX_BUTTON_4);
    case TOUCH_BUTTON_5: return (1 << CS_INDEX_BUTTON_5);
    case TOUCH_BUTTON_6: return (1 << CS_INDEX_BUTTON_6);
    case TOUCH_BUTTON_7: return (1 << CS_INDEX_BUTTON_7);
    case TOUCH_BUTTON_8: return (1 << CS_INDEX_BUTTON_8);
    case TOUCH_BUTTON_9: return (1 << CS_INDEX_BUTTON_9);
    default: return 0;
  }
}

