#ifndef TOUCH_CONTROLLER_H
#define TOUCH_CONTROLLER_H

typedef enum {
  TOUCH_BUTTON_NONE = 0,
  TOUCH_BUTTON_1,
  TOUCH_BUTTON_2,
  TOUCH_BUTTON_3,
  TOUCH_BUTTON_4,
  TOUCH_BUTTON_5,
  TOUCH_BUTTON_6,
  TOUCH_BUTTON_7,
  TOUCH_BUTTON_8,
  TOUCH_BUTTON_9,
  TOUCH_BUTTON_MAX
} touchButton_t;

typedef enum
{
  TOUCH_BTN_PUSH_EVENT,
  TOUCH_BTN_RELEASE_EVENT,
  TOUCH_BTN_SHORT_PRESS_EVENT,
  TOUCH_BTN_BSHORT_PRESS_EVENT,
  TOUCH_BTN_LONG_PRESS_EVENT,
  TOUCH_BTN_SHORT_HOLD_EVENT,
  TOUCH_BTN_MEDIUM_HOLD_EVENT,
  TOUCH_BTN_LONG_HOLD_EVENT
} touchButtonEvent_t;


typedef struct {
  bool Modified;
  touchButton_t Button;
  touchButtonEvent_t Event;
  void* args;
} touchButtonStatus_t;

typedef void
(*touchButtonHandler_t)(touchButton_t btn, touchButtonEvent_t evt,
                             void* args);

typedef void
(*touchProximityHandler_t)(void* args);

/**
 * @brief Register handler for button events
 *
 * @param handler - pointer to handler
 * @param args - arguments
 */
void
Touch_RegisterButtonHandler(touchButtonHandler_t handler, void *args);

/**
 * @brief Register handler for proximity event
 *
 * @param handler - pointer to handler
 * @param args - arguments
 */
void
Touch_RegisterProximityHandler(touchProximityHandler_t handler, void *args);

/**
 * @brief Getting Touch controller CS mask by button number
 * @param button - button number, @see touchButton_t
 */
unsigned short
Touch_Button2CSmask(touchButton_t button);

touchButtonStatus_t Touch_getLastEvent();
#endif //! TOUCH_CONTROLLER_H
