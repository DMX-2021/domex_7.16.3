/**
 * @file i2c_driver.h
 * @brief I2C driver file
 *
 * @details This file contains definitions and basic functions for I2C
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#ifndef I2C_DRIVER_H
#define I2C_DRIVER_H

#include "stddef.h"
#include "sl_i2cspm.h"

#define I2C_ERR_CHECK(err_code)     do{                                 \
                                        if(err_code != i2cTransferDone) \
                                            return err_code;            \
                                    } while(0)

/**
 * @brief Initialization of I2C interface
 *
 * @param i2c_instance - Instance of I2C interface
 *
 */
void
i2c_init(I2CSPM_Init_TypeDef *i2c_instance);

/**
 * @brief I2C write from the buffer
 *
 * @param i2c_instance - Instance of I2C interface
 * @param slave_adr - Slave address
 * @param data - pointer to data buffer
 * @param data_len - data length
 * @return
 */
I2C_TransferReturn_TypeDef
i2c_write(I2CSPM_Init_TypeDef *i2c_instance, uint8_t slave_adr, uint8_t *data,
          uint8_t data_len);

/**
 * @brief I2C read into the buffer
 *
 * @param i2c_instance - Instance of I2C interface
 * @param slave_adr - Slave address
 * @param data - pointer to data buffer
 * @param data_len - data length
 * @return
 */
I2C_TransferReturn_TypeDef
i2c_read(I2CSPM_Init_TypeDef *i2c_instance, uint8_t slave_adr, uint8_t *data,
         uint8_t data_len);

/**
 * @brief I2C write to register from the buffer
 *
 * @param i2c_instance - Instance of I2C interface
 * @param slave_adr - Slave address
 * @param reg - Register address
 * @param data - pointer to data buffer
 * @param data_len - data length
 * @return
 */
I2C_TransferReturn_TypeDef
i2c_write_reg(I2CSPM_Init_TypeDef *i2c_instance, uint8_t slave_adr, uint8_t reg,
              uint8_t *data, uint8_t data_len);

/**
 * @brief I2C read from register into the buffer
 *
 * @param i2c_instance - Instance of I2C interface
 * @param slave_adr - Slave address
 * @param reg - Register address
 * @param data - pointer to data buffer
 * @param data_len - data length
 * @return
 */
I2C_TransferReturn_TypeDef
i2c_read_reg(I2CSPM_Init_TypeDef *i2c_instance, uint8_t slave_adr, uint8_t reg,
             uint8_t *data, uint8_t data_len);

#endif //!I2C_DRIVER_H
