/**
 * @file i2c_driver.c
 * @brief I2C driver file
 *
 * @details This file contains definitions and basic functions for I2C
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#include "i2c_driver.h"
#include "sl_i2cspm.h"
#include <string.h>

#define I2C_MAX_DATA_WRITE              (0x8)
#define I2C_MAX_DATA_READ               (0x8)

void
i2c_init(I2CSPM_Init_TypeDef *i2c_instance)
{
  I2CSPM_Init(i2c_instance);
  return;
}

I2C_TransferReturn_TypeDef
i2c_write(I2CSPM_Init_TypeDef *i2c_instance, uint8_t slave_adr, uint8_t *data,
          uint8_t data_len)
{
  if(i2c_instance == NULL)
    return i2cTransferUsageFault;

  I2C_TransferReturn_TypeDef ret;
  I2C_TransferSeq_TypeDef seq;

  /**
   * Note: 7 bit address - Use format AAAA AAAX
   *       A = Address bit, X = don't care bit (set to 0):
   */
  seq.addr = slave_adr;
  seq.addr &= ~0x01;
  seq.flags = I2C_FLAG_WRITE;

  seq.buf[0].data = data;
  seq.buf[0].len = data_len;
  seq.buf[1].data = NULL;
  seq.buf[1].len = 0;

  ret = I2CSPM_Transfer(i2c_instance->port, &seq);

  return ret;
}

I2C_TransferReturn_TypeDef
i2c_read(I2CSPM_Init_TypeDef *i2c_instance, uint8_t slave_adr, uint8_t *data,
         uint8_t data_len)
{
  if(i2c_instance == NULL)
    return i2cTransferUsageFault;

  I2C_TransferReturn_TypeDef ret;
  I2C_TransferSeq_TypeDef seq;

  /**
   * Note: 7 bit address - Use format AAAA AAAX
   *       A = Address bit, X = don't care bit (set to 0):
   */
  seq.addr = slave_adr;
  seq.addr &= ~0x01;
  seq.flags = I2C_FLAG_READ;

  seq.buf[0].data = data;
  seq.buf[0].len = data_len;
  seq.buf[1].data = NULL;
  seq.buf[1].len = 0;

  ret = I2CSPM_Transfer(i2c_instance->port, &seq);

  return ret;
}

I2C_TransferReturn_TypeDef
i2c_write_reg(I2CSPM_Init_TypeDef *i2c_instance, uint8_t slave_adr, uint8_t reg,
              uint8_t *data, uint8_t data_len)
{
  if(i2c_instance == NULL)
    return i2cTransferUsageFault;

  if(data_len > I2C_MAX_DATA_WRITE)
    return i2cTransferUsageFault;

  I2C_TransferReturn_TypeDef ret;
  I2C_TransferSeq_TypeDef seq;

  uint8_t write_data[I2C_MAX_DATA_WRITE] = { 0 };

  //! Register byte + payload length
  uint8_t write_data_len = data_len + 1;

  write_data[0] = reg;
  memcpy(&write_data[1], data, data_len);

  /**
   * Note: 7 bit address - Use format AAAA AAAX
   *       A = Address bit, X = don't care bit (set to 0):
   */
  seq.addr = slave_adr;
  seq.addr &= ~0x01;
  seq.flags = I2C_FLAG_WRITE_WRITE;

  seq.buf[0].data = write_data;
  seq.buf[0].len = write_data_len;

  seq.buf[1].data = NULL;
  seq.buf[1].len = 0;

  ret = I2CSPM_Transfer(i2c_instance->port, &seq);

  return ret;
}

I2C_TransferReturn_TypeDef
i2c_read_reg(I2CSPM_Init_TypeDef *i2c_instance, uint8_t slave_adr, uint8_t reg,
             uint8_t *data, uint8_t data_len)
{
  if(i2c_instance == NULL)
    return i2cTransferUsageFault;

  if(data_len > I2C_MAX_DATA_READ)
    return i2cTransferUsageFault;

  I2C_TransferReturn_TypeDef ret;
  I2C_TransferSeq_TypeDef seq;

  uint8_t write_data[] = { reg };
  uint8_t write_data_len = 0x01;

  /**
   * Note: 7 bit address - Use format AAAA AAAX
   *       A = Address bit, X = don't care bit (set to 0):
   */
  seq.addr = slave_adr;
  seq.addr &= ~0x01;
  seq.flags = I2C_FLAG_WRITE_READ;

  seq.buf[0].data = write_data;
  seq.buf[0].len = write_data_len;

  seq.buf[1].data = data;
  seq.buf[1].len = data_len;

  ret = I2CSPM_Transfer(i2c_instance->port, &seq);

  return ret;
}
