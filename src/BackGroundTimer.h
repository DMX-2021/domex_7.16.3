
#include "stdbool.h"

#define TIMER_PERIOD_MS                     (10)
#define MS_IN_SEC                           (1000)
#define MS_IN_TENTH_SEC                      (100)
#define SEC_IN_MIN                          (60)
#define TIMER_TIKS_TO_MS(x)                 (x*TIMER_PERIOD_MS)
#define TIMER_TIKS_TO_SEC(x)                (TIMER_TIKS_TO_MS(x) / MS_IN_SEC)
#define TIMER_TIKS_TO_MIN(x)                (TIMER_TIKS_TO_SEC(x) / SEC_IN_MIN)
#define MS_TO_TIMER_TIKS(x)                 (x/TIMER_PERIOD_MS)
#define SEC_TO_TIMER_TIKS(x)                (MS_TO_TIMER_TIKS(x*MS_IN_SEC))
#define TENTH_SEC_TO_TIMER_TIKS(x)          (MS_TO_TIMER_TIKS(x*MS_IN_TENTH_SEC))
#define MIN_TO_TIMER_TIKS(x)                (SEC_TO_TIMER_TIKS(x*SEC_IN_MIN))

/**
 * @brief Initialization of level change timer
 *
 * @return true - success
 *         false - failed
 */
bool
BackGroundTimer_TimerInit(void);

/**
 * @brief Starting level change timer
 *
 * @return true - success
 *         false - failed
 */
bool
BackGroundTimer_TimerStart(void);

