
#include <CC_Basic.h>
#include "EndpointModule.h"
#include "BoardDedicatedModule.h"
#include "zaf_event_helper.h"
#include "events.h"
#include "BackGroundTimer.h"
#include "CC_BinarySwitch.h"
#include "stddef.h"
#include "string.h"
/**
 * @details This function will be invoked when a Basic Get command is received
 *          See prototype for more information.
 *
 * @param endpoint - number of endpoint
 */

e_cmd_handler_return_code_t
CC_Basic_Set_handler(uint8_t val, uint8_t endpoint)
{
  e_cmd_handler_return_code_t ret = E_CMD_HANDLER_RETURN_CODE_NOT_SUPPORTED;
  BoardErr_t boardRet = BOARD_ERR;
  EndpointInfo_t* epInfo;
  BoilerConfig_t boilerConfig = DeviceConfiguration_Get_Boiler();
//  BlindsConfig_t blindsConfig;

  if (endpoint == ENDPOINT_ROOT) {
      goto exit;  //endpoint = ENDPOINT_1;
  }

  epInfo = EndpointInfo_Get_byNumber(endpoint);
  if (epInfo == NULL) {
    goto exit;
  }

  //! Refresh MMI any case
  ZAF_EventHelperEventEnqueue(EVENT_APP_REFRESH_MMI);

  switch (epInfo->config.EpMode) {
//    case EP_MODE_REMOTE_SWITCH: //! switch (epMode)
//      ret = CC_Remote_Switch_handler(val, endpoint);
//      break;

    case EP_MODE_DIMMER_SWITCH: //! switch (epMode)
//      if (val>99) val=99;
//      CC_MultilevelSwitch_Set_DimmerSwitch(val, 1,epInfo,false);
    break;

    case EP_MODE_BLINDS_SWITCH: //! switch (epMode)
//      blindsConfig = DeviceConfiguration_Get_Blinds(epInfo->number);
//      CC_MultilevelSwitch_Set_BlindsSwitch(val, (val) ? blindsConfig.durationUpSec : blindsConfig.durationDownSec,endpoint,false);
    break;

    case EP_MODE_RELAY_SWITCH: //! switch (epMode)
      ret = CommandClassBinarySwitchSupportSet(val, 0, endpoint);
      break;

    case EP_MODE_BOILER_SWITCH: //! switch (epMode)
      if (val == 0)
        {
          epInfo->boilerStatus.state = SWITCH_STATE_OFF;
          epInfo->boilerStatus.isTimerActive = false;
          epInfo->boilerStatus.tiksCount = 0;
          boardRet = Board_BoilerDeactivate(epInfo->config.Output1,
                                          epInfo->config.Output2);
          if (boardRet == BOARD_OK)
            ret = E_CMD_HANDLER_RETURN_CODE_HANDLED;
        }
      else
        {
          if (val > boilerConfig.maxBoilingTime)
            {
              val = boilerConfig.maxBoilingTime;
              epInfo->boilerStatus.state = SWITCH_STATE_ON;
              boardRet = Board_BoilerActivate(epInfo->config.Output1,
                                            epInfo->config.Output2);
              if (boardRet == BOARD_OK)
                ret = E_CMD_HANDLER_RETURN_CODE_HANDLED;
            }
          else
            {
              epInfo->boilerStatus.state = SWITCH_STATE_ON;
              epInfo->boilerStatus.isTimerActive = true;
              epInfo->boilerStatus.tiksCount = MIN_TO_TIMER_TIKS(val);
              boardRet = Board_BoilerActivate(epInfo->config.Output1,
                                                epInfo->config.Output2);
              BackGroundTimer_TimerStart();

             if (boardRet == BOARD_OK)
                ret = E_CMD_HANDLER_RETURN_CODE_HANDLED;
            }
        }
      break;
    case EP_MODE_FAN_SWITCH: //! switch (epMode)
      switch (val)
        {
            case FAN_ON_OFF_BUTTON:
              epInfo->fanStatus.state = FAN_STATE_OFF;
              boardRet = Board_FanActivate(epInfo->config.Output1,epInfo->config.Output2,
                                           RELAY_STATE_INACTIVE,RELAY_STATE_INACTIVE);
              if (boardRet == BOARD_OK)
                ret = E_CMD_HANDLER_RETURN_CODE_HANDLED;
            break;
            case FAN_LOW_BUTTON:
              epInfo->fanStatus.state = FAN_STATE_LOW;
              boardRet = Board_FanActivate(epInfo->config.Output1,epInfo->config.Output2,
                                           RELAY_STATE_ACTIVE,RELAY_STATE_INACTIVE);
              if (boardRet == BOARD_OK)
                ret = E_CMD_HANDLER_RETURN_CODE_HANDLED;
            break;
            case FAN_MID_BUTTON:
              epInfo->fanStatus.state = FAN_STATE_MID;
              boardRet = Board_FanActivate(epInfo->config.Output1,epInfo->config.Output2,
                                           RELAY_STATE_INACTIVE,RELAY_STATE_ACTIVE);
              if (boardRet == BOARD_OK)
                ret = E_CMD_HANDLER_RETURN_CODE_HANDLED;
            break;
            case FAN_HIGH_BUTTON:
              boardRet = Board_FanActivate(epInfo->config.Output1,epInfo->config.Output2,
                                           RELAY_STATE_ACTIVE,RELAY_STATE_ACTIVE);
              if (boardRet == BOARD_OK)
                ret = E_CMD_HANDLER_RETURN_CODE_HANDLED;
             epInfo->fanStatus.state = FAN_STATE_HIGH;
            break;
        }
        EndpointConfig_t EpConfig = { 0 };
        DeviceConfiguration_Get_Ep(epInfo->number, &EpConfig);
        memcpy(&EpConfig.OffOnState, &val,1);
        DeviceConfiguration_Set_Ep(epInfo->number, EpConfig,false);
        DeviceConfiguration_Commit();
      break;

    default: //! switch (epMode)
      break;
  }

  exit: return ret;
}
/**
 * @details This function will be invoked when a Basic Get command is received
 *          See prototype for more information.
 *
 * @param endpoint - number of endpoint
 * @return Current value
 */
uint8_t
CC_Basic_GetCurrentValue_handler(uint8_t endpoint)
{
  uint8_t ret = 0;
  EndpointInfo_t* epInfo = NULL;
  EpMode_t epMode = { 0 };

  if(endpoint == ENDPOINT_ROOT) {
      goto exit;  //endpoint = ENDPOINT_1;
  }

  epInfo = EndpointInfo_Get_byNumber(endpoint);
  if(epInfo == NULL) {
    goto exit;
  }
  epMode = epInfo->config.EpMode;

  if(epMode == EP_MODE_DIMMER_SWITCH || epMode == EP_MODE_BLINDS_SWITCH) {
//     ret = CC_MultilevelSwitch_Get_CurrentValue(endpoint);
  }
  else if(epMode == EP_MODE_RELAY_SWITCH) {
    ret = appBinarySwitchGetCurrentValue(endpoint);
  }

  exit: return ret;
}
/**
 * @details This function will be invoked when a Basic Get command is received
 *          See prototype for more information.
 *
 * @param endpoint - number of endpoint
 * @return Target value
 */
uint8_t
CC_Basic_GetTargetValue_handler(uint8_t endpoint)
{
  uint8_t ret = 0;
  EndpointInfo_t* epInfo = NULL;
  EpMode_t epMode = { 0 };

  if(endpoint == ENDPOINT_ROOT) {
      goto exit;  //endpoint = ENDPOINT_1;
  }

  epInfo = EndpointInfo_Get_byNumber(endpoint);

  if(epInfo == NULL) {
    goto exit;
  }

  epMode = epInfo->config.EpMode;

  if(epMode == EP_MODE_DIMMER_SWITCH || epMode == EP_MODE_BLINDS_SWITCH) {
//       ret = CC_MultilevelSwitch_Get_TargetValue(endpoint);
  }
  else if(epMode == EP_MODE_RELAY_SWITCH) {
    ret = appBinarySwitchGetTargetValue(endpoint);
  }

  exit: return ret;
}
/**
 * @details This function will be invoked when a Basic Get command is received
 *          See prototype for more information
 *
 * @param endpoint - number of endpoint
 * @return Duration
 */
uint8_t
CC_Basic_GetDuration_handler(uint8_t endpoint)
{
  uint8_t ret = 0;
  EndpointInfo_t* epInfo = NULL;
  EpMode_t epMode = { 0 };

  if(endpoint == ENDPOINT_ROOT) {
      goto exit;  //endpoint = ENDPOINT_1;
  }

  epInfo = EndpointInfo_Get_byNumber(endpoint);

  if(epInfo == NULL) {
    goto exit;
  }

  epMode = epInfo->config.EpMode;

  if(epMode == EP_MODE_DIMMER_SWITCH || epMode == EP_MODE_BLINDS_SWITCH) {
//       ret = CC_MultilevelSwitch_Get_Duration(endpoint);
  }
  else if(epMode == EP_MODE_RELAY_SWITCH) {
    ret = appBinarySwitchGetDuration(endpoint);
  }

  exit: return ret;
}
/**
 * @details This function is used to return a pointer for input data for
 *          the TSE trigger
 *
 * @param txOptions - pointer that will be included in the ZAF_TSE input data.
 * @param pData - application specific data
 */
void
CC_Basic_report_stx(TRANSMIT_OPTIONS_TYPE_SINGLE_EX txOptions, void* pData)
{
  s_zaf_tse_data_input_template_t* pDataInput =
      (s_zaf_tse_data_input_template_t*) (pData);
  RECEIVE_OPTIONS_TYPE_EX RxOptions = pDataInput->rxOptions;
  uint8_t endpoint = RxOptions.destNode.endpoint;
  EndpointInfo_t* epInfo = NULL;
  EpMode_t epMode = { 0 };

  if(endpoint == ENDPOINT_ROOT) {
      goto exit;  //endpoint = ENDPOINT_1;
  }

  epInfo = EndpointInfo_Get_byNumber(endpoint);

  if(epInfo == NULL) {
    goto exit;
  }

  epMode = epInfo->config.EpMode;

  if(epMode == EP_MODE_DIMMER_SWITCH || epMode == EP_MODE_BLINDS_SWITCH) {
      //CC_MultilevelSwitch_report_stx(txOptions, pData);
  }
  else if(epMode == EP_MODE_RELAY_SWITCH) {
    CC_BinarySwitch_report_stx(txOptions, pData);
  }

  exit: return;
}
/**
 * @details This function is used to return a pointer for input data
 *          for the TSE trigger
 *
 * @param rxOption - pointer that will be included in the ZAF_TSE input data
 * @return A pointer for input data for the TSE trigger
 */
void*
CC_Basic_prepare_zaf_tse_data(RECEIVE_OPTIONS_TYPE_EX* pRxOpt)
{
  void *ret = NULL;
  uint8_t endpoint = pRxOpt->destNode.endpoint;
  EndpointInfo_t* epInfo = NULL;

  if(endpoint == ENDPOINT_ROOT) {
      goto exit;  //endpoint = ENDPOINT_1;
  }

  epInfo = EndpointInfo_Get_byNumber(endpoint);

  if(epInfo == NULL) {
    goto exit;
  }

  switch (epInfo->config.EpMode) {
    case EP_MODE_DIMMER_SWITCH:
    case EP_MODE_BLINDS_SWITCH:
//       ret = CC_MultilevelSwitch_prepare_zaf_tse_data(pRxOpt);
      break;

    case EP_MODE_RELAY_SWITCH:
      ret = CC_BinarySwitch_prepare_zaf_tse_data(pRxOpt);
      break;

    default:
      break;
  }

  exit: return ret;
}
/**
 * @details This function is used to notify the Application that the
 *          CC Basic Set status is in a WORKING state. The application can
 *          subsequently make the TSE Trigger using the information passed
 *          on from the rxOptions.
 *
 * @param rxOption - pointer used to when triggering the "working state"
 */
void
CC_Basic_report_notifyWorking(RECEIVE_OPTIONS_TYPE_EX* pRxOpt)
{
  uint8_t endpoint = pRxOpt->destNode.endpoint;
  EndpointInfo_t* epInfo = NULL;
  EpMode_t epMode = { 0 };

  if(endpoint == ENDPOINT_ROOT) {
      goto exit;  //endpoint = ENDPOINT_1;
  }

  epInfo = EndpointInfo_Get_byNumber(endpoint);
  epMode = epInfo->config.EpMode;

  if(epInfo == NULL) {
    goto exit;
  }

  /**
   * "Working state" can happen for DIMMER_SWITCH or BLINDS_SWITCH only,
   *  when level changing takes some time to finish
   */
  if(epMode == EP_MODE_DIMMER_SWITCH || epMode == EP_MODE_BLINDS_SWITCH) {
      //TODO CC_MultilevelSwitch_report_notifyWorking(pRxOpt, CC_MultilevelSwitch_Get_DefaultDuraion(true, endpoint));
  }

  exit: return;
}




