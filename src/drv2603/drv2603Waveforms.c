/**
 * @file drv2603Waveforms.c
 *
 * @details This file contains presets of waveforms for DRV2603
 *          Haptic Driver
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#include "drv2603Waveforms.h"
#include "SizeOf.h"

static DRV2603_WaveformData_t clickData[] = {
    { 0xFF, 80 },
    { 0x00, 20 },
    { 0xFF, 80 },
    { 0x00, 20 }
};

DRV2603_Waveform_t clickWaveform = {
    .pData = clickData,
    .length = sizeof_array(clickData)
};

static DRV2603_WaveformData_t softClickData[] = {
    { 0xC0, 60 }
};

DRV2603_Waveform_t softClickWaveform = {
    .pData = softClickData,
    .length = sizeof_array(softClickData)
};
/*
static DRV2603_WaveformData_t doubleClickData[] = {
    { 0xFF, 60 },
    { 0xB4, 30 },
    { 0x00, 20 },
    { 0x80, 80 },
    { 0xFF, 70 },
    { 0xB4, 20 },
    { 0x00, 20 }
};
*/
static DRV2603_WaveformData_t doubleClickData[] = {
    { 0xFF, 150 },
    { 0x00, 50 },
    { 0xFF, 150 },
    { 0x00, 50 },
    { 0xFF, 150 },
    { 0x00, 50 },
    { 0xFF, 150 }
};

DRV2603_Waveform_t doubleClickWaveform = {
    .pData = doubleClickData,
    .length = sizeof_array(doubleClickData)
};

static DRV2603_WaveformData_t alertData[] = {
    { 0xFF, 30 },
    { 0xB4, 1000 },
    { 0x00, 20 }
};

DRV2603_Waveform_t alertWaveform = {
    .pData = alertData,
    .length = sizeof_array(alertData)
};

static DRV2603_WaveformData_t rampupData[] = {
    { 0x90, 200 },
    { 0xA0, 200 },
    { 0xB0, 100 },
    { 0xC0, 50 },
    { 0xFF, 40 },
    { 0x00, 20 }
};

DRV2603_Waveform_t rampupWaveform = {
    .pData = rampupData,
    .length = sizeof_array(rampupData)
};

static DRV2603_WaveformData_t rampdownData[] = {
    { 0xFF, 40 },
    { 0xC0, 50 },
    { 0xB0, 100 },
    { 0xA0, 200 },
    { 0x90, 200 },
    { 0x00, 20 }
};

DRV2603_Waveform_t rampdownWaveform = {
    .pData = rampdownData,
    .length = sizeof_array(rampdownData)
};

static DRV2603_WaveformData_t bumpData[] = {
    { 0xFF, 50 },
    { 0xB4, 70 },
    { 0x00, 20 }
};

DRV2603_Waveform_t bumpWaveform = {
    .pData = bumpData,
    .length = sizeof_array(bumpData)
};
