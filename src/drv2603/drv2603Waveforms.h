/**
 * @file drv2603Waveforms.h
 *
 * @details This file contains presets of waveforms for DRV2603
 *          Haptic Driver
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#ifndef DRV2603_WAVEFORMS_H
#define DRV2603_WAVEFORMS_H

#include "drv2603.h"

/**
 * List of redefined waveforms
 */
extern DRV2603_Waveform_t clickWaveform;
extern DRV2603_Waveform_t softClickWaveform;
extern DRV2603_Waveform_t doubleClickWaveform;
extern DRV2603_Waveform_t alertWaveform;
extern DRV2603_Waveform_t rampupWaveform;
extern DRV2603_Waveform_t rampdownWaveform;
extern DRV2603_Waveform_t bumpWaveform;

#endif //! DRV2603_WAVEFORMS_H
