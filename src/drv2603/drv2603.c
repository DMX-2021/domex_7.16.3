#include "drv2603.h"
#include "em_timer.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "SwTimer.h"
#include "AppTimer.h"
#include "ConfigurationModule.h"
#include "MMI.h"

#define DRV2603_ACTIVE_LVL                  1
#define DRV2603_TIMER_PERIOD                10

#define PWM_ON_VALUE                        1
#define PWM_TIMIER_CH                       1

#define TIMER_ID                            TIMER1
#define TIMER_CLK                           cmuClock_TIMER1
#define TIMER_TOP_VALUE                     0xFF

#define GPIO_ID(port, pin) ((port)*16 + (pin))

static SSwTimer drv2603TimerHandle = {0};

static GPIO_Port_TypeDef _enPort = 0;
static unsigned int _enPin = 0;

static GPIO_Port_TypeDef _pwmPort = 0;
static unsigned int _pwmPin = 0;

static DRV2603_Waveform_t currentWaveform;
static uint8_t dataIndex = 0;
static uint32_t msCounter = 0;
char Vibrate_Stopped=true;

bool m_hapticEnabled = false;
/**
 * @brief Lookup TIM1_CCx location for a GPIO pin
 *
 * @details Used to configure PWM
 * @note This function might be overkill. To save a few bytes (and loosing a
 *       little bit of flexibility) the values can naturally be hardcoded in
 *       e.g. DRV2603_Init().
 *
 * @param port     GPIO port
 * @param pin      GPIO pin
 * @param chan_num Channel number.
 *
 * @return TIM1_CCx location value for the specified GPIO.
 */
static uint32_t
GetTim1CcLocation(GPIO_Port_TypeDef port, uint32_t pin, uint32_t chan_num)
{
  uint32_t loc = 0;

  /** Lookup the TIM1_CC0 location for the GPIO
   * (See TIM1_CC0 in table 6.7 in efr32fg13 or efr32fg14 datasheet)
   */
  switch (GPIO_ID(port, pin))
  {
    case GPIO_ID(gpioPortA, 0): loc = 0; break;
    case GPIO_ID(gpioPortA, 1): loc = 1; break;
    case GPIO_ID(gpioPortA, 2): loc = 2; break;
    case GPIO_ID(gpioPortA, 3): loc = 3; break;
    case GPIO_ID(gpioPortA, 4): loc = 4; break;
    case GPIO_ID(gpioPortA, 5): loc = 5; break;

    case GPIO_ID(gpioPortB, 11): loc = 6; break;
    case GPIO_ID(gpioPortB, 12): loc = 7; break;
    case GPIO_ID(gpioPortB, 13): loc = 8; break;
    case GPIO_ID(gpioPortB, 14): loc = 9; break;
    case GPIO_ID(gpioPortB, 15): loc = 10; break;

    case GPIO_ID(gpioPortC, 6): loc = 11; break;
    case GPIO_ID(gpioPortC, 7): loc = 12; break;
    case GPIO_ID(gpioPortC, 8): loc = 13; break;
    case GPIO_ID(gpioPortC, 9): loc = 14; break;
    case GPIO_ID(gpioPortC, 10): loc = 15; break;
    case GPIO_ID(gpioPortC, 11): loc = 16; break;

    case GPIO_ID(gpioPortD, 9): loc = 17; break;
    case GPIO_ID(gpioPortD, 10): loc = 18; break;
    case GPIO_ID(gpioPortD, 11): loc = 19; break;
    case GPIO_ID(gpioPortD, 12): loc = 20; break;
    case GPIO_ID(gpioPortD, 13): loc = 21; break;
    case GPIO_ID(gpioPortD, 14): loc = 22; break;
    case GPIO_ID(gpioPortD, 15): loc = 23; break;

    case GPIO_ID(gpioPortF, 0): loc = 24; break;
    case GPIO_ID(gpioPortF, 1): loc = 25; break;
    case GPIO_ID(gpioPortF, 2): loc = 26; break;
    case GPIO_ID(gpioPortF, 3): loc = 27; break;
    case GPIO_ID(gpioPortF, 4): loc = 28; break;
    case GPIO_ID(gpioPortF, 5): loc = 29; break;
    case GPIO_ID(gpioPortF, 6): loc = 30; break;
    case GPIO_ID(gpioPortF, 7): loc = 31; break;
    default:
      break;
  }

  /**
   * Generate the TIMER_ROUTELOC0_CC(chan_num)LOC_LOCxx
   * value from the location and channel number
   * */

  uint32_t loc_num = (loc + 32 - chan_num) % 32;
  uint32_t loc_bit_value = loc_num << (8 * chan_num);

  return loc_bit_value;
}

/**
 * @brief Software timer callback
 * @details This timer used for sending waveform control
 *
 * @param pTimer - pointer to SSwTimer object
 */
static void
vDRV2603TimerCallback(SSwTimer* pTimer)
{
  UNUSED(pTimer);
  DRV2603_WaveformData_t *pWaveformData = NULL;
  SW_Config_t SW_Config = DeviceConfiguration_Get_SW();

  if(dataIndex == currentWaveform.length) {
    //! Sent all data, its time to stop timer
    msCounter = 0;
    dataIndex = 0;
    TIMER_CompareBufSet(TIMER_ID, PWM_TIMIER_CH, 0);
    TimerStop(&drv2603TimerHandle);
    GPIO_PinModeSet(_enPort, _enPin, gpioModePushPull, !DRV2603_ACTIVE_LVL);
    Vibrate_Stopped = true;
    if ((SW_Config == SW_CONFIG_B)||(SW_Config == SW_CONFIG_F)) {
      Refresh_Boiler_MMI_Idle();
    } else {
      Refresh_MMI_Idle();
    }
  }
  else {
    pWaveformData = &currentWaveform.pData[dataIndex];
    if(msCounter < pWaveformData->time) {
      if(msCounter == 0) {
        //! Send new data
        TIMER_CompareBufSet(TIMER_ID, PWM_TIMIER_CH, pWaveformData->amplitude);
      }
      msCounter += DRV2603_TIMER_PERIOD;
    }
    else {
      //! Send next data
      dataIndex++;
      msCounter = 0;
    }
  }
}

/**
 * @brief Initialization of DRV2603 driver
 *
 * @param enPort - enable pin port
 * @param enPin - enable pin
 * @param pwmPort - pwm pin port
 * @param pwmPin - pwm pin
 * @return DRV2603_OK - success
 */


void DRV2603_IsEnabled(bool enabled) {
  m_hapticEnabled = enabled;
}

DRV2603_Err_t
DRV2603_Init(GPIO_Port_TypeDef enPort, unsigned int enPin,
             GPIO_Port_TypeDef pwmPort, unsigned int pwmPin)
{
  uint32_t location = 0;
  bool isTimerCreated = false;

  _enPort = enPort;
  _enPin = enPin;
  _pwmPort = pwmPort;
  _pwmPin = pwmPin;

  //! Disable driver
  GPIO_PinModeSet(_enPort, _enPin, gpioModePushPull, !DRV2603_ACTIVE_LVL);
  GPIO_PinModeSet(_pwmPort, _pwmPin, gpioModePushPullAlternate, 0);

  //! Enable timer clock
  CMU_ClockEnable(TIMER_CLK, true);

  TIMER_TopSet(TIMER_ID, TIMER_TOP_VALUE);

  //! Set the PWM duty cycle to zero for all channels
  TIMER_CompareBufSet(TIMER_ID, 0, 0);
  TIMER_CompareBufSet(TIMER_ID, 1, 0);
  TIMER_CompareBufSet(TIMER_ID, 2, 0);
  TIMER_CompareBufSet(TIMER_ID, 3, 0);

  //! Create a timerInit object, based on the API default
  TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;

  TIMER_Init(TIMER_ID, &timerInit);

  //! Create the timer count control object initializer
  TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;
  timerCCInit.mode      = timerCCModePWM;
  timerCCInit.cmoa      = timerOutputActionToggle;
  timerCCInit.outInvert = (1 == PWM_ON_VALUE) ? false : true;

  TIMER_InitCC(TIMER_ID, PWM_TIMIER_CH, &timerCCInit);
  location = GetTim1CcLocation(_pwmPort, _pwmPin, PWM_TIMIER_CH);

  //! Relocate timer to PWM pin
  TIMER_ID->ROUTELOC0 = (TIMER_ID->ROUTELOC0 & ~_TIMER_ROUTELOC0_CC1LOC_MASK) | location;

  //! Enable timer
  TIMER_ID->ROUTEPEN |= TIMER_ROUTEPEN_CC1PEN;

  //! Create software operation timer
  isTimerCreated = AppTimerRegister(&drv2603TimerHandle, true,
                                    vDRV2603TimerCallback);
  if(isTimerCreated)
    return DRV2603_OK;
  else
    return DRV2603_ERR;
}

/**
 * @brief Deinitialization of DRV2603 driver
 *
 * @return DRV2603_OK - success
 */
DRV2603_Err_t
DRV2603_Deinit(void)
{
  bool isTimerActive = false;

  //! Disable PWM
  TIMER_ID->ROUTEPEN &= ~TIMER_ROUTEPEN_CC1PEN;

  isTimerActive = TimerIsActive(&drv2603TimerHandle);
  if(isTimerActive) {
    TimerStop(&drv2603TimerHandle);
  }

  //! Disable DRV2603
  GPIO_PinModeSet(_enPort, _enPin, gpioModePushPull, !DRV2603_ACTIVE_LVL);
  return DRV2603_OK;
}

/**
 * @brief Sending waveform object
 *
 * @param waveform - waveform object
 * @return DRV2603_OK - success
 */
DRV2603_Err_t
DRV2603_SendWaveform(DRV2603_Waveform_t waveform)
{
  if (m_hapticEnabled == false)
    return DRV2603_OK;

  ESwTimerStatus ret;
  bool isTimerActive = false;
  currentWaveform = waveform;
  dataIndex = 0;
  msCounter = 0;

  isTimerActive = TimerIsActive(&drv2603TimerHandle);
  Vibrate_Stopped=false;
  if(isTimerActive) {
    //! Driver in sending mode, it should be stopped
    TimerStop(&drv2603TimerHandle);
    GPIO_PinModeSet(_enPort, _enPin, gpioModePushPull, !DRV2603_ACTIVE_LVL);
  }

  //! Start sending
  ret = TimerStart(&drv2603TimerHandle, DRV2603_TIMER_PERIOD);
  GPIO_PinModeSet(_enPort, _enPin, gpioModePushPull, DRV2603_ACTIVE_LVL);

  if(ret == ESWTIMER_STATUS_SUCCESS)
    return DRV2603_OK;
  else
    return DRV2603_ERR;
}
