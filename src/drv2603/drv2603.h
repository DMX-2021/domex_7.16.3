/**
 * @file drv2603.h
 * @brief DRV2603 driver source file
 *
 * @details This file contains definitions and basic functions for DRV2603
 *          Haptic Driver
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#ifndef DRV2603_H
#define DRV2603_H

#include "em_gpio.h"

/**
 * @brief DRV2603 error code
 */
typedef enum
{
  DRV2603_OK = 0,
  DRV2603_ERR = -1
} DRV2603_Err_t;

/**
 * @brief Waveform data
 */
typedef struct
{
  uint8_t amplitude; //! Vibration amplitude
  uint32_t time;     //! Time in ms for which the amplitude should be set
} DRV2603_WaveformData_t;

/**
 * @brief Waveform object
 */
typedef struct
{
  DRV2603_WaveformData_t *pData; //! Pointer to waveform data
  uint8_t length;                //! Count of waveform data
} DRV2603_Waveform_t;


/**
 * enable or disable haptic functionality
 * @param enabled
 */
void DRV2603_IsEnabled(bool enabled);
/**
 * @brief Initialization of DRV2603 driver
 *
 * @param enPort - enable pin port
 * @param enPin - enable pin
 * @param pwmPort - pwm pin port
 * @param pwmPin - pwm pin
 * @return DRV2603_OK - success
 */
DRV2603_Err_t
DRV2603_Init(GPIO_Port_TypeDef enPort, unsigned int enPin,
             GPIO_Port_TypeDef pwmPort, unsigned int pwmPin);

/**
 * @brief Deinitialization of DRV2603 driver
 *
 * @return DRV2603_OK - success
 */
DRV2603_Err_t
DRV2603_Deinit(void);

/**
 * @brief Sending waveform object
 *
 * @param waveform - waveform object
 * @return DRV2603_OK - success
 */
DRV2603_Err_t
DRV2603_SendWaveform(DRV2603_Waveform_t waveform);

#endif //! DRV2603_H
