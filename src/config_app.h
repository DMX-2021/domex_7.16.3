/**
 * @file
 * @brief Configuration file for Wall Controller sample application.
 * @details This file contains definitions for the Z-Wave+ Framework as well for the sample app.
 *
 * @copyright 2018 Silicon Laboratories Inc.
 */
#ifndef _CONFIG_APP_H_
#define _CONFIG_APP_H_

#include <ZW_product_id_enum.h>
#include <CC_ManufacturerSpecific.h>
#include "em_i2c.h"
#include "tca9555.h"

/****************************************************************************
 *
 * Application version, which is generated during release of SDK.
 * The application developer can freely alter the version numbers
 * according to his needs.
 *
 ****************************************************************************/
#define APP_VERSION           ZAF_VERSION_MAJOR
#define APP_REVISION          ZAF_VERSION_MINOR
#define APP_PATCH             ZAF_VERSION_PATCH

/**
 * Defines generic and specific device types for a Central Scene device type
 */
//@ [GENERIC_TYPE_ID]
#define GENERIC_TYPE          GENERIC_TYPE_WALL_CONTROLLER
#define SPECIFIC_TYPE         SPECIFIC_TYPE_NOT_USED
//@ [GENERIC_TYPE_ID]

/**
 * See ZW_basis_api.h for ApplicationNodeInformation field deviceOptionMask
 */
//@ [DEVICE_OPTIONS_MASK_ID]
#define DEVICE_OPTIONS_MASK   APPLICATION_NODEINFO_LISTENING
//@ [DEVICE_OPTIONS_MASK_ID]


/**
 * Defines used to initialize the Z-Wave Plus Info Command Class.
 */
//@ [APP_TYPE_ID]
//@ [APP_ROLE_TYPE]
#define APP_ROLE_TYPE         ZWAVEPLUS_INFO_REPORT_ROLE_TYPE_SLAVE_ALWAYS_ON
#define APP_NODE_TYPE         ZWAVEPLUS_INFO_REPORT_NODE_TYPE_ZWAVEPLUS_NODE
#define APP_ICON_TYPE         ICON_TYPE_GENERIC_WALL_CONTROLLER
#define APP_USER_ICON_TYPE    ICON_TYPE_GENERIC_WALL_CONTROLLER
//@ [APP_ROLE_TYPE]
//@ [APP_TYPE_ID]

/**
 * Defines used to initialize the Manufacturer Specific Command Class.
 */
#define APP_MANUFACTURER_ID   MFG_ID_ZWAVE
#define APP_PRODUCT_TYPE_ID   PRODUCT_TYPE_ID_ZWAVE_PLUS_V2
#define APP_PRODUCT_ID        PRODUCT_ID_WallController

#define APP_FIRMWARE_ID       APP_PRODUCT_ID | (APP_PRODUCT_TYPE_ID << 8)

/**
 * Defines used to initialize the Association Group Information (AGI)
 * Command Class.
 *
 * @attention
 * The sum of NUMBER_OF_ENDPOINTS, MAX_ASSOCIATION_GROUPS and MAX_ASSOCIATION_IN_GROUP
 * may not exceed 18 as defined by ASSOCIATION_ALLOCATION_MAX or an error will be thrown
 * during compilation.
 *
 * @attention
 * It is advised not to change the parameters once a product has been launched, as subsequent
 * upgrades will erase the old structure's content and start a fresh association table.
 */
#define NUMBER_OF_INDIVIDUAL_ENDPOINTS    9
#define NUMBER_OF_AGGREGATED_ENDPOINTS    0
#define NUMBER_OF_ENDPOINTS         NUMBER_OF_INDIVIDUAL_ENDPOINTS + NUMBER_OF_AGGREGATED_ENDPOINTS
#define MAX_ASSOCIATION_GROUPS      7 // Number of elements in AGITABLE_ROOTDEVICE_GROUPS plus one (for Lifeline)
#define MAX_ASSOCIATION_IN_GROUP    5

/*
 * File identifiers for application file system
 * Range: 0x00000 - 0x0FFFF
 */
#define FILE_ID_APPLICATIONDATA     (0x00000)

//@ [AGI_TABLE_ID]
//@ [AGITABLE_LIFELINE_GROUP]
#define AGITABLE_LIFELINE_GROUP \
 {COMMAND_CLASS_CENTRAL_SCENE_V3, CENTRAL_SCENE_NOTIFICATION_V3}, \
 {COMMAND_CLASS_CENTRAL_SCENE_V3, CENTRAL_SCENE_CONFIGURATION_REPORT_V3}, \
 {COMMAND_CLASS_DEVICE_RESET_LOCALLY, DEVICE_RESET_LOCALLY_NOTIFICATION}, \
 {COMMAND_CLASS_INDICATOR, INDICATOR_REPORT_V3}
//@ [AGITABLE_LIFELINE_GROUP]

//@ [AGITABLE_ROOTDEVICE_GROUPS]
#define  AGITABLE_ROOTDEVICE_GROUPS \
 { /* Group 2 */ \
   {ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL, ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL_KEY01}, \
   1, \
   {{COMMAND_CLASS_BASIC_V2, BASIC_SET_V2}}, \
   "BTN0" \
 }, \
 { /* Group 3 */ \
   {ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL, ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL_KEY01}, \
   2, \
   {{COMMAND_CLASS_SWITCH_MULTILEVEL_V4, SWITCH_MULTILEVEL_START_LEVEL_CHANGE_V4}, {COMMAND_CLASS_SWITCH_MULTILEVEL_V4, SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE_V4}}, \
   "BTN0" \
 }, \
 { /* Group 4 */ \
   {ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL, ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL_KEY02}, \
   1, \
   {{COMMAND_CLASS_BASIC_V2, BASIC_SET_V2}}, \
   "BTN2" \
 }, \
 { /* Group 5 */ \
   {ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL, ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL_KEY02}, \
   2, \
   {{COMMAND_CLASS_SWITCH_MULTILEVEL_V4, SWITCH_MULTILEVEL_START_LEVEL_CHANGE_V4}, {COMMAND_CLASS_SWITCH_MULTILEVEL_V4, SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE_V4}}, \
   "BTN2" \
 }, \
 { /* Group 6 */ \
   {ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL, ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL_KEY03}, \
   1, \
   {{COMMAND_CLASS_BASIC_V2, BASIC_SET_V2}}, \
   "BTN3" \
 }, \
 { /* Group 7 */ \
   {ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL, ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL_KEY03}, \
   2, \
   {{COMMAND_CLASS_SWITCH_MULTILEVEL_V4, SWITCH_MULTILEVEL_START_LEVEL_CHANGE_V4}, {COMMAND_CLASS_SWITCH_MULTILEVEL_V4, SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE_V4}}, \
   "BTN3" \
 }
//@ [AGITABLE_ROOTDEVICE_GROUPS]
//@ [AGI_TABLE_ID]

/**
 * Security keys
 */
//@ [REQUESTED_SECURITY_KEYS_ID]
#define REQUESTED_SECURITY_KEYS   (SECURITY_KEY_S0_BIT | SECURITY_KEY_S2_UNAUTHENTICATED_BIT | SECURITY_KEY_S2_AUTHENTICATED_BIT)
//@ [REQUESTED_SECURITY_KEYS_ID]

/**
 * Setup the Z-Wave frequency
 *
 * The definition is only set if it's not already set to make it possible to pass the frequency to
 * the compiler by command line or in the Studio project.
 */
#ifndef APP_FREQ
#define APP_FREQ              REGION_DEFAULT
#endif
#define UART0_TX_PORT             gpioPortD
#define UART0_TX_PIN              14
#define UART0_RX_PORT             gpioPortD
#define UART0_RX_PIN              11

#define CPT212B_INT_PORT          gpioPortC
#define CPT212B_INT_PIN           7
#define CPT212B_RESET_PORT        gpioPortF
#define CPT212B_RESET_PIN         4

#define LED_5V_ACTIVE_LVL         1
#define LED_5V_PORT               gpioPortC
#define LED_5V_PIN                9

#define WS2812_TX_LOC             _USART_ROUTELOC0_TXLOC_LOC7//! PB12
#define WS2812_RX_LOC             _USART_ROUTELOC0_RXLOC_LOC7//! PB13 not used
#define WS2812_CLK_LOC            _USART_ROUTELOC0_CLKLOC_LOC8//! PB15 not used
#define WS2812_CS_LOC             _USART_ROUTELOC0_CSLOC_LOC8//! PC6 not used

/** I2C interface configuration*/
#define I2C_PERIPHERAL_PORT       I2C0
#define I2C_SCL_PORT              gpioPortA
#define I2C_SCL_PIN               4
#define I2C_SDA_PORT              gpioPortA
#define I2C_SDA_PIN               3
#define I2C_SCL_LOC               3
#define I2C_SDA_LOC               3
#define I2C_REF_CLK               0 //! Currently configured reference clock
#define I2C_MAX_FREQ              I2C_FREQ_STANDARD_MAX //! Standard frequency for this platform
#define I2C_CLK_RATIO             i2cClockHLRStandard //! Standard ratio 4:4

#define TCA9555_I2C_ADR           A2L_A1L_A0L
#define TCA9555_INT_PORT          0xFF//! Interrupt not used
#define TCA9555_INT_PIN           0xFF//! Interrupt not used

#define BOARD_ADDRESS_DIMMER             0x01
#define BOARD_ADDRESS_LIGHT              0x02
#define BOARD_ADDRESS_WATER_HEATER       0x03

#define LIGHT_RELAY_OUT_1         GPIO_P04
#define LIGHT_RELAY_OUT_2         GPIO_P02
#define LIGHT_RELAY_OUT_3         GPIO_P05
#define LIGHT_RELAY_OUT_4         GPIO_P06
#define LIGHT_RELAY_OUT_5         GPIO_P07
#define LIGHT_RELAY_OUT_6         GPIO_P03

#define WATER_HEATER_RELAY_OUT_1  GPIO_P02
#define WATER_HEATER_RELAY_OUT_2  GPIO_P00
#define WATER_HEATER_RELAY_OUT_3  GPIO_P01

#define DIMMER_RESET              GPIO_P00
#define DIMMER_OUT_1              STM32_DIMMER_220V_CH1
#define DIMMER_OUT_2              STM32_DIMMER_220V_CH2
#define DIMMER_OUT_3              STM32_DIMMER_220V_CH3
#define DIMMER_OUT_4              STM32_DIMMER_220V_CH4
#define DIMMER_OUT_5              STM32_DIMMER_10V_CH1
#define DIMMER_OUT_6              STM32_DIMMER_10V_CH2

#define BUTTON_1_LED_NUM          (0x0)
#define BUTTON_2_LED_NUM          (0x5)
#define BUTTON_3_LED_NUM          (0x6)
#define BUTTON_4_LED_NUM          (0x1)
#define BUTTON_5_LED_NUM          (0x4)
#define BUTTON_6_LED_NUM          (0x7)
#define BUTTON_7_LED_NUM          (0x2)
#define BUTTON_8_LED_NUM          (0x3)
#define BUTTON_9_LED_NUM          (0x8)

#define BOILER_EP_NUMB            ENDPOINT_1
#define BOILER_TIME_BUTTON_COUNT  6
#define BOILER_GET_ONE_PART_OF_MAX(TIME_MINUT)  (TIME_MINUT / BOILER_TIME_BUTTON_COUNT)

#define BOILER_ON_OFF_BUTTON      (0x05)
#define BOILER_1_6TH_MIN_BUTTON   (0x07)
#define BOILER_2_6TH_MIN_BUTTON   (0x04)
#define BOILER_3_6TH_MIN_BUTTON   (0x01)
#define BOILER_4_6TH_MIN_BUTTON   (0x03)
#define BOILER_5_6TH_MIN_BUTTON   (0x06)
#define BOILER_6_6TH_MIN_BUTTON   (0x09)

#define FAN_EP_NUMB            ENDPOINT_1
#define FAN_ON_OFF_BUTTON         (0x09)
#define FAN_HIGH_BUTTON           (0x04)
#define FAN_MID_BUTTON            (0x05)
#define FAN_LOW_BUTTON            (0x06)

#define DEFAULT_MIN_DIMMING_VALUE        0
#define DEFAULT_MAX_DIMMING_VALUE        99
#define DEFAULT_DIMMING_TIME_FADE_UP     3
#define DEFAULT_DIMMING_TIME_FADE_DOWN   3
#define DEFAULT_DIMMING_TIME_FADE_MAX    127
#define DEFAULT_BLINDS_MIN_VALUE         0
#define DEFAULT_BLINDS_MAX_VALUE         99
#define DEFAULT_BLINDS_DURATION_UP       60
#define DEFAULT_BLINDS_DURATION_DOWN     60
#define DEFAULT_SHADE_DURATION_SHORT     5
#define DEFAULT_SHADE_DURATION_VERY_SHORT 30
#define DEFAULT_SHADE_DURATION_MIN        1
#define DEFAULT_SHADE_DURATION_MAX        100
#define DEFAULT_BLINDS_DURATION_MIN      1
#define DEFAULT_MAX_BOILING_TIME         90
#define DEFAULT_BUZZER_EN                1
#define DEFAULT_HAPTIC_EN                1
#define DEFAULT_RADIO_CHANNEL            REGION_IL
#define DEFAULT_CENTRAL_SCENE_ID_TOGGLE_OFF 1
#define DEFAULT_CENTRAL_SCENE_ID_TOGGLE_ON  255
#define DEFAULT_CENTRAL_SCENE_ID_MASTER_ALL_OFF           1
#define DEFAULT_CENTRAL_SCENE_ID_MASTER_ALL_ON            2
#define DEFAULT_CENTRAL_SCENE_MASTER_ALL_ONOFF_FUNC_MASK  0xFFFF

#define SYSTEM_BUTTON             (0x1)

/**
 * Configuration for CC Binary Switch
 */
#define SWITCH_BINARY_ENDPOINTS_MAX      6
#define SWITCH_BINARY_ENDPOINTS          SWITCH_BINARY_ENDPOINTS_MAX

/**
 * Configuration for CC Multilevel Switch Dimmer
 */
#define SWITCH_DIMMER_ENDPOINTS_MAX      6
#define SWITCH_DIMMER_ENDPOINTS          SWITCH_DIMMER_ENDPOINTS_MAX
#define SWITCH_DIMMER_SET_DURATION       30

/**
 * Configuration for CC Multilevel Switch Blinds
 */
#define SWITCH_BLINDS_ENDPOINTS_MAX      3
#define SWITCH_BLINDS_ENDPOINTS          SWITCH_BLINDS_ENDPOINTS_MAX

#define SWITCH_MULTI_ENDPOINTS           SWITCH_DIMMER_ENDPOINTS_MAX

#define CS_INDEX_BUTTON_1         CS_INDEX_0
#define CS_INDEX_BUTTON_2         CS_INDEX_10
#define CS_INDEX_BUTTON_3         CS_INDEX_9
#define CS_INDEX_BUTTON_4         CS_INDEX_1
#define CS_INDEX_BUTTON_5         CS_INDEX_8
#define CS_INDEX_BUTTON_6         CS_INDEX_6
#define CS_INDEX_BUTTON_7         CS_INDEX_3
#define CS_INDEX_BUTTON_8         CS_INDEX_4
#define CS_INDEX_BUTTON_9         CS_INDEX_5

#define DRV2603_EN_PORT           gpioPortF
#define DRV2603_EN_PIN            6
#define DRV2603_PWM_PORT          gpioPortF
#define DRV2603_PWM_PIN           7

#define LEARN_MODE_LED_BLINK_PERIOD      (300)
#define BLINDS_PROCESS_LED_BLINK_PERIOD  (400)
#define DIMMER_CALIBRATION_LED_BLINK_PERIOD  (200)
#define BOILER_REFRESH_MMI_PRERIOD       (1*1000)


#endif /* _CONFIG_APP_H_ */

