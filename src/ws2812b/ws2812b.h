/**
 * @file ws2812b.h
 * @brief WS2812B driver header file
 *
 * @details This file contains definitions and basic functions for WS2812B
 *          addressable LEDs
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#ifndef WS2812B_H
#define WS2812B_H

#include "stdint.h"

#define WS2812B_LED_COUNT                   (9)

/**
 * @brief Initialization ws2812 led strips
 *
 * @param txLoc - USART1 Tx location. Used for LEDs data transmit
 * @param rxLoc - USART1 Rx location. Not used
 * @param clkLoc - USART1 Clk location. Not used
 * @param csLoc - USART1 Cs location. Not used
 */
void
ws2812b_init(uint32_t txLoc, uint32_t rxLoc, uint32_t clkLoc, uint32_t csLoc);

/**
 * @brief Set LED color
 *
 * @param led_num - number of LED
 * @param red - red color value
 * @param green - green color value
 * @param blue - blue color value
 */
void
ws2812b_set_color(uint8_t led_num, uint8_t red, uint8_t green, uint8_t blue);

/**
 * @brief Update LEDs colors
 */
void
ws2812b_update(void);

void
ws2812b_Cyan_All(void );

void
ws2812b_Green_All(void );

void
ws2812b_Off_All(void );




#endif //! WS2812B_H
