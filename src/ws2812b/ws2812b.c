/**
 * @file ws2812b.c
 * @brief WS2812B driver source file
 *
 * @details This file contains definitions and basic functions for WS2812B
 *          addressable LEDs
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#include "ws2812b.h"
#include "string.h"
#include "spidrv.h"

#define RESET_OFFSET                        (10)

//! Color type of LEDs
#define WS2812B_RGB                         (1)
#define WS2812B_BGR                         (0)

//! LEDs configuration
#define WS2812B_BYTE_PER_COLOR              (3)
#define WS2812B_RGB_TYPE                    WS2812B_BGR

//! SPI bits relevant to WS2812B bits
#define WS2812B_HIGH_BIT                    (0b110)
#define WS2812B_LOW_BIT                     (0b100)

//! SPI configuration
#define SPI_PORT                            (USART1)
#define SPI_BIT_RATE                        (2600000)
#define SPI_FRAME_LENGTH                    (8)

//! LED color type
typedef uint8_t ws2812_color_t[WS2812B_BYTE_PER_COLOR];

//! LED type
typedef struct
{
#if (WS2812B_RGB_TYPE == WS2812B_RGB)
  ws2812_color_t red;
  ws2812_color_t green;
  ws2812_color_t blue;
#else
  ws2812_color_t green;
  ws2812_color_t red;
  ws2812_color_t blue;
#endif
} ws2812_led_t;

//! SPI handles
static SPIDRV_HandleData_t spiHandleData;
static SPIDRV_Handle_t spiHandle = &spiHandleData;

//! LED array
static ws2812_led_t data_buf[RESET_OFFSET+WS2812B_LED_COUNT+1];

/**
 * @brief SPI initialization
 */
static void
SPI_init(uint32_t txLoc, uint32_t rxLoc, uint32_t clkLoc, uint32_t csLoc)
{
  SPIDRV_Init_t initData = { 0 };
  initData.port = SPI_PORT;
  initData.portLocationTx = txLoc;
  initData.portLocationRx = rxLoc;
  initData.portLocationClk = clkLoc;
  initData.portLocationCs = csLoc;
  initData.bitRate = SPI_BIT_RATE;
  initData.frameLength = SPI_FRAME_LENGTH;
  initData.dummyTxValue = 0x00;
  initData.type = spidrvMaster;
  initData.bitOrder = spidrvBitOrderMsbFirst;
  initData.clockMode = spidrvClockMode0;
  initData.csControl = spidrvCsControlApplication;

  SPIDRV_Init(spiHandle, &initData);
}

/**
 * @brief SPI transmission
 *
 * @param numBytes
 * @param data
 */
static void
SPI_transmit(const uint8_t * data, size_t size)
{
  Ecode_t result;
  size_t remaining = size;

  /**
   * The spidrv is using dma to transfer bytes to the USART (spi)
   * this is why we must transmit bytes in chunks of at most
   * DMADRV_MAX_XFER_COUNT (1024 on EFM32GG990F1024)
   */
  do {
    int count = SL_MIN(remaining, DMADRV_MAX_XFER_COUNT);
    result = SPIDRV_MTransmitB(spiHandle, data, count);
    remaining -= count;
    data += count;
  }
  while((result == ECODE_EMDRV_SPIDRV_OK) && (remaining > 0));

}

/**
 * @brief Encode color info into SPI bits
 *
 * @param color - pointer to color array
 * @param value - color value
 */
static void
color_encode(ws2812_color_t color, uint8_t value)
{

  uint8_t temp;

  //! Clear color array
  memset(color, 0, sizeof(ws2812_color_t));

  //! Encode WS2812B bits into SPI bits
  for(uint32_t bit = 0; bit < 8; bit++) {
    if(value & (1 << bit)) {
      ((uint32_t*) color)[0] |= (WS2812B_HIGH_BIT << (bit * 3));
    }
    else {
      ((uint32_t*) color)[0] |= (WS2812B_LOW_BIT << (bit * 3));
    }
  }
  temp = color[0];
  color[0] = color[2];
  color[2] = temp;
}

/**
 * @brief Initialization ws2812 led strips
 *
 * @param txLoc - USART1 Tx location. Used for LEDs data transmit
 * @param rxLoc - USART1 Rx location. Not used
 * @param clkLoc - USART1 Clk location. Not used
 * @param csLoc - USART1 Cs location. Not used
 */
void
ws2812b_init(uint32_t txLoc, uint32_t rxLoc, uint32_t clkLoc, uint32_t csLoc)
{
  SPI_init(txLoc, rxLoc, clkLoc, csLoc);

  for(uint8_t led = 0; led < WS2812B_LED_COUNT; led++)
      ws2812b_set_color(led, 0, 0, 0);

  ws2812b_update();
}

void
ws2812b_Green_All(void )
{

  for(uint8_t led = 0; led < WS2812B_LED_COUNT; led++)
      ws2812b_set_color(led, 0, 255, 0);
  ws2812b_update();
}
void
ws2812b_Blue_All(void )
{

  for(uint8_t led = 0; led < WS2812B_LED_COUNT; led++)
      ws2812b_set_color(led, 0, 0, 255);
  ws2812b_update();
}
void
ws2812b_Cyan_All(void )
{

  for(uint8_t led = 0; led < WS2812B_LED_COUNT; led++)
      ws2812b_set_color(led, 0, 255, 255);
  ws2812b_update();
}
void
ws2812b_Off_All(void )
{

  for(uint8_t led = 0; led < WS2812B_LED_COUNT; led++)
      ws2812b_set_color(led, 0, 0, 0);
  ws2812b_update();
}

/**
 * @brief Set LED color
 *
 * @param led_num - number of LED
 * @param red - red color value
 * @param green - green color value
 * @param blue - blue color value
 */
void
ws2812b_set_color(uint8_t led_num, uint8_t red, uint8_t green, uint8_t blue)
{
  color_encode(data_buf[RESET_OFFSET+led_num].red, red);
  color_encode(data_buf[RESET_OFFSET+led_num].green, green);
  color_encode(data_buf[RESET_OFFSET+led_num].blue, blue);
}

/**
 * @brief Update LEDs colors
 */
void
ws2812b_update(void)
{
  SPI_transmit((uint8_t*) &data_buf, sizeof(data_buf));
}
