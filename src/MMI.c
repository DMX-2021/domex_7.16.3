/**
 * @file MMI.c
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#include <stddef.h>
#include "config_app.h"
#include "EndpointModule.h"
#include "BoardDedicatedModule.h"
#include "ConfigurationModule.h"
#include "BackGroundTimer.h"
#include "SwTimer.h"
#include "AppTimer.h"

extern SApplicationHandles* g_pAppHandles;
extern char Vibrate_Stopped;

void
Refresh_MMI_Idle(void)
{
  EndpointInfo_t* epInfo = NULL;
  BtnMode_t btnMode;
  BacklightConfig_t backlightConfig;
  DimmerConfig_t DimmerConfig;
  configErrCode_t cfg_ret;

  for (int ep = ENDPOINT_1; ep <= EP_NUM; ep++) {

    epInfo = EndpointInfo_Get_byNumber(ep);
    if (epInfo == NULL)
      continue;

    cfg_ret = DeviceConfiguration_Get_Backlight(epInfo->config.BtnNumber.Btn1, &backlightConfig);
    if (cfg_ret != CONFIG_OK)
      continue;

    btnMode = epInfo->config.BtnMode;
    switch(btnMode) {
      case BTN_MODE_DISABLE:
        Board_LedSet(epInfo->config.BtnNumber.Btn, BACKLIGHT_COLOR_BLACK, 0);
        break;

      case BTN_MODE_SWITCH_ON_OFF:
        if (epInfo->relayStatus.state) {
            if ((EINCLUSIONSTATE_EXCLUDED == g_pAppHandles->pNetworkInfo->eInclusionState)&&
                (!(Vibrate_Stopped)))
              {
                Board_LedSet(epInfo->config.BtnNumber.Btn,
                             BACKLIGHT_COLOR_RED, 0);
              }
            else
              {
                Board_LedSet(epInfo->config.BtnNumber.Btn,
                             backlightConfig.onStatusColor, 0);
              }
        } else {
          Board_LedSet(epInfo->config.BtnNumber.Btn,
                       backlightConfig.offStatusColor, 0);
        }
        break;

      case BTN_MODE_PUSH:
        if (epInfo->relayStatus.state) {
          Board_LedSet(epInfo->config.BtnNumber.Btn,
                       backlightConfig.onStatusColor, 0);
        } else {
          Board_LedSet(epInfo->config.BtnNumber.Btn,
                       backlightConfig.offStatusColor, 0);
        }
        break;

      case BTN_MODE_DIMMER_1B:
       DimmerConfig = DeviceConfiguration_Get_Dimmer(epInfo->number);
       if (epInfo->dimmerStatus.CalibrationStatus==NotCal)
          {
            if (epInfo->dimmerStatus.targetLvl!=DimmerConfig.minDimmingValue)
              {
                 if ((EINCLUSIONSTATE_EXCLUDED == g_pAppHandles->pNetworkInfo->eInclusionState)&&
                     (!(Vibrate_Stopped)))
                   {
                     Board_LedSet(epInfo->config.BtnNumber.Btn,
                                  BACKLIGHT_COLOR_RED, 0);
                   }
                 else
                   {
                      Board_LedSet(epInfo->config.BtnNumber.Btn,
                                     backlightConfig.onStatusColor, 0);
                   }
              }
            else
              {
                Board_LedSet(epInfo->config.BtnNumber.Btn,
                             backlightConfig.offStatusColor, 0);
              }
          }
        if (epInfo->dimmerStatus.CalibrationStatus==StartCal)
          {
            switch (epInfo->dimmerStatus.DimmerDirection)
              {
              case UpDirection:
                Board_LedSet(epInfo->config.BtnNumber.Btn,
                             BACKLIGHT_COLOR_MAGENTA, DIMMER_CALIBRATION_LED_BLINK_PERIOD);
                break;
              case DownDirection:
                Board_LedSet(epInfo->config.BtnNumber.Btn,
                             BACKLIGHT_COLOR_YELLOW, DIMMER_CALIBRATION_LED_BLINK_PERIOD);
                break;
              default:
                break;
              }
          }

        /*
        DimmerConfig = DeviceConfiguration_Get_Dimmer(ep);

        if (epInfo->dimmerStatus.isLvlChanging) {
          if (epInfo->dimmerStatus.targetLvl > DimmerConfig.minDimmingValue) {
            Board_LedSet(epInfo->config.BtnNumber.Btn,
                         backlightConfig.onStatusColor, 0);
          } else {
            Board_LedSet(epInfo->config.BtnNumber.Btn,
                         backlightConfig.offStatusColor, 0);
          }
        } else {
          if (epInfo->dimmerStatus.currentLvl > DimmerConfig.minDimmingValue) {
            Board_LedSet(epInfo->config.BtnNumber.Btn,
                         backlightConfig.onStatusColor, 0);
          } else {
            Board_LedSet(epInfo->config.BtnNumber.Btn,
                         backlightConfig.offStatusColor, 0);
          }
        }
        */
        break;

      case BTN_MODE_DIMMER_2B:
        DimmerConfig = DeviceConfiguration_Get_Dimmer(ep);
/*
        if (epInfo->dimmerStatus.isLvlChanging) {
          if (epInfo->dimmerStatus.targetLvl > DimmerConfig.minDimmingValue) {
            Board_LedSet(epInfo->config.BtnNumber.Btn1,
                         backlightConfig.onStatusColor, 0);
            Board_LedSet(epInfo->config.BtnNumber.Btn2,
                         backlightConfig.onStatusColor, 0);
          } else {
            Board_LedSet(epInfo->config.BtnNumber.Btn1,
                         backlightConfig.offStatusColor, 0);
            Board_LedSet(epInfo->config.BtnNumber.Btn2,
                         backlightConfig.offStatusColor, 0);
          }
        } else {
          if (epInfo->dimmerStatus.currentLvl > DimmerConfig.minDimmingValue) {
            Board_LedSet(epInfo->config.BtnNumber.Btn1,
                         backlightConfig.onStatusColor, 0);
            Board_LedSet(epInfo->config.BtnNumber.Btn2,
                         backlightConfig.offStatusColor, 0);
          } else {
            Board_LedSet(epInfo->config.BtnNumber.Btn1,
                         backlightConfig.offStatusColor, 0);
            Board_LedSet(epInfo->config.BtnNumber.Btn2,
                         backlightConfig.offStatusColor, 0);
          }
        }*/
        break;

      case BTN_MODE_BLINDS_1B:
        if (epInfo->blindsStatus.isLvlChanging) {
          Board_LedSet(epInfo->config.BtnNumber.Btn,
                       backlightConfig.onStatusColor,
                       BLINDS_PROCESS_LED_BLINK_PERIOD);
        } else {
          Board_LedSet(epInfo->config.BtnNumber.Btn,
                       backlightConfig.offStatusColor, 0);
        }
        break;

      case BTN_MODE_BLINDS_2B:
        if (epInfo->blindsStatus.isLvlChanging) {
          if (epInfo->blindsStatus.state == BLINDS_STATE_OPEN) {
            Board_LedSet(epInfo->config.BtnNumber.Btn1,
                         backlightConfig.onStatusColor,
                         BLINDS_PROCESS_LED_BLINK_PERIOD);
            Board_LedSet(epInfo->config.BtnNumber.Btn2,
                         backlightConfig.offStatusColor, 0);
          } else {
            Board_LedSet(epInfo->config.BtnNumber.Btn1,
                         backlightConfig.offStatusColor, 0);
            Board_LedSet(epInfo->config.BtnNumber.Btn2,
                         backlightConfig.onStatusColor,
                         BLINDS_PROCESS_LED_BLINK_PERIOD);
          }
        } else {
          Board_LedSet(epInfo->config.BtnNumber.Btn1,
                       backlightConfig.offStatusColor, 0);
          Board_LedSet(epInfo->config.BtnNumber.Btn2,
                       backlightConfig.offStatusColor, 0);
        }
        break;

      case BTN_MODE_BLINDS_SHADE:
        if (epInfo->blindsStatus.isLvlChanging) {
          if (epInfo->blindsStatus.state == BLINDS_STATE_OPEN) {
              if ((EINCLUSIONSTATE_EXCLUDED == g_pAppHandles->pNetworkInfo->eInclusionState)&&
                  (!(Vibrate_Stopped)))
                {
                  Board_LedSet(epInfo->config.BtnNumber.Btn1,
                               BACKLIGHT_COLOR_RED, BLINDS_PROCESS_LED_BLINK_PERIOD);
                }
              else
                {
                    Board_LedSet(epInfo->config.BtnNumber.Btn1,
                                 backlightConfig.onStatusColor, BLINDS_PROCESS_LED_BLINK_PERIOD);
                }
            Board_LedSet(epInfo->config.BtnNumber.Btn2,
                         backlightConfig.offStatusColor, 0);
          } else {
            Board_LedSet(epInfo->config.BtnNumber.Btn1,
                         backlightConfig.offStatusColor, 0);
            if ((EINCLUSIONSTATE_EXCLUDED == g_pAppHandles->pNetworkInfo->eInclusionState)&&
                (!(Vibrate_Stopped)))
              {
                Board_LedSet(epInfo->config.BtnNumber.Btn2,
                             BACKLIGHT_COLOR_RED, BLINDS_PROCESS_LED_BLINK_PERIOD);
              }
            else
              {
                  Board_LedSet(epInfo->config.BtnNumber.Btn2,
                               backlightConfig.onStatusColor, BLINDS_PROCESS_LED_BLINK_PERIOD);
              }
          }
        } else {
          Board_LedSet(epInfo->config.BtnNumber.Btn1,
                       backlightConfig.offStatusColor, 0);
          Board_LedSet(epInfo->config.BtnNumber.Btn2,
                       backlightConfig.offStatusColor, 0);
        }
        break;

      case BTN_MODE_CENTRAL_SCENE_PUSH:
      case BTN_MODE_CENTRAL_SCENE_MASTER_ALL_ONOFF:
        Board_LedSet(epInfo->config.BtnNumber.Btn,
                     backlightConfig.offStatusColor, 0);
        break;

      case BTN_MODE_CENTRAL_SCENE_TOGGLE:
        if (epInfo->centraSceneToggleStatus.state == SWITCH_STATE_ON)
          Board_LedSet(epInfo->config.BtnNumber.Btn,
                       backlightConfig.onStatusColor, 0);
        else
          Board_LedSet(epInfo->config.BtnNumber.Btn,
                       backlightConfig.offStatusColor, 0);
        break;

      default:
        break;
    }
  }
  Board_LedUpdate();
}

void
Refresh_Boiler_MMI_Idle(void)
{
  EndpointInfo_t* epInfo = NULL;
  boilerSwitchStatus_t *pBoilerStatus = NULL;
  fanSwitchStatus_t *pFanStatus = NULL;
  BacklightConfig_t backlightConfig;
  BacklightColor_t onStatusColor;
  BacklightColor_t offStatusColor;
  BoilerConfig_t boilerConfig = DeviceConfiguration_Get_Boiler();
  uint8_t timerTimeoutMinutes = 0;
  SW_Config_t SW_Config = DeviceConfiguration_Get_SW();

  if (SW_Config==SW_CONFIG_B)
    {
        epInfo = EndpointInfo_Get_byNumber(BOILER_EP_NUMB);
        if (epInfo == NULL) return;

        pBoilerStatus = &epInfo->boilerStatus;
        timerTimeoutMinutes = TIMER_TIKS_TO_MIN(pBoilerStatus->tiksCount);

        //! Check does button allowed
        if (DeviceConfiguration_Get_Backlight(epInfo->config.BtnNumber.Btn, &backlightConfig) != CONFIG_OK)
          return;

        onStatusColor = backlightConfig.onStatusColor;
        offStatusColor = backlightConfig.offStatusColor;

        //! Clear all LEDs
        for (BtnNumber_t btn = BTN_NUMBER_1; btn < BTN_NUMBER_MAX; btn++) {
          Board_LedSet(btn, BACKLIGHT_COLOR_BLACK, 0);
        }

              if (pBoilerStatus->state == SWITCH_STATE_ON) {
                Board_LedSet(BOILER_ON_OFF_BUTTON, onStatusColor, 0);
              } else {
                Board_LedSet(BOILER_ON_OFF_BUTTON, offStatusColor, 0);
              }

              if ((BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 1) != 0 &&
                  pBoilerStatus->tiksCount > 0) {
                Board_LedSet(BOILER_1_6TH_MIN_BUTTON, onStatusColor, 0);
              } else {
                Board_LedSet(BOILER_1_6TH_MIN_BUTTON, offStatusColor, 0);
              }

              if ((BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 2) != 0 &&
                  timerTimeoutMinutes >(BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime))) {
                Board_LedSet(BOILER_2_6TH_MIN_BUTTON, onStatusColor, 0);
              } else {
                Board_LedSet(BOILER_2_6TH_MIN_BUTTON, offStatusColor, 0);
              }

              if ((BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 3) != 0 &&
                  timerTimeoutMinutes > (BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 2)) {
                Board_LedSet(BOILER_3_6TH_MIN_BUTTON, onStatusColor, 0);
              } else {
                Board_LedSet(BOILER_3_6TH_MIN_BUTTON, offStatusColor, 0);
              }

              if ((BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 4) != 0 &&
                  timerTimeoutMinutes > boilerConfig.maxBoilingTime/2) {
                Board_LedSet(BOILER_4_6TH_MIN_BUTTON, onStatusColor, 0);
              } else {
                Board_LedSet(BOILER_4_6TH_MIN_BUTTON, offStatusColor, 0);
              }

              if ((BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 5) != 0 &&
                  timerTimeoutMinutes > (BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 4 )) {
                Board_LedSet(BOILER_5_6TH_MIN_BUTTON, onStatusColor, 0);
              } else {
                Board_LedSet(BOILER_5_6TH_MIN_BUTTON, offStatusColor, 0);
              }

              if ((BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 6) != 0 &&
                  timerTimeoutMinutes > (BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 5 )) {
                Board_LedSet(BOILER_6_6TH_MIN_BUTTON, onStatusColor, 0);
              } else {
                Board_LedSet(BOILER_6_6TH_MIN_BUTTON, offStatusColor, 0);
              }
    }
  if (SW_Config==SW_CONFIG_F)
    {
      epInfo = EndpointInfo_Get_byNumber(FAN_EP_NUMB);
      if (epInfo == NULL) return;

      pFanStatus = &epInfo->fanStatus;

      //! Check does button allowed
      if (DeviceConfiguration_Get_Backlight(epInfo->config.BtnNumber.Btn, &backlightConfig) != CONFIG_OK)
        return;

      onStatusColor = backlightConfig.onStatusColor;
      offStatusColor = backlightConfig.offStatusColor;

      //! Clear all LEDs
      for (BtnNumber_t btn = BTN_NUMBER_1; btn < BTN_NUMBER_MAX; btn++) {
        Board_LedSet(btn, BACKLIGHT_COLOR_BLACK, 0);
      }

      switch (pFanStatus->state)
      {
        case FAN_STATE_OFF:
          Board_LedSet(FAN_ON_OFF_BUTTON, backlightConfig.offStatusColor, 0);
          Board_LedSet(FAN_LOW_BUTTON, backlightConfig.offStatusColor, 0);
          Board_LedSet(FAN_MID_BUTTON, backlightConfig.offStatusColor, 0);
          Board_LedSet(FAN_HIGH_BUTTON, backlightConfig.offStatusColor, 0);
        break;
        case FAN_STATE_LOW:
          Board_LedSet(FAN_ON_OFF_BUTTON, backlightConfig.onStatusColor, 0);
          Board_LedSet(FAN_LOW_BUTTON, backlightConfig.onStatusColor, 0);
          Board_LedSet(FAN_MID_BUTTON, backlightConfig.offStatusColor, 0);
          Board_LedSet(FAN_HIGH_BUTTON, backlightConfig.offStatusColor, 0);
        break;
        case FAN_STATE_MID:
          Board_LedSet(FAN_ON_OFF_BUTTON, backlightConfig.onStatusColor, 0);
          Board_LedSet(FAN_LOW_BUTTON, backlightConfig.onStatusColor, 0);
          Board_LedSet(FAN_MID_BUTTON, backlightConfig.onStatusColor, 0);
          Board_LedSet(FAN_HIGH_BUTTON, backlightConfig.offStatusColor, 0);
        break;
        case FAN_STATE_HIGH:
          Board_LedSet(FAN_ON_OFF_BUTTON, backlightConfig.onStatusColor, 0);
          Board_LedSet(FAN_LOW_BUTTON, backlightConfig.onStatusColor, 0);
          Board_LedSet(FAN_MID_BUTTON, backlightConfig.onStatusColor, 0);
          Board_LedSet(FAN_HIGH_BUTTON, backlightConfig.onStatusColor, 0);
        break;
        case FAN_STATE_UNKNOWN:
        default:
        break;
      }

    }


  Board_LedUpdate();
}

void
Refresh_MMI_LearnMode(void)
{
  EndpointInfo_t* epInfo = NULL;
  BacklightConfig_t backlightConfig;

  for (int ep = ENDPOINT_1; ep < EP_NUM; ep++) {

    epInfo = EndpointInfo_Get_byNumber(ep);
    if (epInfo == NULL)
      continue;

    switch (epInfo->config.BtnMode) {
      case BTN_MODE_DISABLE:
        Board_LedSet(epInfo->config.BtnNumber.Btn, BACKLIGHT_COLOR_BLACK, 0);
        break;

      case BTN_MODE_SWITCH_ON_OFF:
      case BTN_MODE_PUSH:
      case BTN_MODE_DIMMER_1B:
      case BTN_MODE_BLINDS_1B:
      case BTN_MODE_CENTRAL_SCENE_PUSH:
      case BTN_MODE_CENTRAL_SCENE_TOGGLE:
      case BTN_MODE_CENTRAL_SCENE_MASTER_ALL_ONOFF:
        if (DeviceConfiguration_Get_Backlight(epInfo->config.BtnNumber.Btn, &backlightConfig) == CONFIG_OK)
          Board_LedSet(epInfo->config.BtnNumber.Btn,
                       backlightConfig.offStatusColor,
                       LEARN_MODE_LED_BLINK_PERIOD);
        break;

      case BTN_MODE_DIMMER_2B:
      case BTN_MODE_BLINDS_2B:
      case BTN_MODE_BLINDS_SHADE:
        if (DeviceConfiguration_Get_Backlight(epInfo->config.BtnNumber.Btn1, &backlightConfig) == CONFIG_OK)
          Board_LedSet(epInfo->config.BtnNumber.Btn1,
                       backlightConfig.offStatusColor,
                       LEARN_MODE_LED_BLINK_PERIOD);

        if (DeviceConfiguration_Get_Backlight(epInfo->config.BtnNumber.Btn2, &backlightConfig) == CONFIG_OK)
          Board_LedSet(epInfo->config.BtnNumber.Btn2,
                       backlightConfig.offStatusColor,
                       LEARN_MODE_LED_BLINK_PERIOD);
        break;

      default:
        break;
    }
  }

  Board_LedUpdate();
}

void
Refresh_Boiler_MMI_LearnMode(void)
{
  BacklightConfig_t backlightConfig;

  if (DeviceConfiguration_Get_Backlight(BOILER_ON_OFF_BUTTON, &backlightConfig) == CONFIG_OK)
    Board_LedSet(BOILER_ON_OFF_BUTTON, backlightConfig.offStatusColor, LEARN_MODE_LED_BLINK_PERIOD);

  if (DeviceConfiguration_Get_Backlight(BOILER_1_6TH_MIN_BUTTON, &backlightConfig) == CONFIG_OK)
    Board_LedSet(BOILER_1_6TH_MIN_BUTTON, backlightConfig.offStatusColor, LEARN_MODE_LED_BLINK_PERIOD);

  if (DeviceConfiguration_Get_Backlight(BOILER_2_6TH_MIN_BUTTON, &backlightConfig) == CONFIG_OK)
    Board_LedSet(BOILER_2_6TH_MIN_BUTTON, backlightConfig.offStatusColor, LEARN_MODE_LED_BLINK_PERIOD);

  if (DeviceConfiguration_Get_Backlight(BOILER_3_6TH_MIN_BUTTON, &backlightConfig) == CONFIG_OK)
    Board_LedSet(BOILER_3_6TH_MIN_BUTTON, backlightConfig.offStatusColor, LEARN_MODE_LED_BLINK_PERIOD);

  if (DeviceConfiguration_Get_Backlight(BOILER_4_6TH_MIN_BUTTON, &backlightConfig) == CONFIG_OK)
    Board_LedSet(BOILER_4_6TH_MIN_BUTTON, backlightConfig.offStatusColor, LEARN_MODE_LED_BLINK_PERIOD);

  if (DeviceConfiguration_Get_Backlight(BOILER_5_6TH_MIN_BUTTON, &backlightConfig) == CONFIG_OK)
    Board_LedSet(BOILER_5_6TH_MIN_BUTTON, backlightConfig.offStatusColor, LEARN_MODE_LED_BLINK_PERIOD);

  if (DeviceConfiguration_Get_Backlight(BOILER_6_6TH_MIN_BUTTON, &backlightConfig) == CONFIG_OK)
    Board_LedSet(BOILER_6_6TH_MIN_BUTTON, backlightConfig.offStatusColor, LEARN_MODE_LED_BLINK_PERIOD);

  Board_LedUpdate();
}

void
Refresh_MMI_Reset(void)
{
  EndpointInfo_t* epInfo = NULL;
  BacklightConfig_t backlightConfig;

  for (int ep = ENDPOINT_1; ep <= EP_NUM; ep++) {
    epInfo = EndpointInfo_Get_byNumber(ep);

    if (epInfo == NULL)
      continue;

    switch (epInfo->config.BtnMode) {
      case BTN_MODE_DISABLE:
        Board_LedSet(epInfo->config.BtnNumber.Btn, BACKLIGHT_COLOR_BLACK, 0);
        break;

      case BTN_MODE_SWITCH_ON_OFF:
      case BTN_MODE_PUSH:
      case BTN_MODE_DIMMER_1B:
      case BTN_MODE_BLINDS_1B:
      case BTN_MODE_CENTRAL_SCENE_PUSH:
      case BTN_MODE_CENTRAL_SCENE_TOGGLE:
      case BTN_MODE_CENTRAL_SCENE_MASTER_ALL_ONOFF:
        if (DeviceConfiguration_Get_Backlight(epInfo->config.BtnNumber.Btn, &backlightConfig) == CONFIG_OK)
          Board_LedSet(epInfo->config.BtnNumber.Btn,
                       backlightConfig.onStatusColor, 0);
        break;

      case BTN_MODE_DIMMER_2B:
      case BTN_MODE_BLINDS_2B:
      case BTN_MODE_BLINDS_SHADE:
        if (DeviceConfiguration_Get_Backlight(epInfo->config.BtnNumber.Btn1, &backlightConfig) == CONFIG_OK)
          Board_LedSet(epInfo->config.BtnNumber.Btn1,
                       backlightConfig.onStatusColor, 0);

        if (DeviceConfiguration_Get_Backlight(epInfo->config.BtnNumber.Btn2, &backlightConfig) == CONFIG_OK)
          Board_LedSet(epInfo->config.BtnNumber.Btn2,
                       backlightConfig.onStatusColor, 0);
        break;

      default:
        break;
    }
  }
  Board_LedUpdate();
}

void
Refresh_Boiler_MMI_Reset()
{
  BacklightConfig_t backlightConfig = { 0 };

  if (DeviceConfiguration_Get_Backlight(BOILER_1_6TH_MIN_BUTTON, &backlightConfig) == CONFIG_OK)
    Board_LedSet(BOILER_1_6TH_MIN_BUTTON, backlightConfig.offStatusColor, 0);

  if (DeviceConfiguration_Get_Backlight(BOILER_2_6TH_MIN_BUTTON, &backlightConfig) == CONFIG_OK)
    Board_LedSet(BOILER_2_6TH_MIN_BUTTON, backlightConfig.offStatusColor, 0);

  if (DeviceConfiguration_Get_Backlight(BOILER_3_6TH_MIN_BUTTON, &backlightConfig) == CONFIG_OK)
    Board_LedSet(BOILER_3_6TH_MIN_BUTTON, backlightConfig.offStatusColor, 0);

  if (DeviceConfiguration_Get_Backlight(BOILER_4_6TH_MIN_BUTTON, &backlightConfig) == CONFIG_OK)
    Board_LedSet(BOILER_4_6TH_MIN_BUTTON, backlightConfig.offStatusColor, 0);

  if (DeviceConfiguration_Get_Backlight(BOILER_5_6TH_MIN_BUTTON, &backlightConfig) == CONFIG_OK)
    Board_LedSet(BOILER_5_6TH_MIN_BUTTON, backlightConfig.offStatusColor, 0);

  if (DeviceConfiguration_Get_Backlight(BOILER_6_6TH_MIN_BUTTON, &backlightConfig) == CONFIG_OK)
    Board_LedSet(BOILER_6_6TH_MIN_BUTTON, backlightConfig.offStatusColor, 0);

  Board_LedUpdate();
}

void
Refresh_MMI_CentralSceneNotification(uint8_t epNumb)
{
  EndpointInfo_t* epInfo = NULL;
  BacklightConfig_t backlightConfig;

  epInfo = EndpointInfo_Get_byNumber(epNumb);
  if (epInfo) {
    if (epInfo->config.BtnMode == BTN_MODE_CENTRAL_SCENE_PUSH ||
        epInfo->config.BtnMode == BTN_MODE_CENTRAL_SCENE_MASTER_ALL_ONOFF) {
      //! Select Button backlight and Set the LED
      if (DeviceConfiguration_Get_Backlight(epInfo->config.BtnNumber.Btn, &backlightConfig) == CONFIG_OK) {
        Board_LedSet(epInfo->config.BtnNumber.Btn, backlightConfig.onStatusColor, 0);
        Board_LedUpdate();
      }
      vTaskDelay(100 / portTICK_RATE_MS);
    }
  }
}
