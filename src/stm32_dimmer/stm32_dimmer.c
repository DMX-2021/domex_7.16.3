/**
 * @file stm32f0_dimmer.c
 * @brief STM32F0 dimmer driver source file
 *
 * @details This file contains definitions and basic functions for
 *          communication with STM32F0 dimmer via I2C
 *
 * @author Alexander Grin, Anton K.
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#include "stm32_dimmer.h"
#include "em_gpio.h"
#include <em_i2c.h>
#define STM32_DIMMER_RST_PORT             gpioPortD
#define STM32_DIMMER_RST_PIN              12

#define STM32_DIMMER_I2C_ADR              (0x3A)
#define STM32_DIMMER_CHIP_ID              (0xCA)

#define STM32_DIMMER_RESET_CMD            (0xFF)

#define __STM32_VERIFY_CH(CH)             (CH < STM32_DIMMER_CH_COUNT)

typedef enum {
    STM32_DIMMER_220V_CH_1_REG  = 0x00,
    STM32_DIMMER_220V_CH_2_REG,
    STM32_DIMMER_220V_CH_3_REG,
    STM32_DIMMER_220V_CH_4_REG,
    STM32_DIMMER_10V_CH_1_REG,
    STM32_DIMMER_10V_CH_2_REG,
    STM32_DIMMER_RESET_REG,
    STM32_DIMMER_FIRMWARE_VER_REG,
    STM32_DIMMER_CHIP_ID_REG,
    STM32_DIMMER_MAX_REG
} I2C_SlaveRegister_t;

//! Local variables
static I2CSPM_Init_TypeDef *_i2c_instance;

stm32_dimmer_err_t
stm32_dimmer_init(I2CSPM_Init_TypeDef *i2c_instance)
{
  I2C_TransferReturn_TypeDef i2c_ret;
  _i2c_instance = i2c_instance;

  uint8_t chip_id = 0xFF;

  i2c_ret=i2cTransferInProgress;
  while (i2c_ret != i2cTransferDone)
    {
        i2c_ret = i2c_read_reg(_i2c_instance,
                               STM32_DIMMER_I2C_ADR,
                               STM32_DIMMER_CHIP_ID_REG,
                               &chip_id, 1);
    }
  if(i2c_ret != i2cTransferDone)
    return STM32F0_DIMMER_ERR;

  if(chip_id != STM32_DIMMER_CHIP_ID)
    return STM32F0_DIMMER_ERR;

  return STM32F0_DIMMER_OK;
}

stm32_dimmer_err_t
stm32_dimmer_fw_get(uint8_t *fw_version)
{
  I2C_TransferReturn_TypeDef i2c_ret;

  i2c_ret = i2c_read_reg(_i2c_instance,
                         STM32_DIMMER_I2C_ADR,
                         STM32_DIMMER_FIRMWARE_VER_REG,
                         fw_version, 1);

  if (i2c_ret != i2cTransferDone)
    return STM32F0_DIMMER_ERR;

  return STM32F0_DIMMER_OK;
}

static stm32_dimmer_err_t __stm32_dimmer_reg4ch_set(stm32_dimmer_ch_t channel, I2C_SlaveRegister_t *reg)
{
  if (!reg)
    return STM32F0_DIMMER_ERR;

  switch (channel) {
    case STM32_DIMMER_220V_CH1:
      *reg = STM32_DIMMER_220V_CH_1_REG;
      break;
    case STM32_DIMMER_220V_CH2:
      *reg = STM32_DIMMER_220V_CH_2_REG;
      break;
    case STM32_DIMMER_220V_CH3:
      *reg = STM32_DIMMER_220V_CH_3_REG;
      break;
    case STM32_DIMMER_220V_CH4:
      *reg = STM32_DIMMER_220V_CH_4_REG;
      break;
    case STM32_DIMMER_10V_CH1:
      *reg = STM32_DIMMER_10V_CH_1_REG;
      break;
    case STM32_DIMMER_10V_CH2:
      *reg = STM32_DIMMER_10V_CH_2_REG;
      break;

    default:
      return STM32F0_DIMMER_ERR;
      break;
  }

  return STM32F0_DIMMER_OK;
}

stm32_dimmer_err_t
stm32_dimmer_ch_get(stm32_dimmer_ch_t channel, stm32_dimmer_value_t* value, stm32_dimmer_duration_t *duration)
{
  I2C_TransferReturn_TypeDef i2c_ret;
  uint8_t data_in[2];
  I2C_SlaveRegister_t reg;

  if (!__STM32_VERIFY_CH(channel) || !value || !duration)
    goto __err;

  if (__stm32_dimmer_reg4ch_set(channel, &reg) != STM32F0_DIMMER_OK)
    goto __err;

  i2c_ret = i2c_read_reg(_i2c_instance,
                         STM32_DIMMER_I2C_ADR,
                         reg, data_in, 2);
  if (i2c_ret != i2cTransferDone)
    goto __err;

  *duration = data_in[0];
  *value = data_in[1];

  return STM32F0_DIMMER_OK;
__err:
  return STM32F0_DIMMER_ERR;
}

stm32_dimmer_err_t
stm32_dimmer_ch_set(stm32_dimmer_ch_t channel, stm32_dimmer_value_t value, stm32_dimmer_duration_t duration)
{
  I2C_TransferReturn_TypeDef i2c_ret;
  uint8_t data_out[2];
  I2C_SlaveRegister_t reg;

  if (!__STM32_VERIFY_CH(channel))
    goto __err;

  if (__stm32_dimmer_reg4ch_set(channel, &reg) != STM32F0_DIMMER_OK)
    goto __err;

  data_out[0] = duration;
  data_out[1] = value;
  i2c_ret = i2c_write_reg(_i2c_instance,
                          STM32_DIMMER_I2C_ADR,
                          reg, data_out, 2);
  if (i2c_ret != i2cTransferDone)
    goto __err;

  return STM32F0_DIMMER_OK;
__err:
  return STM32F0_DIMMER_ERR;
}

stm32_dimmer_err_t
stm32_dimmer_reset(void)
{
  I2C_TransferReturn_TypeDef i2c_ret;
  uint8_t reset_cmd = STM32_DIMMER_RESET_CMD;

  i2c_ret = i2c_write_reg(_i2c_instance,
                          STM32_DIMMER_I2C_ADR,
                          STM32_DIMMER_RESET_REG,
                          &reset_cmd, 1);
  if (i2c_ret != i2cTransferDone)
    return STM32F0_DIMMER_ERR;

  return STM32F0_DIMMER_OK;
}

void DimmerSTM32_Hard_Reset(void)
{
  GPIO_PinModeSet(STM32_DIMMER_RST_PORT, STM32_DIMMER_RST_PIN, gpioModePushPull, 0);
}
void DimmerSTM32_Hard_Reset_Part2(void)
{
  GPIO_PinModeSet(STM32_DIMMER_RST_PORT, STM32_DIMMER_RST_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(STM32_DIMMER_RST_PORT, STM32_DIMMER_RST_PIN, gpioModeInputPullFilter, 1);

}
