/**
 * @file stm32_dimmer.c
 * @brief STM32 dimmer driver source file
 *
 * @details This file contains definitions and basic functions for
 *          communication with STM32 dimmer via I2C
 *
 * @author Alexander Grin, Anton K.
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#ifndef STM32_DIMMER_H
#define STM32_DIMMER_H

#include <stdint.h>
#include "i2c_driver.h"

/**
 * @brief - STM32 dimmer error code
 */
typedef enum
{
  STM32F0_DIMMER_OK = 0, //!< STM32F0_DIMMER_OK
  STM32F0_DIMMER_ERR = -1 //!< STM32F0_DIMMER_ERR
} stm32_dimmer_err_t;

/**
 * @brief - STM32 dimmer channel
 */
typedef enum
{
  STM32_DIMMER_220V_CH1,
  STM32_DIMMER_220V_CH2,
  STM32_DIMMER_220V_CH3,
  STM32_DIMMER_220V_CH4,
  STM32_DIMMER_10V_CH1,
  STM32_DIMMER_10V_CH2,
  STM32_DIMMER_CH_COUNT
} stm32_dimmer_ch_t;

typedef uint8_t stm32_dimmer_duration_t;
typedef uint8_t stm32_dimmer_value_t;

/**
 * @brief - Initialization of STM32 dimmer,
 *          check slave on line
 *
 * @param i2c_instance - Pointer to instance of I2C interface
 * @return
 *    STM32F0_DIMMER_OK - Success
 */
stm32_dimmer_err_t
stm32_dimmer_init(I2CSPM_Init_TypeDef *i2c_instance);

/**
 * @brief Reset STM32 Dimmer
 *
 * @return
 *    STM32F0_DIMMER_OK - Success
 */
stm32_dimmer_err_t
stm32_dimmer_reset(void);

/**
 * @brief - Get Firmware version
 *
 * @param fw_version - Pointer to the version variable
 * @return
 *    STM32F0_DIMMER_OK - Success
 */
stm32_dimmer_err_t
stm32_dimmer_fw_get(uint8_t *fw_version);

/**
 * @brief Get value of channel
 *
 * @param channel - Channel number
 * @param value - Pointer to value of channel
 * @return
 *    STM32F0_DIMMER_OK - Success
 */
/**
 * @brief Get value of channel
 *
 * @param[IN] channel - Channel number
 * @param[OUT] value - Value of channel
 * @param[OUT] duration - Duration of channel
 *
 * @return STM32F0_DIMMER_OK - success
 *         STM32F0_DIMMER_ERR - error
 */
stm32_dimmer_err_t
stm32_dimmer_ch_get(stm32_dimmer_ch_t channel, stm32_dimmer_value_t* value, stm32_dimmer_duration_t *duration);

/**
 * @brief Set value of channel
 *
 * @param[IN] channel - Channel number
 * @param[IN] value - Value of channel
 * @param[IN] duration - Duration of channel
 *
 * @return STM32F0_DIMMER_OK - success
 *         STM32F0_DIMMER_ERR - error
 */
stm32_dimmer_err_t
stm32_dimmer_ch_set(stm32_dimmer_ch_t channel, stm32_dimmer_value_t value, stm32_dimmer_duration_t duration);

void DimmerSTM32_Hard_Reset(void);
void DimmerSTM32_Hard_Reset_Part2(void);

#endif //!STM32_DIMMER_H
