/**
 * @file opt3002.c
 * @brief OPT3002 Light sensor driver source
 *
 * @details This file contains definitions and basic functions for OPT3002
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#include <math.h>
#include "opt3002.h"
#include "ZW_typedefs.h"

//! OPT3002 device ID
#define OPT3002_DEV_ID                      0x5449

//! Default LOW and HIGH limit
#define DEFAULT_LOW_LIMIT                   (1900)
#define DEFAULT_HIGH_LIMIT                  (10*1000)

#define OPT3002_REGISTER_LEN                (0x02)

#define INT_PIN_NOT_USED                    (0xFF)

/**
 * @brief OPT3002 register map
 */
typedef enum
{
  RESULT_REG = 0x00,         //!< RESULT_REG
  CONFIGURATION_REG,         //!< CONFIGURATION_REG
  LOW_LIMIT_REG,             //!< LOW_LIMIT_REG
  HIGH_LIMIT_REG,            //!< HIGH_LIMIT_REG
  MANUFACTURER_ID_REG = 0x7E,            //!< MANUFACTURER_ID_REG
} opt3002_reg_t;

/**
 * @brief OPT3002 result data structure
 */
typedef union
{
  struct
  {
    uint16_t reading :12;
    uint8_t exponent :4;
  };
  uint16_t raw_data;
} opt3002_data_t;

//! Local variables
static I2CSPM_Init_TypeDef *_i2c_instance;
static opt3002_adr_t _i2c_adr;
static GPIO_Port_TypeDef _int_port;
static unsigned int _int_pin;

/**
 * @brief Write to OPT3002 register
 *
 * @param reg - OPT3002 register
 * @param data - Data for writing
 * @return i2c error code
 */
static I2C_TransferReturn_TypeDef
opt3002_write_reg(opt3002_reg_t reg, uint16_t data)
{
  I2C_TransferReturn_TypeDef i2c_ret;
  uint8_t write_data[2];

  write_data[0] = data >> 8;
  write_data[1] = data & 0x00FF;

  i2c_ret = i2c_write_reg(_i2c_instance, _i2c_adr, reg, write_data,
  OPT3002_REGISTER_LEN);

  return i2c_ret;
}

/**
 * @brief Read from OPT3002 register
 *
 * @param reg - OPT3002 register
 * @param data - Pointer to return data
 * @return i2c error code
 */
static I2C_TransferReturn_TypeDef
opt3002_read_reg(opt3002_reg_t reg, uint16_t *data)
{
  I2C_TransferReturn_TypeDef i2c_ret;
  uint8_t read_data[2];

  i2c_ret = i2c_read_reg(_i2c_instance, _i2c_adr, reg, read_data,
  OPT3002_REGISTER_LEN);

  *data = (read_data[0] << 8) + (read_data[1]);

  return i2c_ret;
}

/**
 * @brief Callback for OPT3002 interrupt
 *
 * @param intNo - Interrupt number
 */
static void
opt3002_int_callback(uint8_t intNo)
{
  UNUSED(intNo);
  opt3002_config_t config = { 0 };
  opt3002_read_reg(CONFIGURATION_REG, (uint16_t*) &config);

  if(config.low_flag) {
  }

  if(config.high_flag) {
  }

  if(config.overflow_flag) {
  }

  if(config.conversion_ready) {
  }

}

opt3002_err_t
opt3002_init(I2CSPM_Init_TypeDef *i2c_instance, opt3002_adr_t i2c_adr,
             GPIO_Port_TypeDef int_port, unsigned int int_pin)
{
  UNUSED(int_pin);

  I2C_TransferReturn_TypeDef i2c_ret;
  _i2c_instance = i2c_instance;
  _i2c_adr = i2c_adr;
  _int_port = int_port;
  _int_pin = _int_pin;

  uint16_t manufacture_id;
  i2c_ret = opt3002_read_reg(MANUFACTURER_ID_REG, &manufacture_id);
  if(i2c_ret != i2cTransferDone)
    return OPT3002_ERR;

  if(manufacture_id != OPT3002_DEV_ID)
    return OPT3002_ERR;

  opt3002_config_t config = { 0 };
  i2c_ret = opt3002_read_reg(CONFIGURATION_REG, (uint16_t*) &config);
  if(i2c_ret != i2cTransferDone)
    return OPT3002_ERR;


  config.fault_count = 0;                     //! One fault count for interrupt
  config.mask_exponent = 0;
  config.polarity = 0;                        //! pin Low, when interrupt
  config.latch = 1;                           //! hysteresis-style trigger
  config.mode_of_conversion_operation = 0b10; //! Continuous conversions
  config.convertion_time = 0;                 //! 100 ms
  config.range_number = 0x0C;                 //! Full-scale mode


  i2c_ret = opt3002_write_reg(CONFIGURATION_REG, (uint16_t) config.raw_data);
  if(i2c_ret != i2cTransferDone)
    return OPT3002_ERR;

  //! Set Low limit value
  i2c_ret = opt3002_write_reg(LOW_LIMIT_REG, DEFAULT_LOW_LIMIT);
  if(i2c_ret != i2cTransferDone)
    return OPT3002_ERR;

  //! Set High limit value
  i2c_ret = opt3002_write_reg(HIGH_LIMIT_REG, DEFAULT_HIGH_LIMIT);
  if(i2c_ret != i2cTransferDone)
    return OPT3002_ERR;

  if(_int_port != INT_PIN_NOT_USED && _int_pin != INT_PIN_NOT_USED) {
    //! Set INT pin as input
    GPIO_PinModeSet(_int_port, _int_pin, gpioModeInputPullFilter, 1);

    //! Register interrupt callback
    GPIOINT_Init();
    GPIO_ExtIntConfig(_int_port, _int_pin, _int_pin, false, true, true);
    GPIOINT_CallbackRegister(_int_pin, opt3002_int_callback);
  }

  return OPT3002_OK;
}

opt3002_err_t
opt3002_get_result(uint16_t *result)
{
  I2C_TransferReturn_TypeDef i2c_ret;

  i2c_ret = opt3002_read_reg(RESULT_REG, result);
  if(i2c_ret != i2cTransferDone)
    return OPT3002_ERR;

  return OPT3002_OK;
}

opt3002_err_t
opt3002_get_optical_power(float *power)
{
  opt3002_data_t result;

  if(opt3002_get_result((uint16_t*) &result) != OPT3002_OK)
    return OPT3002_ERR;

  //! Calculate optical power
  *power = 1.2 * powf(2, result.exponent) * result.reading;

  return OPT3002_OK;
}

