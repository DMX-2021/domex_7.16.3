/**
 * @file opt3002.h
 * @brief OPT3002 Light sensor driver header
 *
 * @details This file contains definitions and basic functions for OPT3002
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#ifndef OPT3002_H_
#define OPT3002_H_

#include <stdint.h>
#include "i2c_driver.h"
#include "gpiointerrupt.h"

/**
 * @brief OPT3002 I2C slave address
 */
typedef enum
{
  GND_ADDRESS = 0x44 << 1, //!< GND_ADDRESS
  VDD_ADDRESS,            //!< VDD_ADDRESS
  SDA_ADDRESS,            //!< SDA_ADDRESS
  SCL_ADDRESS             //!< SCL_ADDRESS
} opt3002_adr_t;

/**
 * @brief OPT3002 error code
 */
typedef enum
{
  OPT3002_OK = 0, //!< OPT3002_OK
  OPT3002_ERR = -1 //!< OPT3002_ERR
} opt3002_err_t;

/**
 * @brief OPT3002 configure register bit field
 */
typedef union
{
  struct
  {
    uint8_t fault_count :2;
    uint8_t mask_exponent :1;
    uint8_t polarity :1;
    uint8_t latch :1;
    uint8_t low_flag :1;
    uint8_t high_flag :1;
    uint8_t conversion_ready :1;
    uint8_t overflow_flag :1;
    uint8_t mode_of_conversion_operation :2;
    uint8_t convertion_time :1;
    uint8_t range_number :4;
  };
  uint16_t raw_data;
} opt3002_config_t;

/**
 * @brief Initialization of OPT3002 light sensor
 *
 * @param i2c_instance - Pointer to instance of I2C interface
 * @param i2c_adr - OPT3002 I2C slave address
 * @param int_port - interrupt pin port. If interrupt not use MUST be 0xFF
 * @param int_pin - interrupt pin. If interrupt not use MUST be 0xFF
 * @return
 *  OPT3002_OK - Success
 */
opt3002_err_t
opt3002_init(I2CSPM_Init_TypeDef *i2c_instance, opt3002_adr_t i2c_adr,
             GPIO_Port_TypeDef int_port, unsigned int int_pin);

/**
 * @brief Get raw data of RESULT register
 *
 * @param opt3002_data - Pointer to return var
 * @return
 *  OPT3002_OK - Success
 */
opt3002_err_t
opt3002_get_result(uint16_t *result);

/**
 * @brief Get optical power
 *
 * @param result - Pointer to return var
 * @return
 *  OPT3002_OK - Success
 */
opt3002_err_t
opt3002_get_optical_power(float* result);

#endif //! OPT3002_H_
