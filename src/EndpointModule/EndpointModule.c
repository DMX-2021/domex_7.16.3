/**
 * @file EndpointModule.h
 * @brief Module helps manage endpoints and build ep NIF, AGI
          by device configuration
 *        by device configurations
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#include "EndpointModule.h"
#include "EndpointModulePresets.h"
#include "ConfigurationPresets.h"
#include "TouchController.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <SizeOf.h>
#include "config_app.h"
#include "CC_Basic.h"

extern void TouchContorller_SwitchOnOff(touchButton_t btn, touchButtonEvent_t evt, void* args);
extern void TouchContorller_Dimmer_1B(touchButton_t btn, touchButtonEvent_t evt, void* args);
extern void Boiler_KeyPress(touchButton_t btn, touchButtonEvent_t evt, void* args);

//! The Multi Channel End Point functionality
static EP_FUNCTIONALITY_DATA EndpointFunctionality = {
    {
      NUMBER_OF_INDIVIDUAL_ENDPOINTS,    /**< nbrIndividualEndpoints 7 bit*/
      RES_ZERO,                          /**< resIndZeorBit 1 bit*/
      NUMBER_OF_AGGREGATED_ENDPOINTS,    /**< nbrAggregatedEndpoints 7 bit*/
      RES_ZERO,                          /**< resAggZeorBit 1 bit*/
      RES_ZERO,                          /**< resZero 6 bit*/
      ENDPOINT_IDENTICAL_DEVICE_CLASS_NO,/**< identical 1 bit*/
      ENDPOINT_DYNAMIC_YES               /**< dynamic 1 bit*/
    }
};

//! Array of endpoints information
static EndpointInfo_t EndpointInfo_list[EP_NUM+1];

//! Array of endpoints node information frame
static EP_NIF endpointNIF_list[EP_NUM];

//! Array if endpoints Association Group Info
static AGI_GROUP *endpointAGI_list[EP_NUM];

//! Array and count of enpoints which supporting Binary Switch CC
uint8_t supportingBinarySwitch_list[SWITCH_BINARY_ENDPOINTS];
uint8_t supportingBinarySwitch_count = 0;

//! Array and count of enpoints which supporting Multilevel Switch CC Dimmer
uint8_t supportingDimmerSwitch_list[SWITCH_DIMMER_ENDPOINTS];
uint8_t supportingDimmerSwitch_count = 0;

//! Array and count of enpoints which supporting Multilevel Switch CC Blinds
uint8_t supportingBlindsSwitch_list[SWITCH_BLINDS_ENDPOINTS];
uint8_t supportingBlindsSwitch_count = 0;

/**
 * @brief Initializes NIF and AGI of endpoint by its configuration structure
 *
 * @param config - endpoint configuration structure
 * @return count of successfully initialized endpoints
 */
static uint8_t
EndpointModule_BuildEp(EndpointConfig_t config, AGI_GROUP* retRootGroup, uint8_t* rootGroupCount)
{
  static uint8_t epCount = 0; //! Count of successfully initialized endpoints
  uint8_t epNumber = epCount + 1; //! Because we always have root EP,
                                  //! number different from the count

  EndpointInfo_t* endpointInfo = &EndpointInfo_list[epCount];
  EpMode_t epMode = EP_MODE_DISABLE;
  BtnMode_t btnMode = BTN_MODE_DISABLE;
  EP_NIF* endpointNIF = &endpointNIF_list[epCount];
  AGI_GROUP* endpointAGI = endpointAGI_list[epCount];

  /**
   * Building endpoint's NIF
   */
  switch(config.EpMode) {
    case EP_MODE_REMOTE_SWITCH:
      if(config.BtnMode == BTN_MODE_CENTRAL_SCENE_PUSH ||
         config.BtnMode == BTN_MODE_CENTRAL_SCENE_TOGGLE ||
         config.BtnMode == BTN_MODE_CENTRAL_SCENE_MASTER_ALL_ONOFF) {
        memcpy(endpointNIF, &CentralScene_NIF, sizeof(EP_NIF));
        CC_AGI_LifeLineGroupSetup(CentralScene_LifeLineList,
                                  CentralScene_LifeLineListSize, epNumber);
      }
      else {
        memcpy(endpointNIF, &RemoteSwitch_NIF, sizeof(EP_NIF));
        CC_AGI_LifeLineGroupSetup(RemoteSwitch_LifeLineList,
                                  RemoteSwitch_LifeLineListSize, epNumber);
      }
      break;
    case EP_MODE_RELAY_SWITCH:
      if(supportingBinarySwitch_count >= SWITCH_BINARY_ENDPOINTS)
        goto exit;
      supportingBinarySwitch_list[supportingBinarySwitch_count++] = epNumber;
      memcpy(endpointNIF, &RelaySwitch_NIF, sizeof(EP_NIF));
      CC_AGI_LifeLineGroupSetup(RelaySwitch_LifeLineList,
                                RelaySwitch_LifeLineListSize, epNumber);
      break;
    case EP_MODE_DIMMER_SWITCH:
      if(supportingDimmerSwitch_count >= SWITCH_DIMMER_ENDPOINTS)
        goto exit;
      supportingDimmerSwitch_list[supportingDimmerSwitch_count++] = epNumber;
      memcpy(endpointNIF, &DimmerSwitch_NIF, sizeof(EP_NIF));
      CC_AGI_LifeLineGroupSetup(DimmerSwitch_LifeLineList,
                                DimmerSwitch_LifeLineListSize, epNumber);
      break;
    case EP_MODE_BLINDS_SWITCH:
      if(supportingBlindsSwitch_count >= SWITCH_BLINDS_ENDPOINTS)
        goto exit;
      supportingBlindsSwitch_list[supportingBlindsSwitch_count++] = epNumber;
      memcpy(endpointNIF, &BlindsSwitch_NIF, sizeof(EP_NIF));
      CC_AGI_LifeLineGroupSetup(BlindsSwitch_LifeLineList,
                                BlindsSwitch_LifeLineListSize, epNumber);
      break;
    case EP_MODE_BOILER_SWITCH:
      if(supportingBinarySwitch_count >= SWITCH_BINARY_ENDPOINTS)
        goto exit;
      supportingBinarySwitch_list[supportingBinarySwitch_count++] = epNumber;
      memcpy(endpointNIF, &RelaySwitch_NIF, sizeof(EP_NIF));
      CC_AGI_LifeLineGroupSetup(RelaySwitch_LifeLineList,
                                RelaySwitch_LifeLineListSize, epNumber);
      break;
    case EP_MODE_FAN_SWITCH:
      if(supportingBinarySwitch_count >= SWITCH_BINARY_ENDPOINTS)
        goto exit;
      supportingBinarySwitch_list[supportingBinarySwitch_count++] = epNumber;
      memcpy(endpointNIF, &RelaySwitch_NIF, sizeof(EP_NIF));
      CC_AGI_LifeLineGroupSetup(RelaySwitch_LifeLineList,
                                RelaySwitch_LifeLineListSize, epNumber);
      break;
    case EP_MODE_DISABLE:
      //! Nothing to build
      goto exit;
      break;
    default:
      //! Should not be here
      goto exit;
      break;
  }

  /**
   * Building endpoint's AGI
   */
  if(config.BtnNumber.Btn1 == BTN_NUMBER_NONE) {
  }
  else {
    switch(config.BtnMode) {
      case BTN_MODE_SWITCH_ON_OFF:
      case BTN_MODE_PUSH:
        endpointAGI = (AGI_GROUP*) malloc(AGI_BasicSize);
        memcpy(endpointAGI, AGI_Basic, AGI_BasicSize);
        endpointAGI->profile.profile_LS = config.BtnNumber.Btn1;
        AGI_ResourceGroupSetup(endpointAGI, (AGI_BasicSize / sizeof(AGI_GROUP)), epNumber);

        memcpy(retRootGroup, AGI_Basic, AGI_BasicSize);
        retRootGroup->profile.profile_LS = config.BtnNumber.Btn1;
        retRootGroup->groupName[27] = 0x30 + epNumber;
        (*rootGroupCount)++;

        break;
      case BTN_MODE_DIMMER_1B:
        endpointAGI = (AGI_GROUP*) malloc(AGI_BasicMultilevelSize_Dimmer);
        memcpy(endpointAGI, AGI_BasicMultilevel_Dimmer, AGI_BasicMultilevelSize_Dimmer);
        endpointAGI->profile.profile_LS = config.BtnNumber.Btn1;
        AGI_ResourceGroupSetup(endpointAGI, (AGI_BasicMultilevelSize_Dimmer / sizeof(AGI_GROUP)), epNumber);

        memcpy(retRootGroup, AGI_BasicMultilevel_Dimmer, AGI_BasicMultilevelSize_Dimmer);
        retRootGroup->profile.profile_LS = config.BtnNumber.Btn1;
        retRootGroup->groupName[28] = 0x30 + epNumber;
        (*rootGroupCount)++;

        break;
      case BTN_MODE_DIMMER_2B:
        break;
      case BTN_MODE_BLINDS_1B:
        break;
      case BTN_MODE_BLINDS_2B:
        endpointAGI = (AGI_GROUP*) malloc(AGI_BasicMultilevelSize_Blinds);
        memcpy(endpointAGI, AGI_BasicMultilevel_Blinds, AGI_BasicMultilevelSize_Blinds);
        endpointAGI->profile.profile_LS = config.BtnNumber.Btn1;
        AGI_ResourceGroupSetup(endpointAGI, (AGI_BasicMultilevelSize_Blinds / sizeof(AGI_GROUP)), epNumber);

        memcpy(retRootGroup, AGI_BasicMultilevel_Blinds, AGI_BasicMultilevelSize_Blinds);
        retRootGroup->profile.profile_LS = config.BtnNumber.Btn1;
        retRootGroup->groupName[28] = 0x30 + epNumber;
        (*rootGroupCount)++;

        break;
      case BTN_MODE_BLINDS_SHADE:
        endpointAGI = (AGI_GROUP*) malloc(AGI_BasicMultilevelSize_Shade);
        memcpy(endpointAGI, AGI_BasicMultilevel_Shade, AGI_BasicMultilevelSize_Shade);
        endpointAGI->profile.profile_LS = config.BtnNumber.Btn1;
        AGI_ResourceGroupSetup(endpointAGI, (AGI_BasicMultilevelSize_Shade / sizeof(AGI_GROUP)), epNumber);

        memcpy(retRootGroup, AGI_BasicMultilevel_Shade, AGI_BasicMultilevelSize_Shade);
        retRootGroup->profile.profile_LS = config.BtnNumber.Btn1;
        retRootGroup->groupName[27] = 0x30 + epNumber;
        (*rootGroupCount)++;

        break;
      case BTN_MODE_CENTRAL_SCENE_PUSH:
      case BTN_MODE_CENTRAL_SCENE_TOGGLE:
      case BTN_MODE_CENTRAL_SCENE_MASTER_ALL_ONOFF:
        //! No additional AGI groups for these modes
        break;
      default:
        //! Should not be here
        break;
    }
  }

  /**
   * Initialization endpoint's information
   */
  memset(endpointInfo, 0x00, sizeof(EndpointInfo_t));
  memcpy(&endpointInfo->config, &config, sizeof(EndpointConfig_t));

  endpointInfo->number = epNumber;
  epMode = endpointInfo->config.EpMode;
  btnMode = endpointInfo->config.BtnMode;

  switch (epMode) {
    case EP_MODE_REMOTE_SWITCH:
      if(btnMode == BTN_MODE_DIMMER_1B || btnMode == BTN_MODE_DIMMER_2B) {
        endpointInfo->dimmerStatus.onSwitchLvl = DEFAULT_MAX_DIMMING_VALUE;
      }
      break;
    case EP_MODE_RELAY_SWITCH:
      if (endpointInfo->config.OffOnState > OffState)
        {
          TouchContorller_SwitchOnOff(TOUCH_BUTTON_MAX,TOUCH_BTN_PUSH_EVENT,endpointInfo);
        }
     break;
    case EP_MODE_DIMMER_SWITCH:
      if (endpointInfo->config.OffOnState > OffState)
        {
          endpointInfo->dimmerStatus.onSwitchLvl = endpointInfo->config.OffOnState;
          endpointInfo->dimmerStatus.lvlChangeDir = LVL_CHANGE_DIR_DOWN;
//          TouchContorller_Dimmer_1B(TOUCH_BUTTON_MAX,TOUCH_BTN_PUSH_EVENT,endpointInfo);
        }
      else
        {
          endpointInfo->dimmerStatus.onSwitchLvl = DEFAULT_MAX_DIMMING_VALUE;
          endpointInfo->dimmerStatus.lvlChangeDir = LVL_CHANGE_DIR_DOWN;
        }
      break;
    case EP_MODE_BLINDS_SWITCH:
      endpointInfo->blindsStatus.state = BLINDS_STATE_CLOSE;
      break;
    case EP_MODE_BOILER_SWITCH:
      endpointInfo->boilerStatus.state = SWITCH_STATE_OFF;
      endpointInfo->boilerStatus.isTimerActive = false;
      endpointInfo->boilerStatus.tiksCount = 0;
      break;
    case EP_MODE_FAN_SWITCH:
      if (endpointInfo->config.OffOnState > OffState)
        {
          Boiler_KeyPress(endpointInfo->config.OffOnState,TOUCH_BTN_SHORT_PRESS_EVENT,endpointInfo);
        }
      else
        {
          endpointInfo->fanStatus.state = FAN_STATE_OFF;
        }
      break;
    default:
      break;
  }

  epCount++; //! Increment the count of successfully initialized EP

  exit: return epCount;
}

/**
 * @brief Initialization of endpoints' NIF and AGI according
 *        to DeviceConfiguration
 *
 * @details This function must be called after Transport_OnApplicationInitSW
 */

AGI_GROUP rootGroups[EP_NUM+1];
const AGI_GROUP AGI_BasicRootGeneral[] = {
  {
    {
      ASSOCIATION_GROUP_INFO_REPORT_PROFILE_GENERAL,
      ASSOCIATION_GROUP_INFO_REPORT_PROFILE_GENERAL_NA
    },
    2,
    {
      { COMMAND_CLASS_BASIC_V2, BASIC_SET_V2 },
      { COMMAND_CLASS_SWITCH_MULTILEVEL, SWITCH_MULTILEVEL_SET }
    },
    "Multichannel Command Encap Basic Report"
  }
};

const size_t AGI_BasicRootGeneralSize = sizeof(AGI_BasicRootGeneral);


void EndpointModule_Init(ASSOCIATION_ROOT_GROUP_MAPPING rgMap[], uint8_t rpMapSize)
{
  UNUSED(rpMapSize);
  configErrCode_t configRetCode = CONFIG_OK;
  EndpointConfig_t epConfig = { 0 };
  uint8_t count = 0;

  uint8_t totalActive = 1;
  for (int epNumber = ENDPOINT_1; epNumber < EP_NUM; ++epNumber)
  {
    configRetCode = DeviceConfiguration_Get_Ep(epNumber, &epConfig);
    if (configRetCode != CONFIG_OK)
      break;

    count = EndpointModule_BuildEp(epConfig, &rootGroups[totalActive], &totalActive);
  }

  memcpy(&rootGroups[0], AGI_BasicRootGeneral, AGI_BasicRootGeneralSize);
  AGI_ResourceGroupSetup(rootGroups, totalActive, ENDPOINT_ROOT);
  EndpointFunctionality.bits.nbrIndividualEndpoints = count;

  for (count = 1 ; count < totalActive ; count++)
  {
    rgMap[count].rootGrpId = count + 2;
    rgMap[count].endpointGrpId = rootGroups[count].profile.profile_LS + 1;
    rgMap[count].endpoint = ENDPOINT_ROOT;
  }

  Transport_AddEndpointSupport(&EndpointFunctionality, endpointNIF_list, sizeof(endpointNIF_list));
}

/**
 * @brief Get endpoint info by button number
 *
 * @param button - button number of endpoint sought
 * @return Pointer to EndpointInfo_t structure
 *         NULL - search result failed
 */
EndpointInfo_t*
EndpointInfo_Get_byBtn(BtnNumber_t button)
{
  EndpointInfo_t *epInfo = NULL;

  if(button > BTN_NUMBER_MAX || button == BTN_NUMBER_NONE)
    return NULL;

  for(int ep = 0; ep < EP_NUM; ++ep) {
    epInfo = &EndpointInfo_list[ep];
    if(epInfo->config.BtnNumber.Btn1 == button)
      return epInfo;
    else if(epInfo->config.BtnNumber.Btn2 == button)
      return epInfo;
  }
  //! There is no ep with such button number
  return NULL;
}

/**
 * @brief Get endpoint info by endpoint number
 *
 * @param number - number of endpoint sought
 * @return Pointer to EndpointInfo_t structure
 *         NULL - search result failed
 */
EndpointInfo_t*
EndpointInfo_Get_byNumber(uint8_t number)
{
  EndpointInfo_t *epInfo = NULL;

  if(number > EP_NUM)
    return NULL;

  for(int ep = 0; ep <= EP_NUM; ep++) {
    epInfo = &EndpointInfo_list[ep];
    if(epInfo->number == number)
      return epInfo;
  }
  //! There is no ep with such number
  return NULL;
}
