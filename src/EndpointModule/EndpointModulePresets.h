/**
 * @file EndpointModulePresets.h
 * @brief This file contains predefined structures thats used in
 *        EndpointModule.c
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */
#ifndef ENDPOINT_MODULE_PRESETS_H
#define ENDPOINT_MODULE_PRESETS_H

#include <ZW_TransportLayer.h>
#include "agi.h"

extern const EP_NIF RelaySwitch_NIF;
extern const cc_group_t RelaySwitch_LifeLineList[];
extern const size_t RelaySwitch_LifeLineListSize;

extern const EP_NIF DimmerSwitch_NIF;
extern const cc_group_t DimmerSwitch_LifeLineList[];
extern const size_t DimmerSwitch_LifeLineListSize;

extern const EP_NIF BlindsSwitch_NIF;
extern const cc_group_t BlindsSwitch_LifeLineList[];
extern const size_t BlindsSwitch_LifeLineListSize;

extern const EP_NIF RemoteSwitch_NIF;
extern const cc_group_t RemoteSwitch_LifeLineList[];
extern const size_t RemoteSwitch_LifeLineListSize;

extern const EP_NIF CentralScene_NIF;
extern const cc_group_t CentralScene_LifeLineList[];
extern const size_t CentralScene_LifeLineListSize;

extern const AGI_GROUP AGI_Basic[];
extern const size_t AGI_BasicSize;

extern const AGI_GROUP AGI_BasicMultilevel_Shade[];
extern const size_t AGI_BasicMultilevelSize_Shade;

extern const AGI_GROUP AGI_BasicMultilevel_Blinds[];
extern const size_t AGI_BasicMultilevelSize_Blinds;

extern const AGI_GROUP AGI_BasicMultilevel_Dimmer[];
extern const size_t AGI_BasicMultilevelSize_Dimmer;

#endif //! ENDPOINT_MODULE_PRESETS_H
