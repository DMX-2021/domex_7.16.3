/**
 * @file EndpointModulePresets.c
 * @brief This file contains predefined structures thats used in
 *        EndpointModule.c
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#include <ZW_TransportLayer.h>
#include "agi.h"
#include <SizeOf.h>
/*
#define DEFAULT_CC_LIST_NON_SECURE_NON_INCLUDED \
  COMMAND_CLASS_ASSOCIATION_V3, \
  COMMAND_CLASS_ASSOCIATION_GRP_INFO_V3,\
  COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION_V4,\
  COMMAND_CLASS_SUPERVISION, \
  COMMAND_CLASS_ZWAVEPLUS_INFO_V2, \
  COMMAND_CLASS_SWITCH_ALL

#define DEFAULT_CC_LIST_NON_SECURE_INCLUDED_SECURE \
  COMMAND_CLASS_ASSOCIATION_V3, \
  COMMAND_CLASS_ASSOCIATION_GRP_INFO_V3,\
  COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION_V4,\
  COMMAND_CLASS_SUPERVISION, \
  COMMAND_CLASS_ZWAVEPLUS_INFO_V2, \
  COMMAND_CLASS_SWITCH_ALL

#define DEFAULT_CC_LIST__SECURE \
  COMMAND_CLASS_ASSOCIATION_V3, \
  COMMAND_CLASS_ASSOCIATION_GRP_INFO_V3,\
  COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION_V4,\
  COMMAND_CLASS_SUPERVISION, \
  COMMAND_CLASS_ZWAVEPLUS_INFO_V2, \
  COMMAND_CLASS_SWITCH_ALL
*/
#define DEFAULT_CC_LIST_NON_SECURE_NON_INCLUDED \
  COMMAND_CLASS_ZWAVEPLUS_INFO_V2, \
  COMMAND_CLASS_ASSOCIATION, \
  COMMAND_CLASS_ASSOCIATION_GRP_INFO, \
  COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION_V2, \
  COMMAND_CLASS_SUPERVISION, \
  COMMAND_CLASS_SECURITY, \
  COMMAND_CLASS_SECURITY_2

#define DEFAULT_CC_LIST_NON_SECURE_INCLUDED_SECURE \
  COMMAND_CLASS_ZWAVEPLUS_INFO_V2, \
  COMMAND_CLASS_SECURITY, \
  COMMAND_CLASS_SECURITY_2, \
  COMMAND_CLASS_SUPERVISION

#define DEFAULT_CC_LIST__SECURE \
  COMMAND_CLASS_ASSOCIATION, \
  COMMAND_CLASS_ASSOCIATION_GRP_INFO, \
  COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION_V2

/****************************************************************************/
/** Relay Switch                                                            */
/****************************************************************************/

static uint8_t RelaySwitch_NonSecureNotIncluded[] = {
  DEFAULT_CC_LIST_NON_SECURE_NON_INCLUDED,
  COMMAND_CLASS_SWITCH_BINARY_V2
};

static uint8_t RelaySwitch_NonSecureIncludedSecure[] = {
  DEFAULT_CC_LIST_NON_SECURE_INCLUDED_SECURE,
  COMMAND_CLASS_SWITCH_BINARY_V2
};

static uint8_t RelaySwitch_Secure[] = {
    DEFAULT_CC_LIST__SECURE,
    COMMAND_CLASS_SWITCH_BINARY_V2
};

const EP_NIF RelaySwitch_NIF = {
  GENERIC_TYPE_SWITCH_BINARY,
  SPECIFIC_TYPE_POWER_SWITCH_BINARY,
  {
    { RelaySwitch_NonSecureNotIncluded,
      sizeof(RelaySwitch_NonSecureNotIncluded) },
    {
        { RelaySwitch_NonSecureIncludedSecure,
          sizeof(RelaySwitch_NonSecureIncludedSecure) },
        { RelaySwitch_Secure,
          sizeof(RelaySwitch_Secure) }
    }
  }
};

const cc_group_t RelaySwitch_LifeLineList[] = {
  {
    COMMAND_CLASS_SWITCH_BINARY,
    SWITCH_BINARY_REPORT
  }
};

const size_t RelaySwitch_LifeLineListSize = sizeof_array(
    RelaySwitch_LifeLineList);

/****************************************************************************/
/** Dimmer Switch                                                           */
/****************************************************************************/

static uint8_t DimmerSwitch_NonSecureNotIncluded[] = {
  DEFAULT_CC_LIST_NON_SECURE_NON_INCLUDED,
  COMMAND_CLASS_SWITCH_MULTILEVEL
};

static uint8_t DimmerSwitch_NonSecureIncludedSecure[] = {
  DEFAULT_CC_LIST_NON_SECURE_INCLUDED_SECURE,
  COMMAND_CLASS_SWITCH_MULTILEVEL
};

static uint8_t DimmerSwitch_Secure[] = {
    DEFAULT_CC_LIST__SECURE,
    COMMAND_CLASS_SWITCH_MULTILEVEL
};

const EP_NIF DimmerSwitch_NIF = {
  GENERIC_TYPE_SWITCH_MULTILEVEL,
  SPECIFIC_TYPE_POWER_SWITCH_MULTILEVEL,
  {
    { DimmerSwitch_NonSecureNotIncluded,
      sizeof(DimmerSwitch_NonSecureNotIncluded) },
    {
        { DimmerSwitch_NonSecureIncludedSecure,
          sizeof(DimmerSwitch_NonSecureIncludedSecure) },
        { DimmerSwitch_Secure,
          sizeof(DimmerSwitch_Secure) }
    }
  }
};

const cc_group_t DimmerSwitch_LifeLineList[] = {
  {
    COMMAND_CLASS_SWITCH_MULTILEVEL,
    SWITCH_MULTILEVEL_REPORT
  }
};

const size_t DimmerSwitch_LifeLineListSize = sizeof_array(
    DimmerSwitch_LifeLineList);

/****************************************************************************/
/** Blinds Switch                                                           */
/****************************************************************************/

static uint8_t BlindsSwitch_NonSecureNotIncluded[] = {
  DEFAULT_CC_LIST_NON_SECURE_NON_INCLUDED,
  COMMAND_CLASS_SWITCH_MULTILEVEL,
  COMMAND_CLASS_SWITCH_BINARY_V2
};

static uint8_t BlindsSwitch_NonSecureIncludedSecure[] = {
  DEFAULT_CC_LIST_NON_SECURE_INCLUDED_SECURE,
  COMMAND_CLASS_SWITCH_MULTILEVEL,
  COMMAND_CLASS_SWITCH_BINARY_V2
};

static uint8_t BlindsSwitch_Secure[] = {
    DEFAULT_CC_LIST__SECURE,
    COMMAND_CLASS_SWITCH_MULTILEVEL,
    COMMAND_CLASS_SWITCH_BINARY_V2
};

const EP_NIF BlindsSwitch_NIF =
{
  GENERIC_TYPE_SWITCH_MULTILEVEL,
  SPECIFIC_TYPE_CLASS_A_MOTOR_CONTROL,
  {
    { BlindsSwitch_NonSecureNotIncluded,
      sizeof(BlindsSwitch_NonSecureNotIncluded) },
    {
        { BlindsSwitch_NonSecureIncludedSecure,
          sizeof(BlindsSwitch_NonSecureIncludedSecure) },
        { BlindsSwitch_Secure,
          sizeof(BlindsSwitch_Secure) }
    }
  }
};

const cc_group_t BlindsSwitch_LifeLineList[] = {
  {
    COMMAND_CLASS_SWITCH_MULTILEVEL,
    SWITCH_MULTILEVEL_REPORT
  }
};

const size_t BlindsSwitch_LifeLineListSize = sizeof_array(
    BlindsSwitch_LifeLineList);

/****************************************************************************/
/** Remote Switch                                                           */
/****************************************************************************/

static uint8_t RemoteSwitch_NonSecureNotIncluded[] =
{ DEFAULT_CC_LIST_NON_SECURE_NON_INCLUDED };

static uint8_t RemoteSwitch_NonSecureIncludedSecure[] =
{ DEFAULT_CC_LIST_NON_SECURE_INCLUDED_SECURE };

static uint8_t RemoteSwitch_Secure[] = { };

const EP_NIF RemoteSwitch_NIF =
{
  GENERIC_TYPE_SWITCH_REMOTE,
  SPECIFIC_TYPE_NOT_USED,
  {
    { RemoteSwitch_NonSecureNotIncluded,
      sizeof(RemoteSwitch_NonSecureNotIncluded) },
    {
        { RemoteSwitch_NonSecureIncludedSecure,
          sizeof(RemoteSwitch_NonSecureIncludedSecure) },
        { RemoteSwitch_Secure,
          sizeof(RemoteSwitch_Secure) }
    }
  }
};

const cc_group_t RemoteSwitch_LifeLineList[] = {
  {
    COMMAND_CLASS_NO_OPERATION,
    NO_OPERATION_VERSION
  }
};

const size_t RemoteSwitch_LifeLineListSize = sizeof_array(
    RemoteSwitch_LifeLineList);

/****************************************************************************/
/** Central Scene                                                           */
/****************************************************************************/

static uint8_t CentralScene_NonSecureNotIncluded[] =
{
  DEFAULT_CC_LIST_NON_SECURE_NON_INCLUDED,
  COMMAND_CLASS_CENTRAL_SCENE_V2
};

static uint8_t CentralScene_NonSecureIncludedSecure[] =
{
  DEFAULT_CC_LIST_NON_SECURE_INCLUDED_SECURE,
  COMMAND_CLASS_CENTRAL_SCENE_V2
};

static uint8_t CentralScene_Secure[] = { };

const EP_NIF CentralScene_NIF =
{
  GENERIC_TYPE_SWITCH_REMOTE,
  SPECIFIC_TYPE_NOT_USED,
  {
    { CentralScene_NonSecureNotIncluded,
      sizeof(CentralScene_NonSecureNotIncluded) },
    {
        { CentralScene_NonSecureIncludedSecure,
          sizeof(CentralScene_NonSecureIncludedSecure) },
        { CentralScene_Secure,
          sizeof(CentralScene_Secure) }
    }
  }
};

const cc_group_t CentralScene_LifeLineList[] = {
  {
      COMMAND_CLASS_CENTRAL_SCENE_V3,
      CENTRAL_SCENE_NOTIFICATION_V3
  }
};

const size_t CentralScene_LifeLineListSize = sizeof_array(
    RemoteSwitch_LifeLineList);

/****************************************************************************/
/** Association Group Info                                                  */
/****************************************************************************/

const AGI_GROUP AGI_Basic[]  = {
  {
    { ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL, ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL_KEY01 },
    1,
    { { COMMAND_CLASS_BASIC_V2, BASIC_SET_V2 } },
    "DOMEX On/Off Switch EP No. X"
  }
};

const size_t AGI_BasicSize = sizeof(AGI_Basic);

const AGI_GROUP AGI_BasicMultilevel_Shade[] = {
  {
    {
      ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL,
      ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL_KEY01
    },
    4,
    {
      { COMMAND_CLASS_BASIC_V2, BASIC_SET_V2 },
      { COMMAND_CLASS_SWITCH_MULTILEVEL, SWITCH_MULTILEVEL_SET }//,
//      { COMMAND_CLASS_SWITCH_MULTILEVEL, SWITCH_MULTILEVEL_START_LEVEL_CHANGE },
//      { COMMAND_CLASS_SWITCH_MULTILEVEL, SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE }
    },
    "DOMEX Shade Control EP No. X"
  }
};

const size_t AGI_BasicMultilevelSize_Shade = sizeof(AGI_BasicMultilevel_Shade);

const AGI_GROUP AGI_BasicMultilevel_Blinds[] = {
  {
    {
      ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL,
      ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL_KEY01
    },
    4,
    {
      { COMMAND_CLASS_BASIC_V2, BASIC_SET_V2 },
      { COMMAND_CLASS_SWITCH_MULTILEVEL, SWITCH_MULTILEVEL_SET }//,
      //      { COMMAND_CLASS_SWITCH_MULTILEVEL, SWITCH_MULTILEVEL_START_LEVEL_CHANGE },
      //      { COMMAND_CLASS_SWITCH_MULTILEVEL, SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE }
    },
    "DOMEX Blinds Control EP No. X"
  }
};

const size_t AGI_BasicMultilevelSize_Blinds = sizeof(AGI_BasicMultilevel_Blinds);

const AGI_GROUP AGI_BasicMultilevel_Dimmer[] = {
  {
    {
      ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL,
      ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL_KEY01
    },
    4,
    {
      { COMMAND_CLASS_BASIC_V2, BASIC_SET_V2 },
      { COMMAND_CLASS_SWITCH_MULTILEVEL, SWITCH_MULTILEVEL_SET }//,
      //      { COMMAND_CLASS_SWITCH_MULTILEVEL, SWITCH_MULTILEVEL_START_LEVEL_CHANGE },
      //      { COMMAND_CLASS_SWITCH_MULTILEVEL, SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE }
    },
    "DOMEX Dimmer Control EP No. X"
  }
};

const size_t AGI_BasicMultilevelSize_Dimmer = sizeof(AGI_BasicMultilevel_Dimmer);

