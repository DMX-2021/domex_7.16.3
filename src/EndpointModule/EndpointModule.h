/**
 * @file EndpointModule.h
 * @brief Module helps manage Endpoints, builds device NIF
 *        by device configurations
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#ifndef ENDPOINT_MODULE_H
#define ENDPOINT_MODULE_H

#include "ConfigurationModule.h"
#include <ZW_TransportLayer.h>
#include "association_plus.h"

typedef enum {
  STATE_OFF = 0,
  STATE_CLOSE = 0,
  STATE_ON = 1,
  STATE_OPEN = 1
} state_t;

typedef enum
{
  SWITCH_STATE_OFF,
  SWITCH_STATE_ON,
  SWITCH_STATE_UNKNOWN
} switchState_t;
typedef enum
{
  FAN_STATE_OFF,
  FAN_STATE_LOW,
  FAN_STATE_MID,
  FAN_STATE_HIGH,
  FAN_STATE_UNKNOWN
} fanState_t;

typedef enum
{
  BLINDS_STATE_CLOSE,
  BLINDS_STATE_OPEN,
  BLINDS_STATE_UNKNOWN
} blindsState_t;

typedef enum {
  LVL_CHANGE_DIR_UP,
  LVL_CHANGE_DIR_DOWN
} lvlChangeDir_t;

typedef struct
{
  DimmerCalibrationStatus_t CalibrationStatus;
  uint8_t onSwitchLvl;
  uint8_t targetLvl;
  uint8_t DurationLvl;
  DimmerDirection_t DimmerDirection;
  lvlChangeDir_t lvlChangeDir;
} dimmerSwitchStatus_t;

typedef struct
{
  switchState_t state;
} relaySwitchStatus_t;

typedef struct
{
  blindsState_t state;
  bool isLvlChanging;
  uint32_t tiksCount;
} blindsSwitchStatus_t;

typedef struct
{
  switchState_t state;
  bool isTimerActive;
  uint32_t tiksCount;
} boilerSwitchStatus_t;

typedef struct
{
  fanState_t state;
} fanSwitchStatus_t;

typedef struct
{
  switchState_t state;
} centraSceneToggleStatus_t;

/**
 * Endpoint's information structure
 */
typedef struct
{
  uint8_t number;
  union
  {
    relaySwitchStatus_t relayStatus;
    dimmerSwitchStatus_t dimmerStatus;
    blindsSwitchStatus_t blindsStatus;
    boilerSwitchStatus_t boilerStatus;
    fanSwitchStatus_t fanStatus;
    centraSceneToggleStatus_t centraSceneToggleStatus;
  };
  EndpointConfig_t config;
} EndpointInfo_t;

/**
 * @brief Initialization of endpoints' NIF and AGI according
 *        to DeviceConfiguration
 *
 * @details This function must be called after Transport_OnApplicationInitSW
 */
void EndpointModule_Init(ASSOCIATION_ROOT_GROUP_MAPPING rgMap[], uint8_t rpMapSize);

/**
 * @brief Get endpoint info by button number
 *
 * @param button - button number of endpoint sought
 * @return Pointer to EndpointInfo_t structure
 *         NULL - search result failed
 */
EndpointInfo_t*
EndpointInfo_Get_byBtn(BtnNumber_t button);

/**
 * @brief Get endpoint info by endpoint number
 *
 * @param number - number of endpoint sought
 * @return Pointer to EndpointInfo_t structure
 *         NULL - search result failed
 */
EndpointInfo_t*
EndpointInfo_Get_byNumber(uint8_t number);

#endif //!ENDPOINT_MODULE_H
