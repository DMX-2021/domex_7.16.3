/**
 * @file ConfigurationModule.h
 * @brief Module helps manage device configurations
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#ifndef CONFIGURATION_MODULE_H
#define CONFIGURATION_MODULE_H

#include <stdbool.h>
#include "config_app.h"
#include "association_plus.h"
#include "ZW_radio_api.h"

#define EP_NUM                          NUMBER_OF_ENDPOINTS
//ASSOCIATION_ROOT_GROUP_MAPPING m_rootGroupMapping[EP_NUM+1];

typedef enum {
  CONFIG_OK = 0,
  CONFIG_ERR,
  CONFIG_NULL_PTR,
  CONFIG_HW_UNKNOWN,
  CONFIG_SW_INVALID,
  CONFIG_SW_INTENDED_FOR_HW,
  CONFIG_EP_INVALID,
  CONFIG_BTN_MODE_INVALID,
  CONFIG_BTN_MODE_NOT_INTENDED_FOR_EP_MODE,
  CONFIG_BTN_HIGHLIGHT_INVALID,
  CONFIG_BTN_NUM_INVALID,
  CONFIG_BTN_NUM_EMPTY,
  CONFIG_BTN_NUM_EQUAL,
  CONFIG_OUTPUT_CH_INVALID,
  CONFIG_OUTPUT_CH_EMPTY,
  CONFIG_OUTPUT_CH_EQUAL
} configErrCode_t;

typedef enum
{
  HW_CONFIG_WATER_HEATER = 0x0,
  HW_CONFIG_LIGHT,
  HW_CONFIG_DIMMER,
  HW_CONFIG_MAX,
  HW_CONFIG_UNKNOWN = 0xFF
} HW_Config_t;

typedef enum
{
  SW_CONFIG_CUSTOM = 0x00,
  SW_CONFIG_1L,
  SW_CONFIG_2L,
  SW_CONFIG_3L,
  SW_CONFIG_4L,
  SW_CONFIG_6L,
  SW_CONFIG_1DT,
  SW_CONFIG_2DT,
  SW_CONFIG_3DT,
  SW_CONFIG_4DT,
  SW_CONFIG_1D,
  SW_CONFIG_2D,
  SW_CONFIG_4DT2D,
  SW_CONFIG_1M,
  SW_CONFIG_1SH,
  SW_CONFIG_2M,
  SW_CONFIG_2SH,
  SW_CONFIG_3M,
  SW_CONFIG_3SH,
  SW_CONFIG_4L1M,
  SW_CONFIG_2L1M,
  SW_CONFIG_2L2M,
  SW_CONFIG_4L1SH,
  SW_CONFIG_2L1SH,
  SW_CONFIG_2L2SH,
  SW_CONFIG_B,
  SW_CONFIG_9R,
  SW_CONFIG_F,
  SW_CONFIG_MAX
} SW_Config_t;

typedef enum
{
  EP_MODE_DISABLE = 0x00,
  EP_MODE_REMOTE_SWITCH,
  EP_MODE_RELAY_SWITCH,
  EP_MODE_DIMMER_SWITCH,
  EP_MODE_BLINDS_SWITCH,
  EP_MODE_BOILER_SWITCH,
  EP_MODE_FAN_SWITCH,
  EP_MODE_MAX
} EpMode_t;

typedef enum
{
  BTN_MODE_DISABLE = 0x00,
  BTN_MODE_SWITCH_ON_OFF,
  BTN_MODE_PUSH,
  BTN_MODE_DIMMER_1B,
  BTN_MODE_DIMMER_2B,
  BTN_MODE_BLINDS_1B,
  BTN_MODE_BLINDS_2B,
  BTN_MODE_CENTRAL_SCENE_PUSH,
  BTN_MODE_CENTRAL_SCENE_TOGGLE,
  BTN_MODE_BLINDS_SHADE,
  BTN_MODE_CENTRAL_SCENE_MASTER_ALL_ONOFF,
  BTN_MODE_MAX
} BtnMode_t;

typedef enum
{
  BTN_NUMBER_NONE = 0x00,
  BTN_NUMBER_1,
  BTN_NUMBER_2,
  BTN_NUMBER_3,
  BTN_NUMBER_4,
  BTN_NUMBER_5,
  BTN_NUMBER_6,
  BTN_NUMBER_7,
  BTN_NUMBER_8,
  BTN_NUMBER_9,
  BTN_NUMBER_MAX
} BtnNumber_t;

typedef enum
{
  OUTPUT_CH_NONE = 0x00,
  OUTPUT_CH_1,
  OUTPUT_CH_2,
  OUTPUT_CH_3,
  OUTPUT_CH_4,
  OUTPUT_CH_5,
  OUTPUT_CH_6,
  OUTPUT_CH_MAX,
} OutputCh_t;

typedef uint8_t SceneNumber_t;

typedef enum {
  SCENE_MASTER_ALL_OFF,
  SCENE_MASTER_ALL_ON = 0xFF
} SceneMasterAll_t;

typedef enum
{
  ON_EXCLUDE_OFF_EXCLUDE = 0x0,
  ON_EXCLUDE_OFF_INCLUDE,
  ON_INCLUDE_OFF_EXCLUDE,
  ON_INCLUDE_OFF_INCLUDE=0xFF,
} Switch_All_State_t;


typedef struct
{
  EpMode_t EpMode;
  BtnMode_t BtnMode;
  union
  {
    struct
    {
      BtnNumber_t Btn1 :4;
      BtnNumber_t Btn2 :4;
    };
    BtnNumber_t Btn;
  } BtnNumber;
  union
  {
    struct
    {
      OutputCh_t Output1 :4;
      OutputCh_t Output2 :4;
    };
    OutputCh_t Output;
  };
  Switch_All_State_t EpSwitchAllState;
  uint8_t ZAF_TSE_Timeout;
  uint8_t OffOnState;
  SceneMasterAll_t SceneMasterAll;
} EndpointConfig_t;
typedef enum {UpDirection,DownDirection} DimmerDirection_t;
typedef enum {NotCal,StartCal} DimmerCalibrationStatus_t;

typedef struct {
  uint8_t minDimmingValue;
  uint8_t maxDimmingValue;
  uint8_t dimmigTimeFadeUp;
  uint8_t dimmigTimeFadeDown;
} DimmerConfig_t;

typedef struct {
  uint8_t durationUpSec;
  uint8_t durationDownSec;
  uint8_t DurationShort;
  uint8_t DurationVeryShort;
} BlindsConfig_t;

typedef struct {
  uint8_t sceneToggleOnID;
  uint8_t sceneToggleOffID;
} CentralSceneToggleConfig_t;

typedef struct {
  CentralSceneToggleConfig_t sceneMasterOnOffID;
  uint16_t masterOnOffFunctionality;
} CentralSceneMasterAllOnOffConfig_t;

typedef struct {
  uint8_t maxBoilingTime;
} BoilerConfig_t;

typedef enum {
  BACKLIGHT_COLOR_BLACK = 0x0,
  BACKLIGHT_COLOR_RED,
  BACKLIGHT_COLOR_GREEN,
  BACKLIGHT_COLOR_BLUE,
  BACKLIGHT_COLOR_YELLOW,
  BACKLIGHT_COLOR_MAGENTA,
  BACKLIGHT_COLOR_CYAN,
  BACKLIGHT_COLOR_WHITE,
  BACKLIGHT_COLOR_MAX
} BacklightColor_t;

typedef struct {
  BacklightColor_t onStatusColor;
  BacklightColor_t offStatusColor;
} BacklightConfig_t;

typedef union {
  struct {
    uint8_t hapticEnable:1;
    uint8_t buzzerEnable:1;
  };
  uint8_t rawData;
} IndicationConfig_t;

typedef struct {
  ZW_Region_t radioChannel;
} RadioConfig_t;



typedef struct
{
  HW_Config_t HW_Config;
  SW_Config_t SW_Config;
  EndpointConfig_t EndpointConfig_list[EP_NUM+1];
  DimmerConfig_t DimmerConfig[EP_NUM+1];
  BlindsConfig_t BlindsConfig[EP_NUM+1];
  CentralSceneToggleConfig_t CentralSceneToggleConfig[EP_NUM+1];
  CentralSceneMasterAllOnOffConfig_t MasterAllOnOff;
  BoilerConfig_t BoilerConfig;
  BacklightConfig_t BacklightConfig[BTN_NUMBER_MAX];
  IndicationConfig_t IndicationConfig;
  Switch_All_State_t SwitchAllOnOffState;
  RadioConfig_t RadioConfig;
} DeviceConfiguration_t;

/**
 * @brief Save configuration struct which was set
 *        by DeviceConfiguration_Set(DeviceConfiguration_t* )
 *
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Commit(void);

/**
 * @brief Function resets configuration to default values
 *
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Default(void);

/**
 * @brief This function loads the application settings from non-volatile memory
 *        If no settings are found, default values are used and saved
 *
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Load(void);

/**
 * @brief Reset application FileSystem to default
 *
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Reset(void);

/**
 * @brief Get current hardware configuration
 *
 * @return Hardware configuration
 */
HW_Config_t
DeviceConfiguration_Get_HW(void);

/**
 * @brief Print device configuration structure to log output
 */
void
DeviceConfiguration_SetUp();

/**
 * @brief Get device software configuration
 * @return Device software configuration
 */
SW_Config_t
DeviceConfiguration_Get_SW(void);

/**
 * @brief Set device software configuration
 * @details Software configuration allows to set the parameters EP #2-10
 *          as preset, which speeds up the process of setting up the device
 * @note If isDefault - true, SW_Config will be ignored
 * @note If SW_Config preset applied, also applied corresponding button colors
 *
 * @param SW_Config - software configuration
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_SW(SW_Config_t SW_Config, bool isDefault);

/**
 * @brief Get Endpoint configuration by it's number
 *
 * @param epNumber - number of endpoint
 * @param pEpConfig - pointer to returned configuration
 * @return Configuration error code
 */
configErrCode_t
DeviceConfiguration_Get_Ep(uint8_t epNumber, EndpointConfig_t *pEpConfig);

/**
 * @brief Set endpoint configuration
 * @note if isDefault - true, config will be ignored
 *
 * @param epNumber - number of endpoint
 * @param config - configuration structure
 * @param isDefault - used to specify if the default value is to be restored
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Ep(uint8_t epNumber, EndpointConfig_t config,
                           bool isDefault);

/**
 * @brief Get dimmer configuration structure
 *
 * @param epNumber - End Point number
 *
 * @return dimmer configuration
 */
DimmerConfig_t
DeviceConfiguration_Get_Dimmer(uint8_t epNumber);

/**
 * @brief Set dimmer configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @param epNumber - End Point number
 *
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Dimmer(DimmerConfig_t *pConfig, uint8_t epNumber);

/**
 * @brief Get blinds configuration structure
 *
 * @param epNumber - End Point number
 *
 * @return blinds configuration
 */
BlindsConfig_t
DeviceConfiguration_Get_Blinds(uint8_t epNumber);

/**
 * @brief Set blinds configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @param epNumber - End Point number
 *
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Blinds(BlindsConfig_t *pConfig, uint8_t epNumber);

/**
 * @brief Get Central Scene Toggle configuration structure
 *
 * @return central scene toggle configuration
 */
CentralSceneToggleConfig_t
DeviceConfiguration_Get_SentralSceneToggle(uint8_t epNumber);

/**
 * @brief Set Central Scene Toggle configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_SentralSceneToggle(CentralSceneToggleConfig_t *pConfig, uint8_t epNumber);

/**
 * @brief Get Central Scene Master All On/Off configuration structure
 *
 * @return central scene Master All On/Off configuration
 */
CentralSceneMasterAllOnOffConfig_t
DeviceConfiguration_Get_SentralSceneMasterAllOnOff(void);

/**
 * @brief Set Central Scene Master All On/Off configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_SentralSceneMasterAllOnOff(CentralSceneMasterAllOnOffConfig_t *pConfig);

/**
 * @brief Get boiler configuration structure
 *
 * @return boiler configuration
 */
BoilerConfig_t
DeviceConfiguration_Get_Boiler(void);

/**
 * @brief Set boiler configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Boiler(BoilerConfig_t *pConfig);

/**
 * @brief Get button backlight configuration structure
 *
 * @param[IN] buttNumber - Button number for getting backlight configuration
 * @param[IN/OUT] pConfig - structure pointer for backlight configuration
 *
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Get_Backlight(uint8_t buttNumber, BacklightConfig_t *pConfig);

/**
 * @brief Set button backlight configuration structure
 *
 * @param[IN] buttNumber - Button number for getting backlight configuration,
 *  @note Possible values in range of BTN_NUMBER_NONE and BTN_NUMBER_MAX
 * @param[IN] pConfig - structure pointer for backlight configuration
 *
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Backlight(uint8_t buttNumber, BacklightConfig_t *pConfig);

/**
 * @brief Get indication configuration structure
 *
 * @return indication configuration
 */
IndicationConfig_t
DeviceConfiguration_Get_Indication(void);

/**
 * @brief Set indication configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Indication(IndicationConfig_t *pConfig);

/**
 * @brief Get radio configuration structure
 *
 * @return indication configuration
 */
RadioConfig_t
DeviceConfiguration_Get_Radio(void);

/**
 * @brief Set radio configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Radio(RadioConfig_t *pConfig);

Switch_All_State_t
DeviceConfiguration_Get_Switch_All(void);

configErrCode_t
DeviceConfiguration_Set_Switch_All(Switch_All_State_t pConfig);

#endif//!CONFIGURATION_MODULE_H
