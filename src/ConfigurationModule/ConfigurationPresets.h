#ifndef CONFIGURATION_PRESETS_H
#define CONFIGURATION_PRESETS_H
#define OffState 0

#include "ConfigurationModule.h"

//! @note As default configuration BL_CONFIG_L_PRESET
#define SW_CONFIG_L_BL_PRESET { .onStatusColor = BACKLIGHT_COLOR_YELLOW, .offStatusColor = BACKLIGHT_COLOR_BLUE }
#define SW_CONFIG_BOIL_BL_PRESET { .onStatusColor = BACKLIGHT_COLOR_YELLOW, .offStatusColor = BACKLIGHT_COLOR_BLUE }
#define SW_CONFIG_M_BL_PRESET { .onStatusColor = BACKLIGHT_COLOR_MAGENTA, .offStatusColor = BACKLIGHT_COLOR_BLUE }
#define SW_CONFIG_D_BL_PRESET { .onStatusColor = BACKLIGHT_COLOR_GREEN, .offStatusColor = BACKLIGHT_COLOR_BLUE }
#define SW_CONFIG_M_D_PRESET { .onStatusColor = BACKLIGHT_COLOR_MAGENTA, .offStatusColor = BACKLIGHT_COLOR_GREEN }
#define SW_CONFIG_FAN_BL_PRESET { .onStatusColor = BACKLIGHT_COLOR_YELLOW, .offStatusColor = BACKLIGHT_COLOR_BLUE }

#define SW_CONFIG_PRESET(CONFIG_STRUCT, SW_CONFIG)  \
  do {  \
    CONFIG_STRUCT.SW_Config = SW_CONFIG;  \
    memcpy(&CONFIG_STRUCT.EndpointConfig_list[1], &SW_CONFIG##_PRESET, \
            sizeof(EndpointConfig_t) * EP_NUM); \
    memcpy(&CONFIG_STRUCT.BacklightConfig[1], &SW_CONFIG##_BL_PRESET, \
            sizeof(BacklightConfig_t) * (BTN_NUMBER_MAX - BTN_NUMBER_1)); \
  } while(0)

extern const EndpointConfig_t SW_CONFIG_1L_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_2L_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_3L_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_4L_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_6L_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_1DT_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_2DT_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_3DT_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_4DT_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_1D_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_2D_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_4DT2D_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_1M_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_2M_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_3M_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_4L1M_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_2L1M_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_2L2M_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_1SH_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_2SH_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_3SH_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_4L1SH_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_2L1SH_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_2L2SH_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_9R_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_B_PRESET[EP_NUM];
extern const EndpointConfig_t SW_CONFIG_F_PRESET[EP_NUM];

extern const BacklightConfig_t SW_CONFIG_1L_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_2L_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_3L_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_4L_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_6L_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_1DT_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_2DT_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_3DT_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_4DT_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_1D_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_2D_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_4DT2D_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_1M_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_2M_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_3M_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_4L1M_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_2L1M_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_2L2M_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_1SH_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_2SH_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_3SH_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_4L1SH_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_2L1SH_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_2L2SH_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_9R_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_B_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];
extern const BacklightConfig_t SW_CONFIG_F_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1];

#endif//!CONFIGURATION_PRESETS_H
