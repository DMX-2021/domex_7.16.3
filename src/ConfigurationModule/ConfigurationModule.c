/**
 * @file ConfigurationModule.c
 * @brief Module helps manage device configurations
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#include "string.h"
#include "ConfigurationModule.h"
#include "SizeOf.h"
#include "ZAF_file_ids.h"
#include "ZAF_nvm3_app.h"
#include "ZAF_app_version.h"
#include "ZW_TransportLayer.h"
#include "nvm3.h"
#include "association_plus.h"
#include "BoardDedicatedModule.h"
#include "ConfigurationPresets.h"
#include "CC_ZWavePlusInfo.h"

#define ZAF_FILE_SIZE_DEVICE_CONFIG  sizeof(DeviceConfiguration_t)

static configErrCode_t
isBtnModeValid(EndpointConfig_t config);

static configErrCode_t
isBtnNumValid(DeviceConfiguration_t *DeviceConfig, EndpointConfig_t config);

static configErrCode_t
isOutputChValid(DeviceConfiguration_t *DeviceConfig, EndpointConfig_t config);

static nvm3_Handle_t* pFileSystemApplication;

static DeviceConfiguration_t DeviceConfiguration;
extern  SCCZWavePlusInfo CCZWavePlusInfo;

//! FileSet - allows FileSystemVerifySet to validate required files
//static const SFileDescriptor g_aFileDescriptors[] = {
//  {
//    .ObjectKey = ZAF_FILE_ID_USERCODE,
//    .iDataSize = ZAF_FILE_SIZE_DEVICE_CONFIG
//  },
//  {
//    .ObjectKey = ZAF_FILE_ID_ASSOCIATIONINFO,
//    .iDataSize = ZAF_FILE_SIZE_ASSOCIATIONINFO
//  },
//  {
//    .ObjectKey = ZAF_FILE_ID_APP_VERSION,
//    .iDataSize = ZAF_FILE_SIZE_APP_VERSION
//  }
//};
//
//static EFileSysVerifyStatus g_aFileStatus[sizeof_array(g_aFileDescriptors)];

/**
 * @brief Function resets configuration to default values
 */
configErrCode_t
DeviceConfiguration_Set_Default(void)
{
  configErrCode_t ret = CONFIG_OK;
  Ecode_t errCode;

  AssociationInit(true, pFileSystemApplication);

  uint32_t appVersion = ZAF_GetAppVersion();
  errCode = nvm3_writeData(pFileSystemApplication, ZAF_FILE_ID_APP_VERSION, &appVersion, ZAF_FILE_SIZE_APP_VERSION);
  ASSERT(ECODE_NVM3_OK == errCode);
  if(ECODE_NVM3_OK != errCode) {
    ret = CONFIG_ERR;
    goto exit;
  }

  DimmerConfig_t dimmerConfig = {
      .minDimmingValue = DEFAULT_MIN_DIMMING_VALUE,
      .maxDimmingValue = DEFAULT_MAX_DIMMING_VALUE,
      .dimmigTimeFadeUp = DEFAULT_DIMMING_TIME_FADE_UP,
      .dimmigTimeFadeDown = DEFAULT_DIMMING_TIME_FADE_DOWN
  };

  BlindsConfig_t blindsConfig = {
      .durationUpSec = DEFAULT_BLINDS_DURATION_UP,
      .durationDownSec = DEFAULT_BLINDS_DURATION_DOWN,
      .DurationShort = DEFAULT_SHADE_DURATION_SHORT,
      .DurationVeryShort = DEFAULT_SHADE_DURATION_VERY_SHORT
  };

  BoilerConfig_t boilerConfig = {
      .maxBoilingTime = DEFAULT_MAX_BOILING_TIME
  };

  IndicationConfig_t indicationConfig = {
      .hapticEnable = DEFAULT_HAPTIC_EN,
      .buzzerEnable = DEFAULT_BUZZER_EN
  };

  RadioConfig_t radioConfig = {
      .radioChannel = DEFAULT_RADIO_CHANNEL
  };

  CentralSceneToggleConfig_t centralSceneToggleConfig = {
      .sceneToggleOnID = DEFAULT_CENTRAL_SCENE_ID_TOGGLE_ON,
      .sceneToggleOffID = DEFAULT_CENTRAL_SCENE_ID_TOGGLE_OFF
  };

  CentralSceneMasterAllOnOffConfig_t centralSceneMasterAllOnOffConfig = {
      .masterOnOffFunctionality = DEFAULT_CENTRAL_SCENE_MASTER_ALL_ONOFF_FUNC_MASK,
      .sceneMasterOnOffID.sceneToggleOnID = DEFAULT_CENTRAL_SCENE_ID_MASTER_ALL_ON,
      .sceneMasterOnOffID.sceneToggleOffID = DEFAULT_CENTRAL_SCENE_ID_MASTER_ALL_OFF
  };

  Switch_All_State_t SwitchAllOnOffStateConfig = ON_INCLUDE_OFF_INCLUDE;

  //! Write default device configurations
  ret = DeviceConfiguration_Set_SW(0, true);

  //! Set Dimmer cfg for all EP
  for (int i = ENDPOINT_1; i <= EP_NUM; i++) {
    DeviceConfiguration_Set_Dimmer(&dimmerConfig, i);
    DeviceConfiguration_Set_Blinds(&blindsConfig, i);
    DeviceConfiguration_Set_SentralSceneToggle(&centralSceneToggleConfig, i);
  }

  DeviceConfiguration_Set_SentralSceneMasterAllOnOff(&centralSceneMasterAllOnOffConfig);
  DeviceConfiguration_Set_Boiler(&boilerConfig);
  DeviceConfiguration_Set_Indication(&indicationConfig);
  DeviceConfiguration_Set_Radio(&radioConfig);
  DeviceConfiguration_Set_Switch_All(SwitchAllOnOffStateConfig);
  DeviceConfiguration_Commit();

  exit: return ret;
}

/**
 * @brief This function loads the application settings from non-volatile memory
 *        If no settings are found, default values are used and saved
 */

configErrCode_t
DeviceConfiguration_Load(void)
{
  configErrCode_t ret = CONFIG_OK;
  Ecode_t errCode;
  HW_Config_t HW_Config = HW_CONFIG_UNKNOWN;


  //! Init file system
  if(ApplicationFileSystemInit(&pFileSystemApplication) == false) {
     ret = CONFIG_ERR;
    goto exit;
  }

  uint32_t appVersion;
  Ecode_t versionFileStatus = nvm3_readData(pFileSystemApplication, ZAF_FILE_ID_APP_VERSION, &appVersion, ZAF_FILE_SIZE_APP_VERSION);

  if (versionFileStatus == ECODE_NVM3_OK ) {


    //! Initialize association module
    AssociationInit(false, pFileSystemApplication);

    //! Read device configurations
    memset(&DeviceConfiguration, 0x0, ZAF_FILE_SIZE_DEVICE_CONFIG);


    errCode = nvm3_readData(pFileSystemApplication, FILE_ID_APPLICATIONDATA, &DeviceConfiguration, ZAF_FILE_SIZE_DEVICE_CONFIG);
    //ASSERT(ECODE_NVM3_OK == errCode);
    if(ECODE_NVM3_OK != errCode) {
      ret = CONFIG_ERR;
      goto exit;
    }

    HW_Config = DeviceConfiguration_Get_HW();
    if(DeviceConfiguration.HW_Config != HW_Config) {
      DeviceConfiguration_Set_Default();
//      Board_Default_Handler();
    }
  }
  else {
    uint32_t objectType;
    size_t dataLen;
    Ecode_t versionFileStatus = nvm3_getObjectInfo(pFileSystemApplication,
                                                   ZAF_FILE_ID_APP_VERSION,
                                                   &objectType, &dataLen);

    if(ECODE_NVM3_OK != versionFileStatus) {
//      /**
//       * Reset the file system if ZAF_FILE_ID_APP_VERSION is missing since
//       * this indicates corrupt or missing file system.
//       */
      DeviceConfiguration_Reset();
    }
    else {
//      /**
//       * Add code for migration of file system to higher version here.
//       * Use info in g_aFileStatus[] to see what files are missing or
//       * have changed size.
//       */
    }
    ret = CONFIG_ERR;
  }

  exit: return ret;
}

/**
 * @brief Reset application FileSystem to default
 */
configErrCode_t
DeviceConfiguration_Reset(void)
{
  configErrCode_t ret;


  /**
   * Assert has been kept for debugging , can be removed
   * from production code.
   * This error can only be caused by some internal flash HW failure
   */
  ASSERT(0 != pFileSystemApplication);

  Ecode_t errCode = nvm3_eraseAll(pFileSystemApplication);
  ASSERT(ECODE_NVM3_OK == errCode);

  /** Initialize transport layer */
  //Transport_SetDefault();

  /**
   * Apparently there is no valid configuration in the NVM3 file system,
   * so load default values and save them
   */
  ret = DeviceConfiguration_Set_Default();

  return ret;
}

/**
 * @brief Get device configuration struct
 *
 * @param config - pointer to destination
 */
void
DeviceConfiguration_Get(DeviceConfiguration_t* config)
{
  memcpy(config, &DeviceConfiguration, ZAF_FILE_SIZE_DEVICE_CONFIG);
}

/**
 * @brief Set device configuration struct
 *
 * @param config - pointer to source
 */
void
DeviceConfiguration_Set(DeviceConfiguration_t* config)
{
  memcpy(&DeviceConfiguration, config, ZAF_FILE_SIZE_DEVICE_CONFIG);
}

/**
 * @brief Save configuration struct which was set
 *        by DeviceConfiguration_Set(DeviceConfiguration_t* )
 *
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Commit(void)
{
  Ecode_t errCode;

  errCode = nvm3_writeData(pFileSystemApplication,
                           FILE_ID_APPLICATIONDATA,
                           &DeviceConfiguration,
                           ZAF_FILE_SIZE_DEVICE_CONFIG);
  ASSERT(ECODE_NVM3_OK == errCode);

  if(errCode == ECODE_NVM3_OK)
    return CONFIG_OK;
  else
    return CONFIG_ERR;
}

/**
 * @brief Get current hardware configuration
 *
 * @return Hardware configuration
 */
HW_Config_t
DeviceConfiguration_Get_HW(void)
{
  static HW_Config_t HW_Config = HW_CONFIG_UNKNOWN;
  uint8_t boardAddress = 0;

  if(HW_Config == HW_CONFIG_UNKNOWN) {
    boardAddress = Board_AddressGet();
    switch(boardAddress) {
      case BOARD_ADDRESS_WATER_HEATER:
        HW_Config = HW_CONFIG_WATER_HEATER;
        break;
      case BOARD_ADDRESS_LIGHT:
        HW_Config = HW_CONFIG_LIGHT;
        break;
      case BOARD_ADDRESS_DIMMER:
        HW_Config = HW_CONFIG_DIMMER;
        break;
      default:
        HW_Config = HW_CONFIG_UNKNOWN;
        break;
    }
  }
  return HW_Config;
}

/**
 * @brief Print device configuration structure to log output
 */
void
DeviceConfiguration_SetUp()
{
  HW_Config_t HW_Config = DeviceConfiguration_Get_HW();
  SW_Config_t SW_Config = DeviceConfiguration_Get_SW();
  EndpointConfig_t EpConfig = { 0 };
  DimmerConfig_t dimmerConfig;
  BlindsConfig_t blindsConfig;
  BoilerConfig_t boilerConfig = DeviceConfiguration_Get_Boiler();
  BacklightConfig_t backlightConfig;
  CentralSceneToggleConfig_t centralSceneToggleConfig;
  CentralSceneMasterAllOnOffConfig_t centralSceneMasterAllOnOffConfig =
      DeviceConfiguration_Get_SentralSceneMasterAllOnOff();
  IndicationConfig_t indicationConfig = DeviceConfiguration_Get_Indication();
  RadioConfig_t radioConfig = DeviceConfiguration_Get_Radio();
  CCZWavePlusInfo.roleType = APP_ROLE_TYPE;
  CCZWavePlusInfo.nodeType = APP_NODE_TYPE;
  /*
  SEndpointIcon EP_Icons[NUMBER_OF_ENDPOINTS]={0};
  SEndpointIconList EP_Icons_List;
  EP_Icons_List.pEndpointInfo = &EP_Icons;
  switch (SW_Config)
  {
    case  SW_CONFIG_1L:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_SPECIFIC_ON_OFF_WALL_SWITCH_ONE_BUTTON;
      CCZWavePlusInfo.userIconType = ICON_TYPE_SPECIFIC_ON_OFF_WALL_SWITCH_ONE_BUTTON;
      for(uint8_t i=0;i<1;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=1;
   break;
   case  SW_CONFIG_B:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_SPECIFIC_ON_OFF_WALL_SWITCH_ONE_BUTTON;
      CCZWavePlusInfo.userIconType = ICON_TYPE_SPECIFIC_ON_OFF_WALL_SWITCH_ONE_BUTTON;
      for(uint8_t i=0;i<1;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=1;
    break;
    case  SW_CONFIG_2L:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_SPECIFIC_ON_OFF_WALL_SWITCH_TWO_BUTTONS;
      CCZWavePlusInfo.userIconType = ICON_TYPE_SPECIFIC_ON_OFF_WALL_SWITCH_TWO_BUTTONS;
      for(uint8_t i=0;i<2;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=2;
    break;
    case  SW_CONFIG_3L:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_SPECIFIC_ON_OFF_WALL_SWITCH_THREE_BUTTONS;
      CCZWavePlusInfo.userIconType = ICON_TYPE_SPECIFIC_ON_OFF_WALL_SWITCH_THREE_BUTTONS;
      for(uint8_t i=0;i<3;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=3;
    break;
    case  SW_CONFIG_4L:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_SPECIFIC_ON_OFF_WALL_SWITCH_FOUR_BUTTONS;
      CCZWavePlusInfo.userIconType = ICON_TYPE_SPECIFIC_ON_OFF_WALL_SWITCH_FOUR_BUTTONS;
      for(uint8_t i=0;i<4;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=4;
   break;
    case  SW_CONFIG_6L:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_SPECIFIC_ON_OFF_WALL_SWITCH_FOUR_BUTTONS+2;
      CCZWavePlusInfo.userIconType = ICON_TYPE_SPECIFIC_ON_OFF_WALL_SWITCH_FOUR_BUTTONS+2;
      for(uint8_t i=0;i<6;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=6;
    break;
    case  SW_CONFIG_1DT:
    case  SW_CONFIG_1D:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_SPECIFIC_DIMMER_WALL_SWITCH_ONE_BUTTON;
      CCZWavePlusInfo.userIconType = ICON_TYPE_SPECIFIC_DIMMER_WALL_SWITCH_ONE_BUTTON;
      for(uint8_t i=0;i<1;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_LIGHT_DIMMER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_LIGHT_DIMMER_SWITCH;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=1;
    break;
    case  SW_CONFIG_2DT:
    case  SW_CONFIG_2D:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_SPECIFIC_DIMMER_WALL_SWITCH_TWO_BUTTONS;
      CCZWavePlusInfo.userIconType = ICON_TYPE_SPECIFIC_DIMMER_WALL_SWITCH_TWO_BUTTONS;
      for(uint8_t i=0;i<2;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_LIGHT_DIMMER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_LIGHT_DIMMER_SWITCH;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=2;
    break;
    case  SW_CONFIG_3DT:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_SPECIFIC_DIMMER_WALL_SWITCH_THREE_BUTTONS;
      CCZWavePlusInfo.userIconType = ICON_TYPE_SPECIFIC_DIMMER_WALL_SWITCH_THREE_BUTTONS;
      for(uint8_t i=0;i<3;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_LIGHT_DIMMER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_LIGHT_DIMMER_SWITCH;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=3;
    break;
    case  SW_CONFIG_4DT:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_SPECIFIC_DIMMER_WALL_SWITCH_FOUR_BUTTONS;
      CCZWavePlusInfo.userIconType = ICON_TYPE_SPECIFIC_DIMMER_WALL_SWITCH_FOUR_BUTTONS;
      for(uint8_t i=0;i<4;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_LIGHT_DIMMER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_LIGHT_DIMMER_SWITCH;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=4;
    break;
    case  SW_CONFIG_1M:
    case  SW_CONFIG_1SH:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
      CCZWavePlusInfo.userIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
      for(uint8_t i=0;i<1;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=1;
    break;
    case  SW_CONFIG_2M:
    case  SW_CONFIG_2SH:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
      CCZWavePlusInfo.userIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
      for(uint8_t i=0;i<2;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=2;
    break;
    case  SW_CONFIG_3M:
    case  SW_CONFIG_3SH:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
      CCZWavePlusInfo.userIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
      for(uint8_t i=0;i<3;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=3;
    break;
    case  SW_CONFIG_F:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_GENERIC_FAN_SWITCH;
      CCZWavePlusInfo.userIconType = ICON_TYPE_GENERIC_FAN_SWITCH;
      for(uint8_t i=0;i<1;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_FAN_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_FAN_SWITCH;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=1;
    break;
    case  SW_CONFIG_4DT2D:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_SPECIFIC_DIMMER_WALL_SWITCH_FOUR_BUTTONS+2;
      CCZWavePlusInfo.userIconType = ICON_TYPE_SPECIFIC_DIMMER_WALL_SWITCH_FOUR_BUTTONS+2;
      for(uint8_t i=0;i<6;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_LIGHT_DIMMER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_LIGHT_DIMMER_SWITCH;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=6;
      break;
    case  SW_CONFIG_4L1SH:
    case  SW_CONFIG_4L1M:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_GENERIC_WALL_CONTROLLER;
      CCZWavePlusInfo.userIconType = ICON_TYPE_GENERIC_WALL_CONTROLLER;
      for(uint8_t i=0;i<4;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
        }
      EP_Icons[4].installerIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
      EP_Icons[4].userIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=5;
    break;
    case  SW_CONFIG_2L1SH:
    case  SW_CONFIG_2L1M:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_GENERIC_WALL_CONTROLLER;
      CCZWavePlusInfo.userIconType = ICON_TYPE_GENERIC_WALL_CONTROLLER;
      for(uint8_t i=0;i<2;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
        }
      EP_Icons[2].installerIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
      EP_Icons[2].userIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=3;
    break;
    case  SW_CONFIG_2L2SH:
    case  SW_CONFIG_2L2M:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_GENERIC_WALL_CONTROLLER;
      CCZWavePlusInfo.userIconType = ICON_TYPE_GENERIC_WALL_CONTROLLER;
      for(uint8_t i=0;i<2;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_ON_OFF_POWER_SWITCH;
        }
      for(uint8_t i=2;i<4;i++)
        {
          EP_Icons[i].installerIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
          EP_Icons[i].userIconType = ICON_TYPE_GENERIC_WINDOW_COVERING_NO_POSITION_ENDPOINT;
        }
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=4;
    break;
    case  SW_CONFIG_9R:
    case SW_CONFIG_MAX:
    case SW_CONFIG_CUSTOM:
      CCZWavePlusInfo.installerIconType = ICON_TYPE_GENERIC_WALL_CONTROLLER;
      CCZWavePlusInfo.userIconType = ICON_TYPE_GENERIC_WALL_CONTROLLER;
      EP_Icons[0].installerIconType = ICON_TYPE_SPECIFIC_ON_OFF_POWER_SWITCH_PLUGIN;
      EP_Icons[0].userIconType = ICON_TYPE_SPECIFIC_ON_OFF_POWER_SWITCH_PLUGIN;
      CCZWavePlusInfo.pEndpointIconList = &EP_Icons_List;
      CCZWavePlusInfo.pEndpointIconList->endpointInfoSize=1;
    break;
  }
  */
  for (int ep = ENDPOINT_1; ep <= EP_NUM; ep++) {
    DeviceConfiguration_Get_Ep(ep, &EpConfig);

    if (EpConfig.BtnMode == BTN_MODE_CENTRAL_SCENE_PUSH) {
    } else {
    }

    //! Dimmer EP configurations
    if (EpConfig.BtnMode == BTN_MODE_DIMMER_1B ||
        EpConfig.BtnMode == BTN_MODE_DIMMER_2B) {
      dimmerConfig = DeviceConfiguration_Get_Dimmer(ep);
    }

    //! Blinds EP configurations
    if (EpConfig.BtnMode == BTN_MODE_BLINDS_1B ||
        EpConfig.BtnMode == BTN_MODE_BLINDS_2B ||
        EpConfig.BtnMode == BTN_MODE_BLINDS_SHADE) {
      blindsConfig = DeviceConfiguration_Get_Blinds(ep);
    }

    if (EpConfig.BtnMode == BTN_MODE_CENTRAL_SCENE_TOGGLE) {
      centralSceneToggleConfig = DeviceConfiguration_Get_SentralSceneToggle(ep);
    }
  }


  //! Button backlight configurations
  for (int butt = BTN_NUMBER_1; butt < BTN_NUMBER_MAX; butt++) {
     if (DeviceConfiguration_Get_Backlight(butt, &backlightConfig) == CONFIG_OK) {
     } else {
     }
  }

  //! Hack to silence warning of unused variable
  UNUSED(HW_Config);
  UNUSED(SW_Config);
  UNUSED(EpConfig);
  UNUSED(dimmerConfig);
  UNUSED(blindsConfig);
  UNUSED(boilerConfig);
  UNUSED(backlightConfig);
  UNUSED(indicationConfig);
  UNUSED(radioConfig);
  UNUSED(centralSceneMasterAllOnOffConfig);
  UNUSED(centralSceneToggleConfig);
}

/**
 * @brief Get device software configuration
 * @return Device software configuration
 */
SW_Config_t
DeviceConfiguration_Get_SW(void)
{
  DeviceConfiguration_t DeviceConfig = { 0 };
  DeviceConfiguration_Get(&DeviceConfig);

  return DeviceConfig.SW_Config;
}

/**
 * @brief Set device software configuration
 * @details Software configuration allows to set the parameters EP #2-10
 *          as preset, which speeds up the process of setting up the device
 * @note If isDefault - true, SW_Config will be ignored
 * @note If SW_Config preset applied, also applied corresponding button colors
 *
 * @param SW_Config - software configuration
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_SW(SW_Config_t SW_Config, bool isDefault)
{
  bool ret = CONFIG_OK;
  HW_Config_t HW_Config = HW_CONFIG_UNKNOWN;
  HW_Config_t MyHW_Config;
  DeviceConfiguration_t DeviceConfig = { 0 };

  DeviceConfiguration_Get(&DeviceConfig);

  //! Get current HW configuration, saved configuration maybe not actual
  HW_Config = DeviceConfiguration_Get_HW();
  MyHW_Config=HW_Config;

  if(isDefault) {
    switch(HW_Config) {
      case HW_CONFIG_WATER_HEATER:
        SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_3L);  //R3 L1,L2,L3,B
        break;
      case HW_CONFIG_LIGHT:
        SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_3SH);  //R6
        break;
      case HW_CONFIG_DIMMER:
        SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_4DT2D);
        break;
      default:
        ret = CONFIG_HW_UNKNOWN;
        break;
    }
  }
  else if(SW_Config >= SW_CONFIG_MAX) {
    ret = CONFIG_SW_INVALID;
  }
  else if(SW_Config == SW_CONFIG_CUSTOM) {
    DeviceConfig.SW_Config = SW_CONFIG_CUSTOM;
  }
  else {
    switch(HW_Config) {
      case HW_CONFIG_WATER_HEATER:

        switch(SW_Config) {
          case SW_CONFIG_1L:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_1L);
            break;
          case SW_CONFIG_2L:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_2L);
            break;
          case SW_CONFIG_3L:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_3L);
            break;
          case SW_CONFIG_1M:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_1M);
            break;
          case SW_CONFIG_B:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_B);
            break;
          case SW_CONFIG_F:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_F);
            break;
          case SW_CONFIG_9R:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_9R);
            break;
          default:
            ret = CONFIG_SW_INTENDED_FOR_HW;
            break;
        }
        break;

      case HW_CONFIG_LIGHT:

        switch(SW_Config) {
          case SW_CONFIG_1L:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_1L);
            break;
          case SW_CONFIG_2L:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_2L);
            break;
          case SW_CONFIG_3L:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_3L);
            break;
          case SW_CONFIG_4L:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_4L);
            break;
          case SW_CONFIG_6L:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_6L);
            break;
          case SW_CONFIG_1M:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_1M);
            break;
          case SW_CONFIG_2M:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_2M);
            break;
          case SW_CONFIG_3M:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_3M);
            break;
          case SW_CONFIG_4L1M:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_4L1M);
            break;
          case SW_CONFIG_2L1M:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_2L1M);
            break;
          case SW_CONFIG_2L2M:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_2L2M);
            break;
          case SW_CONFIG_1SH:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_1SH);
            break;
          case SW_CONFIG_2SH:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_2SH);
            break;
          case SW_CONFIG_3SH:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_3SH);
            break;
          case SW_CONFIG_4L1SH:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_4L1SH);
            break;
          case SW_CONFIG_2L1SH:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_2L1SH);
            break;
          case SW_CONFIG_2L2SH:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_2L2SH);
            break;
         case SW_CONFIG_9R:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_9R);
            break;
          default:
            ret = CONFIG_SW_INTENDED_FOR_HW;
            break;
        }
        break;

      case HW_CONFIG_DIMMER:
        switch(SW_Config) {
          case SW_CONFIG_1DT:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_1DT);
            break;
          case SW_CONFIG_2DT:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_2DT);
            break;
          case SW_CONFIG_3DT:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_3DT);
            break;
          case SW_CONFIG_4DT:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_4DT);
            break;
          case SW_CONFIG_1D:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_1D);
            break;
          case SW_CONFIG_2D:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_2D);
            break;
          case SW_CONFIG_4DT2D:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_4DT2D);
            break;
          case SW_CONFIG_9R:
            SW_CONFIG_PRESET(DeviceConfig, SW_CONFIG_9R);
            break;
          default:
            ret = CONFIG_SW_INTENDED_FOR_HW;
            break;
        }
        break;

      default:
        ret = CONFIG_HW_UNKNOWN;
        break;
    }
    DeviceConfig.SW_Config = SW_Config;
  }

  if(ret == CONFIG_OK) {
    DeviceConfig.HW_Config = MyHW_Config;
    DeviceConfiguration_Set(&DeviceConfig);
  }

  return ret;
}

/**
 * @brief Get Endpoint configuration by it's number
 *
 * @param epNumber - number of endpoint
 * @param pEpConfig - pointer to returned configuration
 * @return Configuration error code
 */
configErrCode_t
DeviceConfiguration_Get_Ep(uint8_t epNumber, EndpointConfig_t *pEpConfig)
{
  DeviceConfiguration_t DeviceConfig = { 0 };

  if (epNumber == ENDPOINT_ROOT ||
      epNumber > EP_NUM) {
    return CONFIG_EP_INVALID;
  }

  DeviceConfiguration_Get(&DeviceConfig);

  memcpy(pEpConfig, &DeviceConfig.EndpointConfig_list[epNumber],
         sizeof(EndpointConfig_t));
  return CONFIG_OK;
}

/**
 * @brief Set endpoint configuration
 * @note if isDefault - true, config will be ignored
 *
 * @param epNumber - number of endpoint
 * @param config - configuration structure
 * @param isDefault - used to specify if the default value is to be restored
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Ep(uint8_t epNumber, EndpointConfig_t config,
                           bool isDefault)
{
  configErrCode_t ret = CONFIG_OK;
  HW_Config_t HW_Config = HW_CONFIG_UNKNOWN;
  DeviceConfiguration_t DeviceConfig = { 0 };

  DeviceConfiguration_Get(&DeviceConfig);
  HW_Config = DeviceConfiguration_Get_HW();

  if (epNumber == ENDPOINT_ROOT ||
      epNumber > EP_NUM) {
    ret = CONFIG_EP_INVALID;
    goto exit;
  }

  if (isDefault) {
    switch (HW_Config) {
      case HW_CONFIG_WATER_HEATER:
        memcpy(&config, &SW_CONFIG_3L_PRESET[epNumber],
               sizeof(EndpointConfig_t));
        break;
      case HW_CONFIG_LIGHT:
        memcpy(&config, &SW_CONFIG_6L_PRESET[epNumber],
               sizeof(EndpointConfig_t));
        break;
      case HW_CONFIG_DIMMER:
        memcpy(&config, &SW_CONFIG_4DT2D_PRESET[epNumber],
               sizeof(EndpointConfig_t));
        break;
      default:
        ret = CONFIG_HW_UNKNOWN;
        goto exit;
        break;
    }
  }

  if (config.EpMode >= EP_MODE_MAX && config.EpMode == EP_MODE_BOILER_SWITCH) {
    ret = CONFIG_EP_INVALID;
    goto exit;
  }

  ret = isBtnModeValid(config);
  if (ret != CONFIG_OK) {
    goto exit;
  }

  ret = isBtnNumValid(&DeviceConfig, config);
  if(ret != CONFIG_OK) {
    goto exit;
  }

/*
  if (config.BtnMode == BTN_MODE_CENTRAL_SCENE_PUSH ||
      config.BtnMode == BTN_MODE_CENTRAL_SCENE_TOGGLE ||
      config.BtnMode == BTN_MODE_CENTRAL_SCENE_MASTER_ALL_ONOFF) {
    //! There is no need to check scene number
  } else {
    ret = isOutputChValid(&DeviceConfig, config);
    if(ret != CONFIG_OK) {
      goto exit;
    }
  }
*/
//  DeviceConfig.SW_Config = SW_CONFIG_CUSTOM;
  memcpy(&DeviceConfig.EndpointConfig_list[epNumber], &config,
         sizeof(EndpointConfig_t));
  DeviceConfiguration_Set(&DeviceConfig);

  exit: return ret;
}

/**
 * @brief Validation of button mode
 * @param config - endpoint configuration structure
 * @return configErrCode_t - error code
 */
static configErrCode_t
isBtnModeValid(EndpointConfig_t config)
{
  configErrCode_t ret = CONFIG_OK;
  EpMode_t EpMode = config.EpMode;
  BtnMode_t BtnMode = config.BtnMode;

  if(BtnMode >= BTN_MODE_MAX) {
    ret = CONFIG_BTN_MODE_INVALID;
    goto exit;
  }

  switch(EpMode) {
    case EP_MODE_REMOTE_SWITCH:
      switch(BtnMode) {
        case BTN_MODE_DISABLE:
        case BTN_MODE_SWITCH_ON_OFF:
        case BTN_MODE_PUSH:
        case BTN_MODE_DIMMER_1B:
        case BTN_MODE_DIMMER_2B:
        case BTN_MODE_BLINDS_1B:
        case BTN_MODE_BLINDS_2B:
        case BTN_MODE_BLINDS_SHADE:
        case BTN_MODE_CENTRAL_SCENE_PUSH:
        case BTN_MODE_CENTRAL_SCENE_TOGGLE:
        case BTN_MODE_CENTRAL_SCENE_MASTER_ALL_ONOFF:
          break;
        default:
          ret = CONFIG_BTN_MODE_NOT_INTENDED_FOR_EP_MODE;
          goto exit;
          break;
      }
      break;
    case EP_MODE_RELAY_SWITCH:
      switch(BtnMode) {
        case BTN_MODE_DISABLE:
        case BTN_MODE_SWITCH_ON_OFF:
        case BTN_MODE_PUSH:
          break;
        default:
          ret = CONFIG_BTN_MODE_NOT_INTENDED_FOR_EP_MODE;
          goto exit;
          break;
      }
      break;
    case EP_MODE_DIMMER_SWITCH:
      if(BtnMode != BTN_MODE_DIMMER_1B && BtnMode != BTN_MODE_DIMMER_2B) {
        ret = CONFIG_BTN_MODE_NOT_INTENDED_FOR_EP_MODE;
        goto exit;
      }
      break;
    case EP_MODE_BLINDS_SWITCH:
      if(BtnMode != BTN_MODE_BLINDS_1B && BtnMode != BTN_MODE_BLINDS_2B && BtnMode != BTN_MODE_BLINDS_SHADE) {
        ret = CONFIG_BTN_MODE_NOT_INTENDED_FOR_EP_MODE;
        goto exit;
      }
      break;
    case EP_MODE_DISABLE:
      if(BtnMode != BTN_MODE_DISABLE) {
        ret = CONFIG_BTN_MODE_NOT_INTENDED_FOR_EP_MODE;
        goto exit;
      }
      break;
    case EP_MODE_FAN_SWITCH:
      if(BtnMode != BTN_MODE_SWITCH_ON_OFF) {
        ret = CONFIG_BTN_MODE_NOT_INTENDED_FOR_EP_MODE;
        goto exit;
      }
      break;
    default:
      ret = CONFIG_EP_INVALID;
      goto exit;
      break;
  }

  exit: return ret;
}

/**
 * @brief Validation of button number
 * @details Use the button that already used with another EP, will force
 *          automatically disconnected previous EP's button
 *
 * @param DeviceConfig - device configuration structure
 * @param config - endpoint configuration structure
 * @return configErrCode_t - error code
 */
static configErrCode_t
isBtnNumValid(DeviceConfiguration_t *DeviceConfig, EndpointConfig_t config)
{
  configErrCode_t ret = CONFIG_OK;
  EndpointConfig_t *EpConfig = NULL;

  if (config.BtnMode == BTN_MODE_DISABLE) {
    //! No need to check the number if the button is disabled
    goto exit;
  }

  if (config.BtnMode == BTN_MODE_DIMMER_2B ||
      config.BtnMode == BTN_MODE_BLINDS_2B ||
      config.BtnMode == BTN_MODE_BLINDS_SHADE) {
    //! Two buttons mode

    //! Check if the button number does it exist
    if(config.BtnNumber.Btn1 >= BTN_NUMBER_MAX
        || config.BtnNumber.Btn2 >= BTN_NUMBER_MAX) {
      ret = CONFIG_BTN_NUM_INVALID;
      goto exit;
    }

    //! Check if the button number has not been set
    if(config.BtnNumber.Btn1 == BTN_NUMBER_NONE
        || config.BtnNumber.Btn2 == BTN_NUMBER_NONE) {
      ret = CONFIG_BTN_NUM_EMPTY;
      goto exit;
    }

    //! Check if the button numbers is not equal
    if(config.BtnNumber.Btn1 == config.BtnNumber.Btn2) {
      ret = CONFIG_BTN_NUM_EQUAL;
      goto exit;
    }

  }
  else {
    //! Check if the button number does it exist
    if(config.BtnNumber.Btn >= BTN_NUMBER_MAX) {
      ret = CONFIG_BTN_NUM_INVALID;
      goto exit;
    }

    //! Check if the button number has not been set
    if(config.BtnNumber.Btn == BTN_NUMBER_NONE) {
      ret = CONFIG_BTN_NUM_EMPTY;
      goto exit;
    }
  }

  /**
   * Use the button that already used with another EP, will force automatically
   * disconnected previous EP's button
   */
  for(int ep = 0; ep < EP_NUM; ep++) {
    EpConfig = &DeviceConfig->EndpointConfig_list[ep];

    if(EpConfig->BtnNumber.Btn1 == config.BtnNumber.Btn1)
      EpConfig->BtnNumber.Btn1 = BTN_NUMBER_NONE;
    if(EpConfig->BtnNumber.Btn1 == config.BtnNumber.Btn2)
      EpConfig->BtnNumber.Btn1 = BTN_NUMBER_NONE;

    if(EpConfig->BtnNumber.Btn2 == config.BtnNumber.Btn1)
      EpConfig->BtnNumber.Btn2 = BTN_NUMBER_NONE;
    if(EpConfig->BtnNumber.Btn2 == config.BtnNumber.Btn2)
      EpConfig->BtnNumber.Btn2 = BTN_NUMBER_NONE;
  }

  exit: return ret;
}

/**
 * @brief Validation of output channel
 * @details Use the output that already used with another EP, will force
 *          automatically disconnected previous EP's output
 * @param DeviceConfig - device configuration structure
 * @param config - endpoint configuration structure
 * @return configErrCode_t - error code
 */
static configErrCode_t
isOutputChValid(DeviceConfiguration_t *DeviceConfig, EndpointConfig_t config)
{
  configErrCode_t ret = CONFIG_OK;
  EndpointConfig_t *EpConfig = NULL;

  //! Output channel should be none if ep mode is remote or disabled
  if(config.EpMode == EP_MODE_DISABLE || config.EpMode == EP_MODE_REMOTE_SWITCH) {
    if(config.Output != OUTPUT_CH_NONE) {
      ret = CONFIG_OUTPUT_CH_INVALID;
      goto exit;
    }
    else {
      ret = CONFIG_OK;
      goto exit;
    }
  }

  if(config.BtnMode == BTN_MODE_BLINDS_1B ||
     config.BtnMode == BTN_MODE_BLINDS_2B ||
     config.BtnMode == BTN_MODE_BLINDS_SHADE) {
    //! Two outputs mode

    //! Check if the output number does it exist
    if(DeviceConfig->HW_Config == HW_CONFIG_WATER_HEATER) {
      if(config.Output1 > OUTPUT_CH_3
          || config.Output2 > OUTPUT_CH_3) {
        ret = CONFIG_OUTPUT_CH_INVALID;
        goto exit;
      }
    }
    else {
      if(config.Output1 >= OUTPUT_CH_MAX
          || config.Output2 >= OUTPUT_CH_MAX) {
        ret = CONFIG_OUTPUT_CH_INVALID;
        goto exit;
      }
    }

    //! Check if the output channel has not been set
    if(config.Output1 == OUTPUT_CH_NONE
        || config.Output2 == OUTPUT_CH_NONE) {
      if(config.EpMode != EP_MODE_REMOTE_SWITCH) {
        ret = CONFIG_OUTPUT_CH_EMPTY;
        goto exit;
      }
    }

    //! Check if the output channels is not equal
    if(config.Output1 == config.Output2) {
      ret = CONFIG_OUTPUT_CH_EQUAL;
      goto exit;
    }

  }
  else {
    //! One output mode

    //! Check if the button number does it exist
    if(DeviceConfig->HW_Config == HW_CONFIG_WATER_HEATER) {
      if(config.Output > OUTPUT_CH_3) {
        ret = CONFIG_OUTPUT_CH_INVALID;
        goto exit;
      }
    }
    else {
      if(config.Output >= OUTPUT_CH_MAX) {
        ret = CONFIG_OUTPUT_CH_INVALID;
        goto exit;
      }
    }

    //! Check if the button number has not been set
    if(config.Output == OUTPUT_CH_NONE) {
      if(config.EpMode != EP_MODE_REMOTE_SWITCH) {
        ret = CONFIG_OUTPUT_CH_EMPTY;
        goto exit;
      }
    }
  }

  /**
   * Use the output that already used with another EP, will force automatically
   * disconnected previous EP's output
   */
  for(int ep = 0; ep < EP_NUM; ep++) {
    EpConfig = &DeviceConfig->EndpointConfig_list[ep];

    if(EpConfig->Output1 == config.Output1) {
      if(EpConfig->Output1 != OUTPUT_CH_NONE) {
        EpConfig->Output1 = OUTPUT_CH_NONE;
        EpConfig->EpMode = EP_MODE_REMOTE_SWITCH;
      }
    }
    else if(EpConfig->Output1 == config.Output2) {
      if(EpConfig->Output1 != OUTPUT_CH_NONE) {
        EpConfig->Output1 = OUTPUT_CH_NONE;
        EpConfig->EpMode = EP_MODE_REMOTE_SWITCH;
      }
    }
    else if(EpConfig->Output2 == config.Output1) {
      if(EpConfig->Output2 != OUTPUT_CH_NONE) {
        EpConfig->Output2 = OUTPUT_CH_NONE;
        EpConfig->EpMode = EP_MODE_REMOTE_SWITCH;
      }
    }
    else if(EpConfig->Output2 == config.Output2) {
      if(EpConfig->Output2 != OUTPUT_CH_NONE) {
        EpConfig->Output2 = OUTPUT_CH_NONE;
        EpConfig->EpMode = EP_MODE_REMOTE_SWITCH;
      }
    }
  }

  exit: return ret;
}

DimmerConfig_t DeviceConfiguration_Get_Dimmer(uint8_t epNumber)
{
  DeviceConfiguration_t DeviceConfig;
  DeviceConfiguration_Get(&DeviceConfig);

  if (epNumber >= EP_NUM) return DeviceConfig.DimmerConfig[EP_NUM];

  return DeviceConfig.DimmerConfig[epNumber];
}

/**
 * @brief Set dimmer configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Dimmer(DimmerConfig_t *pConfig, uint8_t epNumber)
{
  configErrCode_t ret;
  bool isConfigValid = true;
  DeviceConfiguration_t DeviceConfig = { 0 };
  DeviceConfiguration_Get(&DeviceConfig);

  if (pConfig->minDimmingValue >= DEFAULT_MAX_DIMMING_VALUE) isConfigValid = false;

  if (pConfig->maxDimmingValue <= DEFAULT_MIN_DIMMING_VALUE ||
      pConfig->maxDimmingValue > DEFAULT_MAX_DIMMING_VALUE) {
    isConfigValid = false;
  }

  if (pConfig->dimmigTimeFadeUp > DEFAULT_DIMMING_TIME_FADE_MAX ||
      pConfig->dimmigTimeFadeDown > DEFAULT_DIMMING_TIME_FADE_MAX)
    isConfigValid = false;

  if (epNumber > EP_NUM) isConfigValid = false;

  if (isConfigValid) {
    memcpy(&DeviceConfig.DimmerConfig[epNumber], pConfig, sizeof(DimmerConfig_t));
    DeviceConfiguration_Set(&DeviceConfig);
    ret = CONFIG_OK;
  } else {
    ret = CONFIG_ERR;
  }

  return ret;
}

/**
 * @brief Get blinds configuration structure
 *
 * @return blinds configuration
 */
BlindsConfig_t
DeviceConfiguration_Get_Blinds(uint8_t epNumber)
{
  DeviceConfiguration_t DeviceConfig = { 0 };
  DeviceConfiguration_Get(&DeviceConfig);

  if (epNumber >= EP_NUM) return DeviceConfig.BlindsConfig[EP_NUM];

  return DeviceConfig.BlindsConfig[epNumber];
}

/**
 * @brief Set blinds configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Blinds(BlindsConfig_t *pConfig, uint8_t epNumber)
{
  configErrCode_t ret;
  bool isConfigValid = true;
  DeviceConfiguration_t DeviceConfig = { 0 };
  DeviceConfiguration_Get(&DeviceConfig);

  if (pConfig->durationUpSec < DEFAULT_BLINDS_DURATION_MIN ||
      pConfig->durationDownSec < DEFAULT_BLINDS_DURATION_MIN)
    isConfigValid = false;

  if (pConfig->DurationShort < DEFAULT_BLINDS_DURATION_MIN ||
      pConfig->DurationVeryShort < DEFAULT_BLINDS_DURATION_MIN)
    isConfigValid = false;

  if (pConfig->DurationShort > DEFAULT_SHADE_DURATION_MAX ||
      pConfig->DurationVeryShort > DEFAULT_SHADE_DURATION_MAX)
    isConfigValid = false;

  if (epNumber >= EP_NUM)
    isConfigValid = false;

  if(isConfigValid) {
    memcpy(&DeviceConfig.BlindsConfig[epNumber], pConfig, sizeof(BlindsConfig_t));
    DeviceConfiguration_Set(&DeviceConfig);
    ret = CONFIG_OK;
  } else {
    ret = CONFIG_ERR;
  }

  return ret;
}

/**
 * @brief Get Central Scene Toggle configuration structure
 *
 * @return central scene toggle configuration
 */
CentralSceneToggleConfig_t
DeviceConfiguration_Get_SentralSceneToggle(uint8_t epNumber)
{
  DeviceConfiguration_t DeviceConfig;
  DeviceConfiguration_Get(&DeviceConfig);

  if (epNumber >= EP_NUM)
    return DeviceConfig.CentralSceneToggleConfig[EP_NUM];

  return DeviceConfig.CentralSceneToggleConfig[epNumber];
}

/**
 * @brief Set Central Scene Toggle configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_SentralSceneToggle(CentralSceneToggleConfig_t *pConfig, uint8_t epNumber)
{
  DeviceConfiguration_t DeviceConfig;
  DeviceConfiguration_Get(&DeviceConfig);

  if (epNumber >= EP_NUM)
    return CONFIG_ERR;

  memcpy(&DeviceConfig.CentralSceneToggleConfig[epNumber], pConfig, sizeof(CentralSceneToggleConfig_t));
  DeviceConfiguration_Set(&DeviceConfig);

  return CONFIG_OK;
}

/**
 * @brief Get Central Scene Master All On/Off configuration structure
 *
 * @return central scene Master All On/Off configuration
 */
CentralSceneMasterAllOnOffConfig_t
DeviceConfiguration_Get_SentralSceneMasterAllOnOff(void)
{
  DeviceConfiguration_t DeviceConfig;
  DeviceConfiguration_Get(&DeviceConfig);

  return DeviceConfig.MasterAllOnOff;
}

/**
 * @brief Set Central Scene Master All On/Off configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_SentralSceneMasterAllOnOff(CentralSceneMasterAllOnOffConfig_t *pConfig)
{
  DeviceConfiguration_t DeviceConfig;
  DeviceConfiguration_Get(&DeviceConfig);

  memcpy(&DeviceConfig.MasterAllOnOff, pConfig, sizeof(CentralSceneMasterAllOnOffConfig_t));
  DeviceConfiguration_Set(&DeviceConfig);

  return CONFIG_OK;
}

/**
 * @brief Get boiler configuration structure
 *
 * @return boiler configuration
 */
BoilerConfig_t
DeviceConfiguration_Get_Boiler(void)
{
  DeviceConfiguration_t DeviceConfig = { 0 };
  DeviceConfiguration_Get(&DeviceConfig);

  return DeviceConfig.BoilerConfig;
}

/**
 * @brief Set boiler configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Boiler(BoilerConfig_t *pConfig)
{
  configErrCode_t ret;
  bool isConfigValid = true;
  DeviceConfiguration_t DeviceConfig = { 0 };
  DeviceConfiguration_Get(&DeviceConfig);

  if(pConfig->maxBoilingTime < 1)
    isConfigValid = false;

  if(isConfigValid) {
    memcpy(&DeviceConfig.BoilerConfig, pConfig, sizeof(BoilerConfig_t));
    DeviceConfiguration_Set(&DeviceConfig);
    ret = CONFIG_OK;
  }
  else {
    ret = CONFIG_ERR;
  }

  return ret;
}

/**
 * @brief Get button backlight configuration structure
 *
 * @param[IN] buttNumber - Button number for getting backlight configuration,
 *  @note Possible values in range of BTN_NUMBER_NONE and BTN_NUMBER_MAX
 * @param[IN/OUT] pConfig - structure pointer for backlight configuration
 *
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Get_Backlight(uint8_t buttNumber, BacklightConfig_t *pConfig)
{
  configErrCode_t ret;
  DeviceConfiguration_t DeviceConfig = { 0 };
  DeviceConfiguration_Get(&DeviceConfig);

  //! Verify by valid pointer
  if (!pConfig) {
    ret = CONFIG_NULL_PTR;
    goto __ret;
  }

  //! Verify button number
  if (buttNumber == BTN_NUMBER_NONE ||
      buttNumber >= BTN_NUMBER_MAX) {
    ret = CONFIG_BTN_NUM_INVALID;
    goto __ret;
  }

  //! Copy requested button backlight configuration
  memcpy(pConfig, &DeviceConfiguration.BacklightConfig[buttNumber], sizeof(BacklightConfig_t));
  ret = CONFIG_OK;

__ret:
  return ret;
}

/**
 * @brief Set button backlight configuration structure
 *
 * @param[IN] buttNumber - Button number for getting backlight configuration,
 *  @note Possible values in range of BTN_NUMBER_NONE and BTN_NUMBER_MAX
 * @param[IN] pConfig - structure pointer for backlight configuration
 *
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Backlight(uint8_t buttNumber, BacklightConfig_t *pConfig)
{
  configErrCode_t ret;

  //! Verify by valid pointer
  if (!pConfig) {
    ret = CONFIG_NULL_PTR;
    goto __ret;
  }

  //! Verify button number
  if (buttNumber == BTN_NUMBER_NONE ||
      buttNumber >= BTN_NUMBER_MAX) {
    ret = CONFIG_BTN_NUM_INVALID;
    goto __ret;
  }

  //! Verify colors
  if (pConfig->offStatusColor >= BACKLIGHT_COLOR_MAX ||
      pConfig->onStatusColor >= BACKLIGHT_COLOR_MAX) {
    ret = CONFIG_BTN_HIGHLIGHT_INVALID;
    goto __ret;
  }

  memcpy(&DeviceConfiguration.BacklightConfig[buttNumber], pConfig, sizeof(BacklightConfig_t));
  ret = CONFIG_OK;

__ret:
  return ret;
}

/**
 * @brief Get indication configuration structure
 *
 * @return indication configuration
 */
IndicationConfig_t
DeviceConfiguration_Get_Indication(void)
{
  DeviceConfiguration_t DeviceConfig = { 0 };
  DeviceConfiguration_Get(&DeviceConfig);

  return DeviceConfig.IndicationConfig;
}

/**
 * @brief Set indication configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Indication(IndicationConfig_t *pConfig)
{
  DeviceConfiguration_t DeviceConfig = { 0 };
  DeviceConfiguration_Get(&DeviceConfig);

  memcpy(&DeviceConfig.IndicationConfig, pConfig, sizeof(IndicationConfig_t));
  DeviceConfiguration_Set(&DeviceConfig);

  return CONFIG_OK;
}

/**
 * @brief Get radio configuration structure
 *
 * @return indication configuration
 */
RadioConfig_t
DeviceConfiguration_Get_Radio(void)
{
  DeviceConfiguration_t DeviceConfig = { 0 };
  DeviceConfiguration_Get(&DeviceConfig);

  return DeviceConfig.RadioConfig;
}

/**
 * @brief Set radio configuration structure
 *
 * @param pConfig - pointer to configuration structure
 * @return configErrCode_t - error code
 */
configErrCode_t
DeviceConfiguration_Set_Radio(RadioConfig_t *pConfig)
{
  configErrCode_t ret;
  bool isConfigValid;
  DeviceConfiguration_t DeviceConfig = { 0 };
  DeviceConfiguration_Get(&DeviceConfig);

  switch (pConfig->radioChannel) {
    case REGION_EU:
    case REGION_US:
    case REGION_IL:
      isConfigValid = true;
      break;

    default:
      isConfigValid = false;
      break;
  }

  if(isConfigValid) {
    memcpy(&DeviceConfig.RadioConfig, pConfig, sizeof(RadioConfig_t));
    DeviceConfiguration_Set(&DeviceConfig);
    ret = CONFIG_OK;
  }
  else {
    ret = CONFIG_ERR;
  }

  return ret;
}

Switch_All_State_t
DeviceConfiguration_Get_Switch_All(void)
{
  DeviceConfiguration_t DeviceConfig = { 0 };
  DeviceConfiguration_Get(&DeviceConfig);

  return DeviceConfig.SwitchAllOnOffState;
}

configErrCode_t
DeviceConfiguration_Set_Switch_All(Switch_All_State_t pConfig)
{
  configErrCode_t ret;
  bool isConfigValid;
  DeviceConfiguration_t DeviceConfig = { 0 };
  DeviceConfiguration_Get(&DeviceConfig);

  switch (pConfig) {
    case ON_EXCLUDE_OFF_EXCLUDE:
    case ON_EXCLUDE_OFF_INCLUDE:
    case ON_INCLUDE_OFF_EXCLUDE:
    case ON_INCLUDE_OFF_INCLUDE:
      isConfigValid = true;
      break;

    default:
      isConfigValid = false;
      break;
  }

  if(isConfigValid) {
    memcpy(&DeviceConfig.SwitchAllOnOffState, &pConfig, sizeof(Switch_All_State_t));
    DeviceConfiguration_Set(&DeviceConfig);
    ret = CONFIG_OK;
  }
  else {
    ret = CONFIG_ERR;
  }

  return ret;
}
