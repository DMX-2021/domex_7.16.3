#include "ConfigurationPresets.h"
#define EP_Disable      EP_MODE_DISABLE,\
                        BTN_MODE_DISABLE,\
                        {{BTN_NUMBER_NONE,BTN_NUMBER_NONE}},\
                        {{OUTPUT_CH_NONE,OUTPUT_CH_NONE}},\
                        ON_EXCLUDE_OFF_INCLUDE,\
                        16,\
                        OffState,\
                        SCENE_MASTER_ALL_OFF\

const EndpointConfig_t SW_CONFIG_1L_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_5,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
      EP_Disable
  },
  { /* EP3 */
      EP_Disable
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
 },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_2L_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_4,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_6,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
      EP_Disable
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_3L_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_4,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_5,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_6,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_3,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_4L_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_1,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_3,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_7,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_3,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_9,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_4,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_6L_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_1,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_2,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_3,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_3,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_7,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_4,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP5 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_8,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_5,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP6 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_9,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_6,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_1DT_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_5,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
      EP_Disable
  },
  { /* EP3 */
      EP_Disable
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_2DT_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_4,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_6,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
      EP_Disable
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_3DT_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_4,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_5,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_6,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_3,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_4DT_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_1,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_3,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_7,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_3,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_9,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_4,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_1D_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_5,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_5,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
      EP_Disable
  },
  { /* EP3 */
      EP_Disable
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_2D_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_4,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_5,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_6,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_6,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
      EP_Disable
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_4DT2D_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_1,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_2,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_3,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_5,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_7,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_3,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP5 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_8,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_4,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP6 */
    EP_MODE_DIMMER_SWITCH,
    BTN_MODE_DIMMER_1B,
    {{BTN_NUMBER_9,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_6,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_1M_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_2B,
    {{BTN_NUMBER_4, BTN_NUMBER_6}},
    {{OUTPUT_CH_6, OUTPUT_CH_5}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
      EP_Disable
  },
  { /* EP3 */
      EP_Disable
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_2M_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_2B,
    {{BTN_NUMBER_1, BTN_NUMBER_7}},
    {{OUTPUT_CH_4, OUTPUT_CH_3}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_2B,
    {{BTN_NUMBER_3, BTN_NUMBER_9}},
    {{OUTPUT_CH_6, OUTPUT_CH_5}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
      EP_Disable
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_3M_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_2B,
    {{BTN_NUMBER_1, BTN_NUMBER_7}},
    {{OUTPUT_CH_2, OUTPUT_CH_1}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_2B,
    {{BTN_NUMBER_2, BTN_NUMBER_8}},
    {{OUTPUT_CH_4, OUTPUT_CH_3}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_2B,
    {{BTN_NUMBER_3, BTN_NUMBER_9}},
    {{OUTPUT_CH_6, OUTPUT_CH_5}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_4L1M_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_1,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_2,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_7,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_3,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_8,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_4,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP5 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_2B,
    {{BTN_NUMBER_3, BTN_NUMBER_9}},
    {{OUTPUT_CH_6, OUTPUT_CH_5}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_2L1M_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_1,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_7,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_2B,
    {{BTN_NUMBER_3, BTN_NUMBER_9}},
    {{OUTPUT_CH_6, OUTPUT_CH_5}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_2L2M_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_1,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_7,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_2B,
    {{BTN_NUMBER_2, BTN_NUMBER_8}},
    {{OUTPUT_CH_4, OUTPUT_CH_3}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_2B,
    {{BTN_NUMBER_3, BTN_NUMBER_9}},
    {{OUTPUT_CH_6, OUTPUT_CH_5}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_1SH_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_SHADE,
    {{BTN_NUMBER_4, BTN_NUMBER_6}},
    {{OUTPUT_CH_6, OUTPUT_CH_5}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
      EP_Disable
  },
  { /* EP3 */
      EP_Disable
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_2SH_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_SHADE,
    {{BTN_NUMBER_1, BTN_NUMBER_7}},
    {{OUTPUT_CH_4, OUTPUT_CH_3}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_SHADE,
    {{BTN_NUMBER_3, BTN_NUMBER_9}},
    {{OUTPUT_CH_6, OUTPUT_CH_5}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
      EP_Disable
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_3SH_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_SHADE,
    {{BTN_NUMBER_1, BTN_NUMBER_7}},
    {{OUTPUT_CH_2, OUTPUT_CH_1}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_SHADE,
    {{BTN_NUMBER_2, BTN_NUMBER_8}},
    {{OUTPUT_CH_4, OUTPUT_CH_3}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_SHADE,
    {{BTN_NUMBER_3, BTN_NUMBER_9}},
    {{OUTPUT_CH_6, OUTPUT_CH_5}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_4L1SH_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_1,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_2,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_7,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_3,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_8,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_4,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP5 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_SHADE,
    {{BTN_NUMBER_3, BTN_NUMBER_9}},
    {{OUTPUT_CH_6, OUTPUT_CH_5}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_2L1SH_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_1,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_7,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_SHADE,
    {{BTN_NUMBER_3, BTN_NUMBER_9}},
    {{OUTPUT_CH_6, OUTPUT_CH_5}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_2L2SH_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_1,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_RELAY_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_7,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_2,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_SHADE,
    {{BTN_NUMBER_2, BTN_NUMBER_8}},
    {{OUTPUT_CH_4, OUTPUT_CH_3}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
    EP_MODE_BLINDS_SWITCH,
    BTN_MODE_BLINDS_SHADE,
    {{BTN_NUMBER_3, BTN_NUMBER_9}},
    {{OUTPUT_CH_6, OUTPUT_CH_5}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_9R_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_REMOTE_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_1,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_NONE,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
    EP_MODE_REMOTE_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_2,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_NONE,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP3 */
    EP_MODE_REMOTE_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_3,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_NONE,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP4 */
    EP_MODE_REMOTE_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_4,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_NONE,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP5 */
    EP_MODE_REMOTE_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_5,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_NONE,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP6 */
    EP_MODE_REMOTE_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_6,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_NONE,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP7 */
    EP_MODE_REMOTE_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_7,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_NONE,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP8 */
    EP_MODE_REMOTE_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_8,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_NONE,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP9 */
    EP_MODE_REMOTE_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BTN_NUMBER_9,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_NONE,OUTPUT_CH_NONE}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  }
};

const EndpointConfig_t SW_CONFIG_B_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_BOILER_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{BOILER_ON_OFF_BUTTON,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1, OUTPUT_CH_2}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
      EP_Disable
  },
  { /* EP3 */
      EP_Disable
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const EndpointConfig_t SW_CONFIG_F_PRESET [EP_NUM] =
{
  { /* EP1 */
    EP_MODE_FAN_SWITCH,
    BTN_MODE_SWITCH_ON_OFF,
    {{FAN_ON_OFF_BUTTON,BTN_NUMBER_NONE}},
    {{OUTPUT_CH_1, OUTPUT_CH_2}},
    ON_EXCLUDE_OFF_INCLUDE,
    16,
    OffState,
    SCENE_MASTER_ALL_OFF
  },
  { /* EP2 */
      EP_Disable
  },
  { /* EP3 */
      EP_Disable
  },
  { /* EP4 */
      EP_Disable
  },
  { /* EP5 */
      EP_Disable
  },
  { /* EP6 */
      EP_Disable
  },
  { /* EP7 */
      EP_Disable
  },
  { /* EP8 */
      EP_Disable
  },
  { /* EP9 */
      EP_Disable
  }
};

const BacklightConfig_t SW_CONFIG_1L_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_2L_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_3L_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_4L_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_6L_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_1DT_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_2DT_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_3DT_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_4DT_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_D_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_1D_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_2D_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_4DT2D_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_D_BL_PRESET,
    SW_CONFIG_D_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_1M_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_2M_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_3M_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_M_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_4L1M_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_2L1M_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_2L2M_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_M_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_1SH_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_2SH_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_3SH_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_M_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_4L1SH_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_2L1SH_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_2L2SH_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_L_BL_PRESET,
    SW_CONFIG_M_BL_PRESET,
    SW_CONFIG_M_BL_PRESET
};


const BacklightConfig_t SW_CONFIG_9R_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_M_D_PRESET,
    SW_CONFIG_M_D_PRESET,
    SW_CONFIG_M_D_PRESET,
    SW_CONFIG_M_D_PRESET,
    SW_CONFIG_M_D_PRESET,
    SW_CONFIG_M_D_PRESET,
    SW_CONFIG_M_D_PRESET,
    SW_CONFIG_M_D_PRESET,
    SW_CONFIG_M_D_PRESET
};

const BacklightConfig_t SW_CONFIG_B_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_BOIL_BL_PRESET,
    SW_CONFIG_BOIL_BL_PRESET,
    SW_CONFIG_BOIL_BL_PRESET,
    SW_CONFIG_BOIL_BL_PRESET,
    SW_CONFIG_BOIL_BL_PRESET,
    SW_CONFIG_BOIL_BL_PRESET,
    SW_CONFIG_BOIL_BL_PRESET,
    SW_CONFIG_BOIL_BL_PRESET,
    SW_CONFIG_BOIL_BL_PRESET
};

const BacklightConfig_t SW_CONFIG_F_BL_PRESET[BTN_NUMBER_MAX - BTN_NUMBER_1] = {
    SW_CONFIG_FAN_BL_PRESET,
    SW_CONFIG_FAN_BL_PRESET,
    SW_CONFIG_FAN_BL_PRESET,
    SW_CONFIG_FAN_BL_PRESET,
    SW_CONFIG_FAN_BL_PRESET,
    SW_CONFIG_FAN_BL_PRESET,
    SW_CONFIG_FAN_BL_PRESET,
    SW_CONFIG_FAN_BL_PRESET,
    SW_CONFIG_FAN_BL_PRESET
};

