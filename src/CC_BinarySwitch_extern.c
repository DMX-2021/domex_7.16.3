
#include "CC_BinarySwitch.h"
#include "EndpointModule.h"
#include "BoardDedicatedModule.h"
#include "zaf_event_helper.h"
#include "events.h"
#include "BackGroundTimer.h"
#include "stddef.h"
#include "string.h"
#include "Touchcontroller.h"
#include "EndpointModule.h"
#include "Agi_Trans.h"

extern uint8_t supportingBinarySwitch_list[SWITCH_BINARY_ENDPOINTS];
extern int AGI_Delay;
extern char CanSend_Start_AGI;
bool IsKeyPress;
s_CC_binarySwitch_data_t ZAF_TSE_BinarySwitchData[SWITCH_BINARY_ENDPOINTS];
extern uint32_t my_ZAF_TSE_DELAY_TRIGGER;
extern e_cmd_handler_return_code_t CC_Basic_Set_handler(uint8_t val, uint8_t endpoint);

uint8_t appBinarySwitchGetFactoryDefaultDuration(uint8_t endpoint) {
  UNUSED(endpoint);
  return 0;
}

/**
 * @details This function will be invoked when a Binary Switch Get command
 *          is received by the root device or by the endpoint that supports
 *          the Binary Switch CC.
 *          See prototype for more information.
 *
 * @param endpoint - number of enpoint
 * @return Current value of BinarySwitch
 */
CMD_CLASS_BIN_SW_VAL
appBinarySwitchGetCurrentValue(uint8_t endpoint)
{
  CMD_CLASS_BIN_SW_VAL ret = CMD_CLASS_BIN_OFF;
  EndpointInfo_t* epInfo;

  if(endpoint == ENDPOINT_ROOT) {
    endpoint = ENDPOINT_1;
  }

  epInfo = EndpointInfo_Get_byNumber(endpoint);
  if (epInfo == NULL) {
    goto exit;
  }

  switch (epInfo->config.EpMode) {
    case EP_MODE_RELAY_SWITCH:
      if(epInfo->relayStatus.state == SWITCH_STATE_ON)
        ret = CMD_CLASS_BIN_ON;
      else if(epInfo->relayStatus.state == SWITCH_STATE_OFF)
        ret = CMD_CLASS_BIN_OFF;
      else
        ret = CMD_CLASS_BIN_UNKNOWN;
      break;

    case EP_MODE_BLINDS_SWITCH:
      if(epInfo->blindsStatus.state == BLINDS_STATE_OPEN)
        ret = CMD_CLASS_BIN_ON;
      else if(epInfo->blindsStatus.state == BLINDS_STATE_CLOSE)
        ret = CMD_CLASS_BIN_OFF;
      else
        ret = CMD_CLASS_BIN_UNKNOWN;
      break;

    case EP_MODE_BOILER_SWITCH:
      if(epInfo->boilerStatus.state == SWITCH_STATE_ON)
        ret = CMD_CLASS_BIN_ON;
      else if(epInfo->boilerStatus.state == SWITCH_STATE_OFF)
        ret = CMD_CLASS_BIN_OFF;
      else
        ret = CMD_CLASS_BIN_UNKNOWN;
      break;
    case EP_MODE_FAN_SWITCH:
      if(epInfo->fanStatus.state > FAN_STATE_OFF)
        ret = CMD_CLASS_BIN_ON;
      else if(epInfo->fanStatus.state == FAN_STATE_OFF)
        ret = CMD_CLASS_BIN_OFF;
      else
        ret = CMD_CLASS_BIN_UNKNOWN;
      break;

    default:
      ret = CMD_CLASS_BIN_UNKNOWN;
      goto exit;
      break;
  }

  exit: return ret;
}

/**
 * @details This function will be invoked when a Binary Switch Get command
 *          is received by the root device or by the endpoint that supports
 *          the Binary Switch CC.
 *          See prototype for more information.
 *
 * @param endpoint - number of enpoint
 * @return Target value of BinarySwitch
 */
CMD_CLASS_BIN_SW_VAL
appBinarySwitchGetTargetValue(uint8_t endpoint)
{
  UNUSED(endpoint);
  //! Always report target value equal to current value
  return appBinarySwitchGetCurrentValue(endpoint);
}

/**
 * @details This function will be invoked when a Binary Switch Get command
 *          is received by the root device or by the endpoint that supports
 *          the Binary Switch CC.
 *          See prototype for more information.
 *
 * @param endpoint - number of enpoint
 * @return Duration of BinarySwitch
 */
uint8_t
appBinarySwitchGetDuration(uint8_t endpoint)
{
  UNUSED(endpoint);

  /**
   * Always report instant transition; the only reasonable transition for
   * a binary switch
   */
  return 0;
}

/**
 * @details This function will be invoked when a Binary Switch Set command
 *          is received by the root device or by the endpoint that supports
 *          the Binary Switch CC.
 *          See prototype for more information.
 *
 * @param val - value to set
 * @param duration - duration switch transition duration (encoded according
 *                   to Binary Switch CC specification)
 * @param endpoint - number of enpoint
 * @return command handler return code
 */
e_cmd_handler_return_code_t
appBinarySwitchSet(CMD_CLASS_BIN_SW_VAL val, uint8_t duration, uint8_t endpoint)
{
  UNUSED(duration); //! Ignore duration - for a binary switch only instant
                    //! transitions make sense
  BoardErr_t boardRet = BOARD_ERR;
  e_cmd_handler_return_code_t ret = E_CMD_HANDLER_RETURN_CODE_NOT_SUPPORTED;
  EndpointInfo_t* epInfo;
//  BlindsConfig_t blindsConfig;
  RECEIVE_OPTIONS_TYPE_EX rxOpt = { 0 };
  void *ZAF_TSE_data = NULL;

  if (endpoint == ENDPOINT_ROOT) {
    endpoint = ENDPOINT_1;
  }

  epInfo = EndpointInfo_Get_byNumber(endpoint);
  if (epInfo == NULL) {
    goto exit;
  }

  switch (epInfo->config.EpMode) {
    case EP_MODE_RELAY_SWITCH:
      if (val == CMD_CLASS_BIN_ON) {
        epInfo->relayStatus.state = SWITCH_STATE_ON;
        boardRet = Board_RelaySet(epInfo->config.Output,
                                  RELAY_STATE_ACTIVE);
        if (boardRet == BOARD_OK)
          ret = E_CMD_HANDLER_RETURN_CODE_HANDLED;
      } else if (val == CMD_CLASS_BIN_OFF) {
        epInfo->relayStatus.state = SWITCH_STATE_OFF;
        boardRet = Board_RelaySet(epInfo->config.Output,
                                  RELAY_STATE_INACTIVE);
        if (boardRet == BOARD_OK)
          ret = E_CMD_HANDLER_RETURN_CODE_HANDLED;
      }

      if (boardRet == BOARD_OK) {
        ret = E_CMD_HANDLER_RETURN_CODE_HANDLED;
        rxOpt.destNode.endpoint = epInfo->number;
        ZAF_TSE_data = CC_BinarySwitch_prepare_zaf_tse_data(&rxOpt);
        my_ZAF_TSE_DELAY_TRIGGER = epInfo->config.ZAF_TSE_Timeout<<8;
        ZAF_TSE_Trigger((void *)CC_BinarySwitch_report_stx, ZAF_TSE_data,true);
      } else {
        ret = E_CMD_HANDLER_RETURN_CODE_FAIL;
      }
      break;

    case EP_MODE_BLINDS_SWITCH:
      /*
      blindsConfig = DeviceConfiguration_Get_Blinds(epInfo->number);
      ret = cc_multilevel_switch_set(NULL, val, (val) ? blindsConfig.durationUpSec : blindsConfig.durationDownSec); //TODO - change NULL
      rxOpt.destNode.endpoint = epInfo->number;
      ZAF_TSE_data = CC_MultilevelSwitch_prepare_zaf_tse_data(&rxOpt);
      my_ZAF_TSE_DELAY_TRIGGER = epInfo->config.ZAF_TSE_Timeout<<8;
      ZAF_TSE_Trigger((void *)CC_MultilevelSwitch_report_stx, ZAF_TSE_data,true);
      */
      break;

    case EP_MODE_BOILER_SWITCH:
      //! Use basic handler as the same
      CC_Basic_Set_handler(val, epInfo->number);
      rxOpt.destNode.endpoint = epInfo->number;
      ZAF_TSE_data = CC_BinarySwitch_prepare_zaf_tse_data(&rxOpt);
      my_ZAF_TSE_DELAY_TRIGGER = epInfo->config.ZAF_TSE_Timeout<<8;
      ZAF_TSE_Trigger((void *)CC_BinarySwitch_report_stx, ZAF_TSE_data,true);
      break;
    case EP_MODE_FAN_SWITCH:
      //! Use basic handler as the same
      CC_Basic_Set_handler(val, epInfo->number);
      rxOpt.destNode.endpoint = epInfo->number;
      ZAF_TSE_data = CC_BinarySwitch_prepare_zaf_tse_data(&rxOpt);
      my_ZAF_TSE_DELAY_TRIGGER = epInfo->config.ZAF_TSE_Timeout<<8;
      ZAF_TSE_Trigger((void *)CC_BinarySwitch_report_stx, ZAF_TSE_data,true);
      break;

    default:
      ret = E_CMD_HANDLER_RETURN_CODE_NOT_SUPPORTED;
      break;
  }
  ZAF_EventHelperEventEnqueue(EVENT_APP_REFRESH_MMI);
  if ((IsKeyPress)||(epInfo->config.Output1!=OUTPUT_CH_NONE)||(epInfo->config.Output2!=OUTPUT_CH_NONE))
     {
       if ((CanSend_Start_AGI)||(IsKeyPress))
         {
           wc_prepareAGITransmission((touchButton_t) epInfo->config.BtnNumber.Btn1, epInfo,TRANSMISSION_CMD_BASIC_SET);
           CanSend_Start_AGI=false;
           AGI_Delay=SEC_TO_TIMER_TIKS(0.5);
           BackGroundTimer_TimerStart();
         }
     }
  EndpointConfig_t EpConfig = { 0 };
  DeviceConfiguration_Get_Ep(epInfo->number, &EpConfig);
  memcpy(&EpConfig.OffOnState, &val,1);
  DeviceConfiguration_Set_Ep(epInfo->number, EpConfig,false);
  DeviceConfiguration_Commit();
  exit: return ret;
}
/**
 * @brief Get ep index by its number
 * @note Considering several endpoints supporting the same Command Class,
 *       this function finds the index of the end point if they were stored in
 *       an array.
 *       e.g. if Endpoints 0, 3, 5 and 12 support the Command Class, their
 *       index would be respectively 0, 1, 2 and 3.
 *
 * @param ep_number - Indicating the endpoint identifier to search for.
 * @return endpoint index
 */
uint8_t
CC_BinarySwitch_getEndpointIndex(uint8_t ep_number)
{
  for(uint8_t ep_index = 0; ep_index < SWITCH_BINARY_ENDPOINTS; ++ep_index) {
    if(ep_number == supportingBinarySwitch_list[ep_index]) {
      return ep_index;
    }
  }
  /**
   * End point was not found, this should not happen but in this case,
   * just return the first End Point
   */
  return 0;
}


/**
 * @brief Prepare the data input for the TSE for any Binary Switch CC command
 *        based on the pRxOption pointer
 *
 * @param pRxOpt - pointer used to indicate how the frame was received (if any)
 *        and what endpoints are affected
 * @return pointer to ZAF TSE data
 */
void*
CC_BinarySwitch_prepare_zaf_tse_data(RECEIVE_OPTIONS_TYPE_EX* pRxOpt)
{
  RECEIVE_OPTIONS_TYPE_EX *EP_pRxOpt;
  uint8_t endpoint_index = CC_BinarySwitch_getEndpointIndex(pRxOpt->destNode.endpoint);

  memset(&ZAF_TSE_BinarySwitchData[endpoint_index], 0, sizeof(s_CC_binarySwitch_data_t));

  EP_pRxOpt = &ZAF_TSE_BinarySwitchData[endpoint_index].rxOptions;
  memcpy(EP_pRxOpt, pRxOpt, sizeof(RECEIVE_OPTIONS_TYPE_EX));

  if (ENDPOINT_ROOT == EP_pRxOpt->destNode.endpoint) {
    EP_pRxOpt->destNode.endpoint = ENDPOINT_1;
  }

  return &ZAF_TSE_BinarySwitchData[endpoint_index];
}

