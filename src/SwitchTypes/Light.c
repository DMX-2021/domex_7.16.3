/*
 * Light.c
 *
 *  Created on: 21 בנוב׳ 2021
 *      Author: User
 */
#include "stddef.h"
#include "config_app.h"
#include "TouchController.h"
#include "EndpointModule.h"
#include "ConfigurationModule.h"
#include "drv2603Waveforms.h"
#include "CC_BinarySwitch.h"

extern bool IsKeyPress;

void
TouchContorller_SwitchOnOff(touchButton_t btn, touchButtonEvent_t evt, void* args)
{
  UNUSED(btn);
  EndpointInfo_t* epInfo = (EndpointInfo_t*) args;
  switchState_t *pSwitchState = &epInfo->relayStatus.state;

  if (evt == TOUCH_BTN_PUSH_EVENT)
    DRV2603_SendWaveform(doubleClickWaveform);

  if (evt != TOUCH_BTN_PUSH_EVENT)
    return;

  if (epInfo->config.EpMode == EP_MODE_RELAY_SWITCH) {
      IsKeyPress=true;
      appBinarySwitchSet((*pSwitchState == SWITCH_STATE_OFF ? CMD_CLASS_BIN_ON : CMD_CLASS_BIN_OFF), 0, epInfo->number);
  }
}
