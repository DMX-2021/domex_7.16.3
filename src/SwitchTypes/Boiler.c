/*
 * Boiler.c
 *
 *  Created on: 21 בנוב׳ 2021
 *      Author: User
 */
#include "stddef.h"
#include "config_app.h"
#include "TouchController.h"
#include "EndpointModule.h"
#include "ConfigurationModule.h"
#include "drv2603Waveforms.h"

extern e_cmd_handler_return_code_t CC_Basic_Set_handler(uint8_t val, uint8_t endpoint);
void
Boiler_KeyPress(touchButton_t btn, touchButtonEvent_t evt, void* args)
{
  UNUSED(args);
  EndpointInfo_t* epInfo = NULL;
  boilerSwitchStatus_t *pBoilerStatus = NULL;
  IndicationConfig_t indicationConfig = DeviceConfiguration_Get_Indication();
  BoilerConfig_t boilerConfig = DeviceConfiguration_Get_Boiler();
  SW_Config_t SW_Config = DeviceConfiguration_Get_SW();

  if(evt == TOUCH_BTN_PUSH_EVENT) {
    if(indicationConfig.hapticEnable)
      DRV2603_SendWaveform(doubleClickWaveform);
  }

  //! Receive only short press events
  if(evt != TOUCH_BTN_SHORT_PRESS_EVENT)
    return;
  if (SW_Config == SW_CONFIG_B)
    {
        epInfo = EndpointInfo_Get_byNumber(BOILER_EP_NUMB);
        if(epInfo == NULL)
         return;

        pBoilerStatus = &epInfo->boilerStatus;

        switch (btn) {
          case BOILER_ON_OFF_BUTTON:
            if (pBoilerStatus->state == SWITCH_STATE_ON) {
              CC_Basic_Set_handler(0, epInfo->number);
            } else {
              CC_Basic_Set_handler(boilerConfig.maxBoilingTime+1, epInfo->number);
            }
            break;

          case BOILER_1_6TH_MIN_BUTTON:
            CC_Basic_Set_handler(BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 1, epInfo->number);
            break;

          case BOILER_2_6TH_MIN_BUTTON:
            CC_Basic_Set_handler(BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 2, epInfo->number);
            break;

          case BOILER_3_6TH_MIN_BUTTON:
            CC_Basic_Set_handler(BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 3, epInfo->number);
            break;

          case BOILER_4_6TH_MIN_BUTTON:
            CC_Basic_Set_handler(BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 4, epInfo->number);
            break;

          case BOILER_5_6TH_MIN_BUTTON:
            CC_Basic_Set_handler(BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 5, epInfo->number);
            break;

          case BOILER_6_6TH_MIN_BUTTON:
            CC_Basic_Set_handler(BOILER_GET_ONE_PART_OF_MAX(boilerConfig.maxBoilingTime) * 6, epInfo->number);
            break;

          default:
            break;
        }
    }
  if (SW_Config == SW_CONFIG_F)
    {
        epInfo = EndpointInfo_Get_byNumber(FAN_EP_NUMB);
        if(epInfo == NULL)
         return;
        CC_Basic_Set_handler(btn, epInfo->number);
    }


          /*
  //! Report about changing
  rxOpt.destNode.endpoint = epInfo->number;
  ZAF_TSE_data = CC_BinarySwitch_prepare_zaf_tse_data(&rxOpt);
  ZAF_ret = ZAF_TSE_Trigger((void *) CC_BinarySwitch_report_stx,
                           ZAF_TSE_data, true);
  ZAF_CHECK(ZAF_ret);
  //AppTransmitCommand(epInfo, TRANSMISSION_CMD_BASIC_SET);*/
}

