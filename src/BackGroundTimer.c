/**
 * @file MLS_LevelChangeUtility.h
 * @brief Utility helps changing multilevel switch levev
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#include <BackGroundTimer.h>
#include "SwTimer.h"
#include "ZW_typedefs.h"
#include "AppTimer.h"
/*
#include "CC_MultilevelSwitch.h"
#include "ZAF_TSE.h"
#include "BoardDedicatedModule.h"
#include "EndpointModule.h"
#include "zaf_event_helper.h"
#include "ev_man.h"
#include "events.h"
#include "TouchController_Handlers.h"

*/
SSwTimer BackGroundTimer_TimerHandle;
int AGI_Delay;
char CanSend_Start_AGI;
/*
extern char NeedLedToBlink;
extern char FadeOutFactor;
extern int LedsFadeOut;


extern int DimmerDelay;
extern uint32_t my_ZAF_TSE_DELAY_TRIGGER;
extern uint16_t on_time_ms_Count;
extern uint16_t off_time_ms_Count;
extern uint8_t blinker_cycles_Count;
extern uint16_t on_time_ms;
extern uint16_t off_time_ms;
*/
/**
 * @brief Level change timer callback
 *
 * @param pTimer - timer handle
 */
static void
vBackGroundTimer_Callback(SSwTimer* pTimer)
{
  UNUSED(pTimer);
  /*
  EndpointInfo_t* epInfo = NULL;
  EpMode_t epMode = { 0 };
  BtnMode_t btnMode = { 0 };
  blindsSwitchStatus_t *pBlindsStatus = NULL;
  boilerSwitchStatus_t *pBoilerStatus = NULL;
  void *ZAF_TSE_data = NULL;
  static RECEIVE_OPTIONS_TYPE_EX rxOpt = { 0 };
  bool ZAF_ret = false;
//  bool needStopTimer = true;
  if (AGI_Delay>0)
    {
//      needStopTimer = false;
      AGI_Delay--;
      if (AGI_Delay==0)
        {
          CanSend_Start_AGI=true;
        }
    }
  if (DimmerDelay>0)
    {
//      needStopTimer = false;
      DimmerDelay--;
      if (DimmerDelay==0)
         {
          for (int ep = 0; ep <= SWITCH_DIMMER_ENDPOINTS_MAX; ep++)
            {
              epInfo = EndpointInfo_Get_byNumber(ep);
              if (epInfo->dimmerStatus.CalibrationStatus==StartCal)
                {
                  epInfo->dimmerStatus.CalibrationStatus=NotCal;
                  epInfo->dimmerStatus.DimmerDirection=DownDirection;
                }
            }
          ZAF_EventHelperEventEnqueue(EVENT_APP_REFRESH_MMI);
        }
    }
  if (blinker_cycles_Count>0)
    {
//      needStopTimer = false;
      if (on_time_ms_Count>0)
        {
          on_time_ms_Count--;
        }
      else
        {
          if (off_time_ms_Count>0)
            {
              off_time_ms_Count--;
            }
          else
            {
              blinker_cycles_Count--;
              if (blinker_cycles_Count>0)
                {
                  on_time_ms_Count=MS_TO_TIMER_TIKS(on_time_ms);
                  off_time_ms_Count=MS_TO_TIMER_TIKS(off_time_ms);
                }
            }
        }
      ZAF_EventHelperEventEnqueue(EVENT_APP_REFRESH_MMI);
    }
  if (LedsFadeOut>0)
    {
//        needStopTimer = false;
        switch(LedsFadeOut--)
        {
          case SEC_TO_TIMER_TIKS(15):
                  FadeOutFactor=1;
                  ZAF_EventHelperEventEnqueue(EVENT_APP_REFRESH_MMI);
          break;
          case SEC_TO_TIMER_TIKS(7):
                  FadeOutFactor=2;
                  ZAF_EventHelperEventEnqueue(EVENT_APP_REFRESH_MMI);
          break;
          case SEC_TO_TIMER_TIKS(1):
                  FadeOutFactor=3;
//                  needStopTimer = true;
                  ZAF_EventHelperEventEnqueue(EVENT_APP_REFRESH_MMI);
          break;
          default:
          break;
        }
    }
  for (int ep = 0; ep < EP_NUM; ep++) {
    epInfo = EndpointInfo_Get_byNumber(ep);
    epMode = epInfo->config.EpMode;
    btnMode = epInfo->config.EpMode;

    if (epMode == EP_MODE_BLINDS_SWITCH ) {

      pBlindsStatus = &epInfo->blindsStatus;
      if (!pBlindsStatus->isLvlChanging)
        continue;
//      else
//        needStopTimer = false; //! Don't stop timer if any blinds is changing

      if (--pBlindsStatus->tiksCount == 0) {
        pBlindsStatus->isLvlChanging = false;
        ZAF_EventHelperEventEnqueue(EVENT_APP_REFRESH_MMI);

        if(epMode == EP_MODE_BLINDS_SWITCH) {
          rxOpt.destNode.endpoint = epInfo->number;
          Board_BlindsStop(epInfo->config.Output1, epInfo->config.Output2);
          if (NeedLedToBlink)
            {
              pBlindsStatus->isLvlChanging = true;
              pBlindsStatus->tiksCount = SEC_TO_TIMER_TIKS(6);
              LevelChangeUtility_TimerStart();
              ZAF_EventHelperEventEnqueue(EVENT_APP_REFRESH_MMI);
              NeedLedToBlink=false;
            }
          else
            {
              ZAF_TSE_data = CC_MultilevelSwitch_prepare_zaf_tse_data(&rxOpt);
              my_ZAF_TSE_DELAY_TRIGGER = epInfo->config.ZAF_TSE_Timeout<<8;
              ZAF_ret = ZAF_TSE_Trigger((void *) CC_MultilevelSwitch_report_stx, ZAF_TSE_data, true);
              if(ZAF_ret == false) {
                LOG_WARNING("ZAF_TSE_Trigger failed!");
                  }
            }
        }
        LOG_DEBUG("Stop blinds #%d by timeout", epInfo->number);
      }
    }
    else if (epMode == EP_MODE_BOILER_SWITCH) {
      pBoilerStatus = &epInfo->boilerStatus;
      if (!pBoilerStatus->isTimerActive)
        continue;
//      else
//        needStopTimer = false; //! Don't stop timer if any blinds is changing

      if(--pBoilerStatus->tiksCount == 0) {
        pBoilerStatus->isTimerActive = false;
        pBoilerStatus->state = SWITCH_STATE_OFF;

        ZAF_EventHelperEventEnqueue(EVENT_APP_REFRESH_MMI);

        rxOpt.destNode.endpoint = epInfo->number;
        Board_BoilerDeactivate(epInfo->config.Output1,
                               epInfo->config.Output2);
        ZAF_TSE_data = CC_BinarySwitch_prepare_zaf_tse_data(&rxOpt);
        my_ZAF_TSE_DELAY_TRIGGER = epInfo->config.ZAF_TSE_Timeout<<8;
        ZAF_ret = ZAF_TSE_Trigger((void *) CC_BinarySwitch_report_stx,
                                  ZAF_TSE_data,
                                  true);
        if (ZAF_ret == false) {
          LOG_WARNING("ZAF_TSE_Trigger failed!");
        }

        LOG_DEBUG("Stop boiler #%d by timeout", epInfo->number);
      }
      else if(TIMER_TIKS_TO_MS(pBoilerStatus->tiksCount) % BOILER_REFRESH_MMI_PRERIOD == 0) {
        ZAF_EventHelperEventEnqueue(EVENT_APP_REFRESH_MMI);
      }
    }
  }
  // There's no need to keep the levelChange timer running if all
  //  ep has been changed


//  if (needStopTimer) {
//    TimerStop(&lvlChangeTimerHandle);
//  }
  */
}

/**
 * @brief Initialization of level change timer
 *
 * @return true - success
 *         false - failed
 */
bool
BackGroundTimer_TimerInit(void)
{
  bool ret = false;
  ret = AppTimerRegister(&BackGroundTimer_TimerHandle, true, vBackGroundTimer_Callback);
  return ret;
}

/**
 * @brief Starting level change timer
 *
 * @return true - success
 *         false - failed
 */
bool
BackGroundTimer_TimerStart(void)
{
  ESwTimerStatus ret = ESWTIMER_STATUS_SUCCESS;

  //! Starts timer if it was stopped
  if(TimerIsActive(&BackGroundTimer_TimerHandle) == false)
    ret = TimerStart(&BackGroundTimer_TimerHandle, TIMER_PERIOD_MS);

  return ret;
}
