/**
 * @file cpt212b_config.h
 * @brief CPT212B driver header file
 *
 * @details This file contains configurations parameters and functions for
 *          CPT212B Touch Controller
 *
 * @ref https://www.silabs.com/documents/public/data-sheets/cpt212b-datasheet.pdf
 *      https://www.silabs.com/documents/public/application-notes/an1092-touchxpress-custom-configuration-profile-programming-guide.pdf
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#ifndef CPT212B_CONFIG_H
#define CPT212B_CONFIG_H

//! Define Endian-ness:
//! Big Endian Compilers: No swapping necessary
#if ((defined __C51__) || (defined __RC51__) || (defined _CC51))
#define htobe16(x) (x)

//! Little Endian Compilers: Bit swapping necessary
#elif ((defined __GNUC__)|| (defined SDCC) || (defined HI_TECH_C) || (defined __ICC8051__))
#define htobe16(x) (((x) >> 8) | ((x & 0xFF) << 8))

#else
#error "Compiler not defined. Endian-ness of the compiler cannot be determined, and is necessary for CPT2xxx device configuration."
#endif

struct config_profile
{                                       //  Min-Max |  Default
  uint8_t reserved_0[3];                //      0-0 |       0
  uint8_t active_threshold[12];         //    1-100 |      60
  uint8_t inactive_threshold[12];       //    1-100 |      40
  uint8_t average_touch_delta[12];      //    1-255 |      43
  uint8_t accumulation[12];             //      0-5 |       2
  uint8_t gain[12];                     //      0-7 |       6
  uint16_t active_mode_period;          //   1-5000 |      20
  uint16_t sleep_mode_period;           //   1-5000 |     200
  uint8_t button_debounce;              //     1-15 |       2
  uint8_t active_mode_mask[2];          //      0-1 |       1
  uint8_t sleep_mode_mask[2];           //      0-1 |       1
  uint8_t counts_before_sleep;          //    2-255 |     100
  uint8_t active_mode_scan_type;        //      0-1 |       1
  uint8_t reserved_1[4];                //      0-0 |       0
  uint8_t buzzer_output_duration;       //    0-255 |      16
  uint8_t buzzer_output_frequency;      //    0-100 |     100
  uint8_t buzzer_drive_strength;        //      0-1 |       1
  uint8_t touch_time_duration;          //     0-14 |       0
  uint8_t mutex_buttons;                //      0-1 |       0
  uint8_t prox_settings;                //     0-15 |       0
  uint8_t i2c_slave_address;            //    0-255 |     224
  uint8_t reserved_2;                   //      0-0 |       0
  uint8_t i2c_timeout_duration;         //     0-15 |       1
  uint8_t cs00_sensor_string[10];       //          |  "CS00"
  uint8_t cs01_sensor_string[10];       //          |  "CS01"
  uint8_t cs02_sensor_string[10];       //          |  "CS02"
  uint8_t cs03_sensor_string[10];       //          |  "CS03"
  uint8_t cs04_sensor_string[10];       //          |  "CS04"
  uint8_t cs05_sensor_string[10];       //          |  "CS05"
  uint8_t cs06_sensor_string[10];       //          |  "CS06"
  uint8_t cs07_sensor_string[10];       //          |  "CS07"
  uint8_t cs08_sensor_string[10];       //          |  "CS08"
  uint8_t cs09_sensor_string[10];       //          |  "CS09"
  uint8_t cs10_sensor_string[10];       //          |  "CS10"
  uint8_t cs11_sensor_string[10];       //          |  "CS11"
  uint8_t reserved_3;                   //          |     255
}__attribute__((packed));
typedef struct config_profile config_profile_t;

#define CPT212B_ACTIVE_TH       0x64
#define CPT212B_INACTIVE_TH     0x46
//#define CPT212B_ACTIVE_TH       0x04
//#define CPT212B_INACTIVE_TH     0x01
//#define CPT212B_INACTIVE_TH     0x5A
#define CPT212B_AVG_TOUCH_DELTA 0x02
//#define CPT212B_AVG_TOUCH_DELTA 0x10
//#define CPT212B_ACCUMULATION    0x00
#define CPT212B_ACCUMULATION    0x00
#define CPT212B_GAIN            0x07
// This structure must be transmitted to the CPT device in Big-endian format!
#define CPT212B_A01_GM_DEFAULT_CONFIG \
{ \
  {0x00, 0x00, 0x00},                                                              /* reserved_0 */ \
  {CPT212B_ACTIVE_TH, CPT212B_ACTIVE_TH, CPT212B_ACTIVE_TH, CPT212B_ACTIVE_TH, CPT212B_ACTIVE_TH, CPT212B_ACTIVE_TH, CPT212B_ACTIVE_TH, CPT212B_ACTIVE_TH, CPT212B_ACTIVE_TH, CPT212B_ACTIVE_TH, CPT212B_ACTIVE_TH, CPT212B_ACTIVE_TH}, /* Active Threshold */ \
  {CPT212B_INACTIVE_TH, CPT212B_INACTIVE_TH, CPT212B_INACTIVE_TH, CPT212B_INACTIVE_TH, CPT212B_INACTIVE_TH, CPT212B_INACTIVE_TH, CPT212B_INACTIVE_TH, CPT212B_INACTIVE_TH, CPT212B_INACTIVE_TH, CPT212B_INACTIVE_TH, CPT212B_INACTIVE_TH, CPT212B_INACTIVE_TH}, /* Inactive Threshold */ \
  {CPT212B_AVG_TOUCH_DELTA, CPT212B_AVG_TOUCH_DELTA, CPT212B_AVG_TOUCH_DELTA, CPT212B_AVG_TOUCH_DELTA, CPT212B_AVG_TOUCH_DELTA, CPT212B_AVG_TOUCH_DELTA, CPT212B_AVG_TOUCH_DELTA, CPT212B_AVG_TOUCH_DELTA, CPT212B_AVG_TOUCH_DELTA, CPT212B_AVG_TOUCH_DELTA, CPT212B_AVG_TOUCH_DELTA, CPT212B_AVG_TOUCH_DELTA}, /* Average Touch Delta */ \
  {CPT212B_ACCUMULATION, CPT212B_ACCUMULATION, CPT212B_ACCUMULATION, CPT212B_ACCUMULATION, CPT212B_ACCUMULATION, CPT212B_ACCUMULATION, CPT212B_ACCUMULATION, CPT212B_ACCUMULATION, CPT212B_ACCUMULATION, CPT212B_ACCUMULATION, CPT212B_ACCUMULATION, CPT212B_ACCUMULATION},        /* Accumulation */ \
  {CPT212B_GAIN, CPT212B_GAIN, CPT212B_GAIN, CPT212B_GAIN, CPT212B_GAIN, CPT212B_GAIN, CPT212B_GAIN, CPT212B_GAIN, CPT212B_GAIN, CPT212B_GAIN, CPT212B_GAIN, CPT212B_GAIN},        /* Gain */ \
  htobe16(0x1388),                                                                 /* active mode period */ \
  htobe16(0x0001),                                                                 /* sleep mode period */ \
  0x0F,                                                                            /* button debounce */ \
  {0x7B, 0x07},                                                                    /* active mode mask */ \
  {0x7B, 0x07},                                                                    /* sleep mode mask */ \
  0xFF,                                                                            /* counts before sleep */ \
  0x01,                                                                            /* active mode scan type */ \
  {0x00, 0x00, 0x00, 0x00},                                                        /* reserved_1 */ \
  0x10,                                                                            /* buzzer output duration */ \
  0x64,                                                                            /* buzzer output frequency */ \
  0x01,                                                                            /* buzzer drive strength */ \
  0x00,                                                                            /* touch time duration */ \
  0x01,                                                                            /* mutex buttons */ \
  0x00,                                                                            /* prox settings */ \
  0xE0,                                                                            /* I2C slave address */ \
  0x00,                                                                            /* reserved_2 */ \
  0x01,                                                                            /* I2C timeout duration */ \
  {'C', 'S', '0', '0', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'},            /* CS00 sensor string */ \
  {'C', 'S', '0', '1', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'},            /* CS01 sensor string */ \
  {'C', 'S', '0', '2', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'},            /* CS02 sensor string */ \
  {'C', 'S', '0', '3', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'},            /* CS03 sensor string */ \
  {'C', 'S', '0', '4', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'},            /* CS04 sensor string */ \
  {'C', 'S', '0', '5', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'},            /* CS05 sensor string */ \
  {'C', 'S', '0', '6', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'},            /* CS06 sensor string */ \
  {'C', 'S', '0', '7', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'},            /* CS07 sensor string */ \
  {'C', 'S', '0', '8', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'},            /* CS08 sensor string */ \
  {'C', 'S', '0', '9', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'},            /* CS09 sensor string */ \
  {'C', 'S', '1', '0', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'},            /* CS10 sensor string */ \
  {'C', 'S', '1', '1', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'},            /* CS11 sensor string */ \
  0xFF                                                                             /* reserved_3 */ \
}

uint16_t
generateConfigProfileCRC (config_profile_t* profile);

#endif //!CPT212B_CONFIG_H
