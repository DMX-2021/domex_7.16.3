/**
 * @file cpt212b.h
 * @brief CPT212B driver header file
 *
 * @details This file contains definitions and basic functions for CPT212B
 *          Touch Controller
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#ifndef CPT212B_DRIVER_H
#define CPT212B_DRIVER_H

#include "i2c_driver.h"
#include "cpt212b_config.h"

/**
 * @brief - Type of CPT212B event
 */
typedef enum
{
  TOUCH_EVENT = 0x00,    //!< TOUCH_EVENT
  RELEASE_EVENT = 0x01,  //!< RELEASE_EVENT
  PROXIMITY_EVENT = 0x03,  //!< PROXIMITY_EVENT
  ERROR_EVENT = 0xFF     //!< ERROR_EVENT
} cpt212b_evt_type_t;

/**
 * @brief - Index of capacitive sensor
 */
typedef enum
{
  CS_INDEX_0 = 0x00,     //!< CS_INDEX_0
  CS_INDEX_1,       //!< CS_INDEX_1
  CS_INDEX_2,       //!< CS_INDEX_2
  CS_INDEX_3,       //!< CS_INDEX_3
  CS_INDEX_4,       //!< CS_INDEX_4
  CS_INDEX_5,       //!< CS_INDEX_5
  CS_INDEX_6,       //!< CS_INDEX_6
  CS_INDEX_7,       //!< CS_INDEX_7
  CS_INDEX_8,       //!< CS_INDEX_8
  CS_INDEX_9,       //!< CS_INDEX_9
  CS_INDEX_10,      //!< CS_INDEX_10
  CS_INDEX_11       //!< CS_INDEX_11
} cpt212b_cs_index_t;

/**
 * @brief CPT212B event struct
 */
typedef struct
{
  cpt212b_evt_type_t event;
  union
  {
    cpt212b_cs_index_t cs_index;
    uint32_t err_code;
  } data;
} cpt212b_evt_t;

/**
 * @brief CPT212B error code
 */
typedef enum
{
  CPT212B_OK = 0,                  //!< CPT212B_OK
  CPT212B_ERR = -1,                //!< CPT212B_ERR
  CPT212B_CONFIG_UPLOAD_ERR = -2,  //!< CPT212B_CONFIG_UPLOAD_ERR
  CPT212B_PACKET_LOST_ERR = -3,    //!< CPT212B_PACKET_LOST_ERR
  CPT212B_PACKET_NOT_VALID_ERR = -4    //!< CPT212B_PACKET_NOT_VALID_ERR
} cpt212b_err_t;

/**
 * @brief Event handler prototype
 *
 * @param evt - CPT212B event struct
 */
typedef void
(*cpt212b_evt_handler_t)(cpt212b_evt_t *evt);

/**
 * @brief Initialization of CPT212B
 *
 * @param i2c_instance - Instance of I2C interface
 * @param isr_port - Port of CPT212B int pin
 * @param isr_pin - Pin of CPT212B int pin
 * @param reset_port - Port of CPT212B reset pin
 * @param reset_pin - Pin of CPT212B reset pin
 * @param cpt212b_config - Pointer to configuration profile
 * @return
 *      CPT212B_OK - success
 */
cpt212b_err_t
cpt212b_init(I2CSPM_Init_TypeDef *i2c_instance,
             GPIO_Port_TypeDef int_port, unsigned int int_pin,
             GPIO_Port_TypeDef reset_port, unsigned int reset_pin,
             config_profile_t *cpt212b_config);

/**
 * @brief Deinitialization of CPT212B
 *
 * @return
 *      CPT212B_OK - success
 */
cpt212b_err_t
cpt212b_deinit(void);

/**
 * @brief Upload config profile to CPT212B
 *
 * @param config - pointer to config profile
 * @return
 *      CPT212B_OK - success
 */
cpt212b_err_t
cpt212b_config_upload(config_profile_t *config);

/**
 * @brief Registration of event handler
 *
 * @param handler - pointer to handler function
 */
void
cpt212b_reg_evt_handler(cpt212b_evt_handler_t handler);

#endif //!CPT212B_DRIVER_H
