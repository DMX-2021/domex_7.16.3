/**
 * @file cpt212b.c
 * @brief CPT212B driver source file
 *
 * @details This file contains definitions and basic functions for CPT212B
 *          Touch Controller
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#include "stdlib.h"
#include "string.h"
#include "cpt212b.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "gpiointerrupt.h"
#include "ustimer.h"
#include "ZW_typedefs.h"
//! I2C slave address
#define CPT212B_DEFAULT_ADR                 (0xC0)
#define CPT212B_PROGRAMMED_ADR              (0xE0)

//! Poling task configs
#define POLING_TASK_STACK_SIZE              (1024)
#define POLING_TASK_PRIORITY                (3)

#define CPT212B_PACKET_SIZE                 (3)
#define CPT212B_MAX_PACKET_COUNTER          (15)

//! CPT212B commands defines
#define CPT212B_MODE_SELECTION_CMD          {0b1000, 0x01}
#define CPT212B_CONFIG_UNLOCK_CMD           {0b1001, 0xA5, 0xF1}
#define CPT212B_CONFIG_ERASE_CMD            {0b1010}
#define CPT212B_CONFIG_WRITE_CMD            (0b1011)
#define CPT212B_CRC_WRITE_CMD               (0b1100)

//! Time for each upload state
#define RESET_TIME                          (5*1000)
#define BOOT_TIME                           (25*1000)
#define UNLOCK_DELAY                        (50*1000)
#define ERASE_DELAY                         (50*1000)
#define WRITE_DELAY                         (2*1000)
#define WRITE_CRC                           (50*1000)

/**
 * @brief CPT212B packet structure
 */
typedef struct
{
  uint8_t packet_counter :4;
  uint8_t event_type :4;
  uint8_t cs_index;
  uint8_t reserved;
} cpt212b_packet_t;

/** Local functions declaration */
static void
cpt212b_polling(void * pvParameters);

static bool
cpt212b_packet_isValid(cpt212b_packet_t packet);

static I2C_TransferReturn_TypeDef
cpt212b_packet_get(cpt212b_packet_t *packet);

static void
cpt212b_int_callback(uint8_t intNo);

/** RTOS task's variables*/
static TaskHandle_t xPolingTaskHandle = NULL;
static StaticTask_t xPolingTaskBuffer;
static StackType_t xPolingTaskStack[POLING_TASK_STACK_SIZE];

static SemaphoreHandle_t PollingSemaphore = NULL;
static StaticSemaphore_t xSemaphoreBuffer;

static I2CSPM_Init_TypeDef* _i2c_instance = NULL;

static GPIO_Port_TypeDef _int_port;
static unsigned int _int_pin;

static GPIO_Port_TypeDef _reset_port;
static unsigned int _reset_pin;

static cpt212b_evt_handler_t _handler = NULL;

cpt212b_err_t
cpt212b_init(I2CSPM_Init_TypeDef *i2c_instance,
             GPIO_Port_TypeDef int_port, unsigned int int_pin,
             GPIO_Port_TypeDef reset_port, unsigned int reset_pin,
             config_profile_t *cpt212b_config)
{

  if(_i2c_instance != NULL) {
    return CPT212B_ERR;
  }

  cpt212b_config->i2c_slave_address = CPT212B_PROGRAMMED_ADR;

  _i2c_instance = i2c_instance;
  _int_port = int_port;
  _int_pin = int_pin;
  _reset_port = reset_port;
  _reset_pin = reset_pin;

  USTIMER_Init();

  if(cpt212b_config_upload(cpt212b_config) != CPT212B_OK) {
    return CPT212B_CONFIG_UPLOAD_ERR;
  }

  //! Set INT pin as input
  GPIO_PinModeSet(_int_port, _int_pin, gpioModeInputPullFilter, 1);

  //! Enable interrupts for INT pin
  GPIOINT_Init();
  GPIO_ExtIntConfig(_int_port, _int_pin, _int_pin, 0, 1, true);
  GPIOINT_CallbackRegister(_int_pin, cpt212b_int_callback);

  PollingSemaphore = xSemaphoreCreateBinaryStatic(&xSemaphoreBuffer);
  if(PollingSemaphore == NULL) {
    return CPT212B_ERR;
  }

  //! Create RTOS polling task
  if(!xPolingTaskHandle)
    xPolingTaskHandle = xTaskCreateStatic(cpt212b_polling, "cpt212b_polling",
    POLING_TASK_STACK_SIZE,
                                          NULL, 1, xPolingTaskStack,
                                          &xPolingTaskBuffer);

  return CPT212B_OK;
}

cpt212b_err_t
cpt212b_deinit(void)
{
  if(_i2c_instance == NULL) {
    return CPT212B_ERR;
  }

  USTIMER_DeInit();

  vSemaphoreDelete(PollingSemaphore);

  GPIOINT_CallbackUnRegister(_int_pin);
  GPIO_IntClear(1 << _int_pin);
  GPIO_IntDisable(1 << _int_pin);

  _int_port = 0;
  _int_pin = 0;
  _reset_port = 0;
  _reset_pin = 0;
  _i2c_instance = NULL;
  _handler = NULL;

  return CPT212B_OK;
}

static void
cpt212b_int_callback(uint8_t intNo)
{
  UNUSED(intNo);
  BaseType_t xHigherPriorityTaskWoken = pdTRUE;
  xSemaphoreGiveFromISR(PollingSemaphore, &xHigherPriorityTaskWoken);
}

void
cpt212b_reg_evt_handler(cpt212b_evt_handler_t handler)
{
  _handler = handler;
}

static void
cpt212b_polling(void * pvParameters)
{
  UNUSED(pvParameters);
  I2C_TransferReturn_TypeDef i2c_ret;
  cpt212b_evt_t _evt = { 0 };
  cpt212b_packet_t packet = { 0 };

  for(;;) {

    xSemaphoreTake(PollingSemaphore, portMAX_DELAY);
    //! If we are here it means the semaphore is successfully taken

    while(GPIO_PinInGet(_int_port, _int_pin) == 0) {
      //! CPT212B has available packets
      i2c_ret = cpt212b_packet_get(&packet);
      if(i2c_ret != i2cTransferDone) {
        break;
      }

      if(cpt212b_packet_isValid(packet)) {
        _evt.data.cs_index = (cpt212b_cs_index_t) packet.cs_index;
        _evt.event = (cpt212b_evt_type_t) packet.event_type;
      }
      else {
        _evt.data.err_code = CPT212B_PACKET_NOT_VALID_ERR;
        _evt.event = ERROR_EVENT;
      }

      //! Call handler
      if(_handler)
        _handler(&_evt);
    }
  }
  vTaskDelete(NULL);
}

static I2C_TransferReturn_TypeDef
cpt212b_config_unlock(void)
{
  I2C_TransferReturn_TypeDef ret;
  uint8_t cpt212b_cmd[] = CPT212B_CONFIG_UNLOCK_CMD;

  GPIO_PinOutClear(_int_port, _int_pin);
  ret = i2c_write(_i2c_instance, CPT212B_DEFAULT_ADR, cpt212b_cmd,
                  sizeof(cpt212b_cmd));
  GPIO_PinOutSet(_int_port, _int_pin);

  return ret;
}

static I2C_TransferReturn_TypeDef
cpt212b_config_erase(void)
{
  I2C_TransferReturn_TypeDef ret;
  uint8_t cpt212b_cmd[] = CPT212B_CONFIG_ERASE_CMD;

  GPIO_PinOutClear(_int_port, _int_pin);
  ret = i2c_write(_i2c_instance, CPT212B_DEFAULT_ADR, cpt212b_cmd,
                  sizeof(cpt212b_cmd));
  GPIO_PinOutSet(_int_port, _int_pin);

  return ret;
}

static I2C_TransferReturn_TypeDef
cpt212b_config_write(config_profile_t *cpt212b_config)
{
  I2C_TransferReturn_TypeDef ret;

  uint8_t playload_size = 8;
  uint8_t *data_ptr = (uint8_t*) cpt212b_config;

  for(uint8_t i = 0; i < sizeof(config_profile_t); i += playload_size) {

    uint8_t write_data[9] = { 0 };
    write_data[0] = CPT212B_CONFIG_WRITE_CMD;
    memcpy(&write_data[1], &data_ptr[i], playload_size);

    GPIO_PinOutClear(_int_port, _int_pin);
    ret = i2c_write(_i2c_instance, CPT212B_DEFAULT_ADR, write_data,
                    sizeof(write_data));
    GPIO_PinOutSet(_int_port, _int_pin);
    I2C_ERR_CHECK(ret);

    USTIMER_DelayIntSafe(WRITE_DELAY);
  }

  return ret;
}

static I2C_TransferReturn_TypeDef
cpt212b_crc_write(uint8_t CRC_MSB, uint8_t CRC_LSB)
{
  I2C_TransferReturn_TypeDef ret;
  uint8_t cpt212b_crc[3];

  GPIO_PinOutClear(_int_port, _int_pin);

  cpt212b_crc[0] = CPT212B_CRC_WRITE_CMD;
  cpt212b_crc[1] = CRC_MSB;
  cpt212b_crc[2] = CRC_LSB;

  ret = i2c_write(_i2c_instance, CPT212B_DEFAULT_ADR, cpt212b_crc,
                  sizeof(cpt212b_crc));

  GPIO_PinOutSet(_int_port, _int_pin);

  return ret;
}

static I2C_TransferReturn_TypeDef
cpt212b_config_isValid(bool *result)
{
  I2C_TransferReturn_TypeDef ret;
  uint8_t cpt212b_cmd[0x01];

  GPIO_PinOutClear(_int_port, _int_pin);
  ret = i2c_read(_i2c_instance, CPT212B_DEFAULT_ADR, cpt212b_cmd,
                 sizeof(cpt212b_cmd));
  GPIO_PinOutSet(_int_port, _int_pin);

  if(cpt212b_cmd[0] == 0x80)
    *result = true;
  else
    *result = false;

  return ret;
}

/**
 * @brief Switch CPT212B to sending mode
 * @return I2C err code
 */
static I2C_TransferReturn_TypeDef
cpt212b_enter_sending_mode(void)
{
  I2C_TransferReturn_TypeDef ret;
  uint8_t cpt212b_cmd[] = CPT212B_MODE_SELECTION_CMD;

  GPIO_PinOutClear(_int_port, _int_pin);
  ret = i2c_write(_i2c_instance, CPT212B_DEFAULT_ADR, cpt212b_cmd,
                  sizeof(cpt212b_cmd));
  GPIO_PinOutSet(_int_port, _int_pin);

  return ret;
}

cpt212b_err_t
cpt212b_config_upload(config_profile_t *config)
{
  I2C_TransferReturn_TypeDef i2c_ret;

  /**
   * Note:
   *  CPT212B need time for upload config profile.
   *  Therefore, blocking delays are used here.
   */

  //! Reset CPT212B for boot in configuration mode
  GPIO_PinModeSet(_reset_port, _reset_pin, gpioModePushPull, 0);
  USTIMER_DelayIntSafe(RESET_TIME);
  GPIO_PinOutSet(_reset_port, _reset_pin);
  GPIO_PinModeSet(_int_port, _int_pin, gpioModePushPull, 0);

  //! Generate CRC
  uint16_t CRC = generateConfigProfileCRC(config);

  //! Wait until boot
  USTIMER_DelayIntSafe(BOOT_TIME);

  //! NOTE: Upload process takes time, so we can’t do it without delays
  i2c_ret = cpt212b_config_unlock();
  if(i2c_ret != i2cTransferDone)
    return CPT212B_CONFIG_UPLOAD_ERR;
  USTIMER_DelayIntSafe(UNLOCK_DELAY);

  i2c_ret = cpt212b_config_erase();
  if(i2c_ret != i2cTransferDone)
    return CPT212B_CONFIG_UPLOAD_ERR;
  USTIMER_DelayIntSafe(ERASE_DELAY);

  i2c_ret = cpt212b_config_write(config);
  if(i2c_ret != i2cTransferDone)
    return CPT212B_CONFIG_UPLOAD_ERR;

  i2c_ret = cpt212b_crc_write(CRC >> 8, CRC & 0xFF);
  if(i2c_ret != i2cTransferDone)
    return CPT212B_CONFIG_UPLOAD_ERR;
  USTIMER_DelayIntSafe(WRITE_CRC);

  bool config_isValid = false;
  i2c_ret = cpt212b_config_isValid(&config_isValid);
  if(i2c_ret != i2cTransferDone)
    return CPT212B_CONFIG_UPLOAD_ERR;

  if(!config_isValid)
    return CPT212B_CONFIG_UPLOAD_ERR;

  i2c_ret = cpt212b_enter_sending_mode();

  return CPT212B_OK;
}

/**
 * @brief Read and parse CPT212B packet
 * @return CPT212B packet
 */
static I2C_TransferReturn_TypeDef
cpt212b_packet_get(cpt212b_packet_t *packet)
{
  I2C_TransferReturn_TypeDef ret;
  uint8_t cpt212b_packet[0x03];

  ret = i2c_read(_i2c_instance, CPT212B_PROGRAMMED_ADR, cpt212b_packet,
                 sizeof(cpt212b_packet));
  I2C_ERR_CHECK(ret);

  packet->packet_counter = cpt212b_packet[0] >> 4;
  packet->event_type = cpt212b_packet[0];
  packet->cs_index = cpt212b_packet[1];
  packet->reserved = cpt212b_packet[2];

  return ret;
}

/**
 * @brief Validation of CPT212B packet
 * @param packet
 * @return
 *      true - valid packet
 *      false - invalid packet
 */
static bool
cpt212b_packet_isValid(cpt212b_packet_t packet)
{
  bool ret = true;

  if(packet.event_type < TOUCH_EVENT || packet.event_type > PROXIMITY_EVENT)
    ret = false;
  if(/*packet.cs_index < CS_INDEX_0 || */packet.cs_index > CS_INDEX_11)
    ret = false;
  if(packet.reserved)
    ret = false;

  return ret;
}
