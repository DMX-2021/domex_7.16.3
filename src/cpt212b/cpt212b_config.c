/**
 * @file cpt212b_config.c
 * @brief CPT212B driver source file
 *
 * @details This file contains configurations parameters and functions for
 *          CPT212B Touch Controller
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#define POLY 0x1021

#include "stdint.h"
#include "cpt212b_config.h"

uint16_t
generateConfigProfileCRC (config_profile_t* profile)
{
  uint16_t byte_index;
  uint8_t i, CRC_input;
  uint16_t CRC_acc = 0xFFFF;
  uint8_t * bytePtr = (uint8_t*) profile;

  for (byte_index = 0; byte_index < 510; byte_index++) {
    //! If not all bytes of config profile have been used to generate
    //! the CRC, read another byte
    if (byte_index < sizeof(config_profile_t)) {
      CRC_input = *bytePtr; //! Read byte from config profile
      bytePtr++; //! Increment pointer by one byte
    }
    //! else if all bytes have been used, use padding byte of 0xFF
    else {
      CRC_input = 0xFF;
    }
    //! Create the CRC "dividend" for polynomial arithmetic
    //! (binary arithmetic with no carries)
    CRC_acc = CRC_acc ^ (CRC_input << 8);
    //! "Divide" the poly into the dividend using CRC XOR subtraction
    //! CRC_acc holds the "remainder" of each divide
    //! Only complete this division for 8 bits since input is 1 byte
    for (i = 0; i < 8; i++) {
      //! Check if the MSB is set (if MSB is 1, then the POLY can "divide"
      //! into the "dividend")
      if ((CRC_acc & 0x8000) == 0x8000) {
        //! if so, shift the CRC value, and XOR "subtract" the poly
        CRC_acc = CRC_acc << 1;
        CRC_acc ^= POLY;
      }
      else {
        //! if not, just shift the CRC value
        CRC_acc = CRC_acc << 1;
      }
    }
  }
  return CRC_acc;
}
