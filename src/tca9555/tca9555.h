/**
 * @file tca9555.h
 * @brief TCA9555 driver header file
 *
 * @details This file contains definitions and basic functions for TCA9555
 *          GPIO expander
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#ifndef TCA9555_H
#define TCA9555_H

#include <stdint.h>
#include "sl_i2cspm.h"
/**
 * @brief TCA9555 error code
 */
typedef enum
{
  TCA9555_OK = 0, //!< TCA9555_OK
  TCA9555_ERR = -1 //!< TCA9555_ERR
} tca9555_err_t;

/**
 * @brief I2C slave address
 */
typedef enum
{
  A2L_A1L_A0L = 0x20 << 1, //!< A2L_A1L_A0L
  A2L_A1L_A0H,       //!< A2L_A1L_A0H
  A2L_A1H_A0L,       //!< A2L_A1H_A0L
  A2L_A1H_A0H,       //!< A2L_A1H_A0H
  A2H_A1L_A0L,       //!< A2H_A1L_A0L
  A2H_A1L_A0H,       //!< A2H_A1L_A0H
  A2H_A1H_A0L,       //!< A2H_A1H_A0L
  A2H_A1H_A0H,       //!< A2H_A1H_A0H
} tca9555_adr_t;

/**
 * @brief TCA9555 GPIO pins
 */
typedef enum
{
  GPIO_P00 = 0,       //!< GPIO_P00
  GPIO_P01,    //!< GPIO_P01
  GPIO_P02,    //!< GPIO_P02
  GPIO_P03,    //!< GPIO_P03
  GPIO_P04,    //!< GPIO_P04
  GPIO_P05,    //!< GPIO_P05
  GPIO_P06,    //!< GPIO_P06
  GPIO_P07,    //!< GPIO_P07
  GPIO_P10,    //!< GPIO_P10
  GPIO_P11,    //!< GPIO_P11
  GPIO_P12,    //!< GPIO_P12
  GPIO_P13,    //!< GPIO_P13
  GPIO_P14,    //!< GPIO_P14
  GPIO_P15,    //!< GPIO_P15
  GPIO_P16,    //!< GPIO_P16
  GPIO_P17,    //!< GPIO_P17
} tca9555_GPIO_pin_t;

/**
 * @brief TCA9555 pin mode
 */
typedef enum
{
  GPIO_OUTPUT = 0,    //!< GPIO_OUTPUT
  GPIO_INPUT = 1  //!< GPIO_INPUT
} tca9555_GPIO_mode_t;

/**
 * @brief TCA9555 pin state
 */
typedef enum
{
  GPIO_PIN_RESET = 0,  //!< GPIO_PIN_RESET
  GPIO_PIN_SET = 1,   //!< GPIO_PIN_SET
  GPIO_PIN_ERROR = -1
} tca9555_GPIO_state_t;

/**
 * @brief Initialization of TCA9555 driver
 *
 * @param i2c_instance - Pointer to instance of I2C interface
 * @param i2c_adr - I2C slave address
 * @param int_port - interrupt pin port. If interrupt not use MUST be 0xFF
 * @param int_pin - interrupt pin. If interrupt not use MUST be 0xFF
 * @return
 *  TCA9555_OK - Success
 */
tca9555_err_t
tca9555_init(I2CSPM_Init_TypeDef *i2c_instance, tca9555_adr_t i2c_adr,
             GPIO_Port_TypeDef int_port, unsigned int int_pin);

/**
 * @brief Initialization of GPIO pin
 *
 * @param pin - GPIO pin
 * @param mode - GPIO mode
 * @return
 *  TCA9555_OK - Success
 */
tca9555_err_t
tca9555_GPIO_init(tca9555_GPIO_pin_t pin, tca9555_GPIO_mode_t mode);

/**
 * @brief Read GPIO state
 *
 * @param pin - GPIO pin
 * @return
 GPIO_PIN_RESET - GPIO low level
 GPIO_PIN_SET - GPIO high level
 */
tca9555_GPIO_state_t
tca9555_GPIO_read(tca9555_GPIO_pin_t pin);

/**
 * @brief Write GPIO state
 *
 * @param pin - GPIO pin
 * @param state - GPIO state
 * @return
 *  TCA9555_OK - Success
 */
tca9555_err_t
tca9555_GPIO_write(tca9555_GPIO_pin_t pin, tca9555_GPIO_state_t state);

/**
 * @brief Toggle GPIO pin
 *
 * @param pin - GPIO pin
 * @return
 *  TCA9555_OK - Success
 */
tca9555_err_t
tca9555_GPIO_toggle(tca9555_GPIO_pin_t pin);

#endif //!TCA9555_H
