/**
 * @file tca9555.c
 * @brief TCA9555 driver source file
 *
 * @details This file contains definitions and basic functions for TCA9555
 *          GPIO expander
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */
#include "stddef.h"
#include "tca9555.h"
#include "gpiointerrupt.h"
#include "i2c_driver.h"

//! Flag, that int pin is note used
#define INT_PIN_NOT_USED                    (0xFF)

//! Length of register
#define TCA9555_REG_LEN                     (0x02)

#define BIT_SET(reg,nbit)                   do{(reg) |=  (1<<(nbit));}while(0)
#define BIT_CLR(reg,nbit)                   do{(reg) &= ~(1<<(nbit));}while(0)
#define BIT_TGL(reg,nbit)                   do{(reg) ^=  (1<<(nbit));}while(0)
#define BIT_CHECK(reg,nbit)                 ((reg) &   (1<<(nbit)))

/**
 * @brief TCA9555 register map
 */
typedef enum
{
  INPUT_REG = 0x00,       //!< INPUT_REG
  OUTPUT_REG = 0x02,      //!< OUTPUT_REG
  POLARITY_INV_REG = 0x4, //!< POLARITY_INV_REG
  CONFIGURATION_REG = 0x6, //!< CONFIGURATION_REG
} tca9555_reg_t;

//! Local functions
static I2C_TransferReturn_TypeDef
tcp9555_write_reg(tca9555_reg_t reg, uint16_t *data);
static I2C_TransferReturn_TypeDef
tcp9555_read_reg(tca9555_reg_t reg, uint16_t *data);

//! Local variables
static I2CSPM_Init_TypeDef *_i2c_instance = NULL;
static tca9555_adr_t _i2c_adr = 0;
static GPIO_Port_TypeDef _int_port = 0;
static unsigned int _int_pin = 0;


extern void GPIOINT_CallbackRegister(uint8_t intNo, GPIOINT_IrqCallbackPtr_t callbackPtr);

/**
 * @brief Callback for PCT2075 interrupt
 *
 * @param intNo - Interrupt number
 */
static void
tca9555_int_callback(void)
{

}

tca9555_err_t
tca9555_init(I2CSPM_Init_TypeDef *i2c_instance, tca9555_adr_t i2c_adr,
             GPIO_Port_TypeDef int_port, unsigned int int_pin)
{
  if(_i2c_instance != NULL) {
    return TCA9555_ERR;
  }

  I2C_TransferReturn_TypeDef i2c_ret;

  _i2c_instance = i2c_instance;
  _i2c_adr = i2c_adr;
  _int_port = int_port;
  _int_pin = int_pin;// YB was _int_pin

  if(_int_port != INT_PIN_NOT_USED && _int_pin != INT_PIN_NOT_USED) {
    //! Set INT pin as input
    GPIO_PinModeSet(_int_port, _int_pin, gpioModeInputPullFilter, 1);

    //! Register interrupt callback
    GPIOINT_Init();
    GPIO_ExtIntConfig(_int_port, _int_pin, _int_pin, false, true, true);
    GPIOINT_CallbackRegister(_int_pin, tca9555_int_callback);
  }

  //! Read stored configurations
  uint16_t configuration = 0;
  i2c_ret = tcp9555_read_reg(CONFIGURATION_REG, &configuration);
  if(i2c_ret != i2cTransferDone)
    return TCA9555_ERR;

  (void) configuration;

  return TCA9555_OK;
}

tca9555_err_t
tca9555_GPIO_init(tca9555_GPIO_pin_t pin, tca9555_GPIO_mode_t mode)
{
  /*
  if(pin < GPIO_P00 || pin > GPIO_P17) {
    LOG_ERROR("ERROR! There is no such pin!");
    return TCA9555_ERR;
  }
*/
  if(mode != GPIO_OUTPUT && mode != GPIO_INPUT) {
    return TCA9555_ERR;
  }

  I2C_TransferReturn_TypeDef i2c_ret;
  uint16_t configuration = 0;

  i2c_ret = tcp9555_read_reg(CONFIGURATION_REG, &configuration);
  if(i2c_ret != i2cTransferDone)
    return TCA9555_ERR;

  if(mode == GPIO_OUTPUT)
    BIT_CLR(configuration, pin);
  else
    BIT_SET(configuration, pin);

  i2c_ret = tcp9555_write_reg(CONFIGURATION_REG, &configuration);
  if(i2c_ret != i2cTransferDone)
    return TCA9555_ERR;

  return TCA9555_OK;
}

tca9555_GPIO_state_t
tca9555_GPIO_read(tca9555_GPIO_pin_t pin)
{
  /*
  if(pin < GPIO_P00 || pin > GPIO_P17) {
    LOG_ERROR("ERROR! There is no such pin!");
    return TCA9555_ERR;
  }
*/
  I2C_TransferReturn_TypeDef i2c_ret;
  uint16_t input_reg = 0;

  i2c_ret = tcp9555_read_reg(INPUT_REG, &input_reg);
  if(i2c_ret != i2cTransferDone)
    return GPIO_PIN_ERROR;

  if((BIT_CHECK(input_reg, pin)))
    return GPIO_PIN_SET;
  else
    return GPIO_PIN_RESET;
}

tca9555_err_t
tca9555_GPIO_write(tca9555_GPIO_pin_t pin, tca9555_GPIO_state_t state)
{
  /*
  if(pin < GPIO_P00 || pin > GPIO_P17) {
    LOG_ERROR("ERROR! There is no such pin!");
    return TCA9555_ERR;
  }
*/
  if(state != GPIO_PIN_RESET && state != GPIO_PIN_SET) {
    return TCA9555_ERR;
  }

  I2C_TransferReturn_TypeDef i2c_ret;
  uint16_t output_reg = 0;

  i2c_ret = tcp9555_read_reg(OUTPUT_REG, &output_reg);
  if(i2c_ret != i2cTransferDone)
    return TCA9555_ERR;

  if(state == GPIO_PIN_SET)
    BIT_SET(output_reg, pin);
  else
    BIT_CLR(output_reg, pin);

  i2c_ret = tcp9555_write_reg(OUTPUT_REG, &output_reg);
  if(i2c_ret != i2cTransferDone)
    return TCA9555_ERR;

  return TCA9555_OK;
}

tca9555_err_t
tca9555_GPIO_toggle(tca9555_GPIO_pin_t pin)
{
  /*
  if(pin < GPIO_P00 || pin > GPIO_P17) {
    LOG_ERROR("ERROR! There is no such pin!");
    return TCA9555_ERR;
  }
*/
  I2C_TransferReturn_TypeDef i2c_ret;
  uint16_t output_reg = 0;

  i2c_ret = tcp9555_read_reg(OUTPUT_REG, &output_reg);
  if(i2c_ret != i2cTransferDone)
    return TCA9555_ERR;

  if(BIT_CHECK(output_reg, pin))
    BIT_CLR(output_reg, pin);
  else
    BIT_SET(output_reg, pin);

  i2c_ret = tcp9555_write_reg(OUTPUT_REG, &output_reg);
  if(i2c_ret != i2cTransferDone)
    return TCA9555_ERR;

  return TCA9555_OK;
}

/**
 * @brief Write to TCP9555 register
 *
 * @param reg - TCP9555 register
 * @param data - Data for writing
 * @return i2c error code
 */
static I2C_TransferReturn_TypeDef
tcp9555_write_reg(tca9555_reg_t reg, uint16_t *data)
{
  I2C_TransferReturn_TypeDef i2c_ret;

  i2c_ret = i2c_write_reg(_i2c_instance, _i2c_adr, reg, (uint8_t*)data,
                          TCA9555_REG_LEN);
  return i2c_ret;
}

/**
 * @brief Read from TCP9555 register
 *
 * @param reg - TCP9555 register
 * @param data - Pointer to return data
 * @return i2c error code
 */
static I2C_TransferReturn_TypeDef
tcp9555_read_reg(tca9555_reg_t reg, uint16_t *data)
{
  I2C_TransferReturn_TypeDef i2c_ret;

  i2c_ret = i2c_read_reg(_i2c_instance, _i2c_adr, reg, (uint8_t*)data,
                         TCA9555_REG_LEN);
  return i2c_ret;
}
