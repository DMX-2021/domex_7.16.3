/**
 * @file MMI.c
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#ifndef MMI_H
#define MMI_H

void
Refresh_MMI_Idle(void);

void
Refresh_Boiler_MMI_Idle(void);

void
Refresh_MMI_LearnMode(void);

void
Refresh_Boiler_MMI_LearnMode(void);

void
Refresh_MMI_Reset(void);

void
Refresh_Boiler_MMI_Reset(void);

void
Refresh_MMI_CentralSceneNotification(uint8_t epNumb);

#endif//!MMI_H
