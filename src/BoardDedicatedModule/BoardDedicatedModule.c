/**
 * @file BoardDedicatedModule
 * @brief Module helps control board periphery
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */

#include "BoardDedicatedModule.h"
#include "tca9555.h"
#include "stm32_dimmer.h"
#include "ws2812b.h"
#include "em_wdog.h"
#include "config_app.h"
#include "SwTimer.h"
#include "AppTimer.h"

#define ERROR_CHECK(comp, retCode)              do{ \
                                                 if(!(comp)) \
                                                   return retCode; \
                                                }while(0)

#define BIT_SET(val,nbit)                   do{(val) |=  (1<<(nbit));}while(0)

#define BOARD_ADDRESS_BIT0_PIN                  GPIO_P15
#define BOARD_ADDRESS_BIT1_PIN                  GPIO_P16
#define BOARD_ADDRESS_BIT2_PIN                  GPIO_P17

#define LED_TIMER_PERIOD                  (100)
extern char LearnModeActive;
typedef struct  {
  uint8_t minDimmingValue;
  uint8_t maxDimmingValue;
} Board_DimmerConfig_t;

typedef struct {
  uint8_t red;
  uint8_t green;
  uint8_t blue;
  uint8_t isLedOn;
  uint16_t reloadBlinkTime;
  uint16_t blinkTime;
} Board_LedConfig_t;

static Board_LedConfig_t LedConfig_List[WS2812B_LED_COUNT] = {0};
extern char FadeOutFactor;
extern void StartLedFade(void);
//! Button timer's variables
static SSwTimer ledTimerHandle = {0};
static bool isLedTimerCreated = false;
extern uint8_t blinker_cycles_Count;
extern uint16_t on_time_ms_Count;
/**
 * @brief Get board address
 *
 * @return Board address
 */
uint8_t
Board_AddressGet(void)
{
  uint8_t address = 0;
  tca9555_GPIO_init(BOARD_ADDRESS_BIT0_PIN, GPIO_INPUT);
  tca9555_GPIO_init(BOARD_ADDRESS_BIT1_PIN, GPIO_INPUT);
  tca9555_GPIO_init(BOARD_ADDRESS_BIT2_PIN, GPIO_INPUT);

  if(tca9555_GPIO_read(BOARD_ADDRESS_BIT0_PIN))
    BIT_SET(address, 0);
  if(tca9555_GPIO_read(BOARD_ADDRESS_BIT1_PIN))
    BIT_SET(address, 1);
  if(tca9555_GPIO_read(BOARD_ADDRESS_BIT2_PIN))
    BIT_SET(address, 2);

  return address;
}

/**
 * @brief Initialization of relays channels
 *
 * @return board error code
 */
BoardErr_t
Board_RelayInit(void)
{
  int8_t ret = 0;
  uint8_t boardAdress = 0;

  boardAdress = Board_AddressGet();

  if(boardAdress == BOARD_ADDRESS_LIGHT) {
    ret = tca9555_GPIO_init(LIGHT_RELAY_OUT_1, GPIO_OUTPUT);
    ERROR_CHECK(ret == TCA9555_OK, ret);
    ret = tca9555_GPIO_write(LIGHT_RELAY_OUT_1, (tca9555_GPIO_state_t)RELAY_STATE_INACTIVE);
    ERROR_CHECK(ret == TCA9555_OK, ret);

    ret = tca9555_GPIO_init(LIGHT_RELAY_OUT_2, GPIO_OUTPUT);
    ERROR_CHECK(ret == TCA9555_OK, ret);
    ret = tca9555_GPIO_write(LIGHT_RELAY_OUT_2, (tca9555_GPIO_state_t)RELAY_STATE_INACTIVE);
    ERROR_CHECK(ret == TCA9555_OK, ret);

    ret = tca9555_GPIO_init(LIGHT_RELAY_OUT_3, GPIO_OUTPUT);
    ERROR_CHECK(ret == TCA9555_OK, ret);
    ret = tca9555_GPIO_write(LIGHT_RELAY_OUT_3, (tca9555_GPIO_state_t)RELAY_STATE_INACTIVE);
    ERROR_CHECK(ret == TCA9555_OK, ret);

    ret = tca9555_GPIO_init(LIGHT_RELAY_OUT_4, GPIO_OUTPUT);
    ERROR_CHECK(ret == TCA9555_OK, ret);
    ret = tca9555_GPIO_write(LIGHT_RELAY_OUT_4,(tca9555_GPIO_state_t) RELAY_STATE_INACTIVE);
    ERROR_CHECK(ret == TCA9555_OK, ret);

    ret = tca9555_GPIO_init(LIGHT_RELAY_OUT_5, GPIO_OUTPUT);
    ERROR_CHECK(ret == TCA9555_OK, ret);
    ret = tca9555_GPIO_write(LIGHT_RELAY_OUT_5, (tca9555_GPIO_state_t)RELAY_STATE_INACTIVE);
    ERROR_CHECK(ret == TCA9555_OK, ret);

    ret = tca9555_GPIO_init(LIGHT_RELAY_OUT_6, GPIO_OUTPUT);
    ERROR_CHECK(ret == TCA9555_OK, ret);
    ret = tca9555_GPIO_write(LIGHT_RELAY_OUT_6,(tca9555_GPIO_state_t) RELAY_STATE_INACTIVE);
    ERROR_CHECK(ret == TCA9555_OK, ret);
  }
  else if(boardAdress == BOARD_ADDRESS_WATER_HEATER) {
    ret = tca9555_GPIO_init(WATER_HEATER_RELAY_OUT_1, GPIO_OUTPUT);
    ERROR_CHECK(ret == TCA9555_OK, ret);
    ret = tca9555_GPIO_write(WATER_HEATER_RELAY_OUT_1, (tca9555_GPIO_state_t)RELAY_STATE_INACTIVE);
    ERROR_CHECK(ret == TCA9555_OK, ret);

    ret = tca9555_GPIO_init(WATER_HEATER_RELAY_OUT_2, GPIO_OUTPUT);
    ERROR_CHECK(ret == TCA9555_OK, ret);
    ret = tca9555_GPIO_write(WATER_HEATER_RELAY_OUT_2, (tca9555_GPIO_state_t)RELAY_STATE_INACTIVE);
    ERROR_CHECK(ret == TCA9555_OK, ret);

    ret = tca9555_GPIO_init(WATER_HEATER_RELAY_OUT_3, GPIO_OUTPUT);
    ERROR_CHECK(ret == TCA9555_OK, ret);
    ret = tca9555_GPIO_write(WATER_HEATER_RELAY_OUT_3, (tca9555_GPIO_state_t)RELAY_STATE_INACTIVE);
    ERROR_CHECK(ret == TCA9555_OK, ret);
  }
  else
    return BOARD_ERR;

  return BOARD_OK;
}

/**
 * @brief Set relay state
 *
 * @param ch - relay channel
 * @param state - relay state
 * @return board error code
 */
BoardErr_t
Board_RelaySet(OutputCh_t ch, RelayState_t state)
{
  tca9555_err_t ret = TCA9555_ERR;
  uint8_t boardAdress = 0;

  boardAdress = Board_AddressGet();
  StartLedFade();

  if(boardAdress == BOARD_ADDRESS_LIGHT) {
    switch(ch) {
      case OUTPUT_CH_1:
        ret = tca9555_GPIO_write(LIGHT_RELAY_OUT_1, (tca9555_GPIO_state_t)state);
        break;
      case OUTPUT_CH_2:
        ret = tca9555_GPIO_write(LIGHT_RELAY_OUT_2,(tca9555_GPIO_state_t) state);
        break;
      case OUTPUT_CH_3:
        ret = tca9555_GPIO_write(LIGHT_RELAY_OUT_3, (tca9555_GPIO_state_t)state);
        break;
      case OUTPUT_CH_4:
        ret = tca9555_GPIO_write(LIGHT_RELAY_OUT_4, (tca9555_GPIO_state_t)state);
        break;
      case OUTPUT_CH_5:
        ret = tca9555_GPIO_write(LIGHT_RELAY_OUT_5, (tca9555_GPIO_state_t)state);
        break;
      case OUTPUT_CH_6:
        ret = tca9555_GPIO_write(LIGHT_RELAY_OUT_6, (tca9555_GPIO_state_t)state);
        break;
      default:
        ret = TCA9555_ERR;
        break;
    }
  }
  else if (boardAdress == BOARD_ADDRESS_WATER_HEATER) {
    switch(ch) {
      case OUTPUT_CH_1:
        ret = tca9555_GPIO_write(WATER_HEATER_RELAY_OUT_1, (tca9555_GPIO_state_t)state);
        break;
      case OUTPUT_CH_2:
        ret = tca9555_GPIO_write(WATER_HEATER_RELAY_OUT_2, (tca9555_GPIO_state_t)state);
        break;
      case OUTPUT_CH_3:
        ret = tca9555_GPIO_write(WATER_HEATER_RELAY_OUT_3, (tca9555_GPIO_state_t)state);
        break;
      default:
        ret = TCA9555_ERR;
        break;
    }
  }
  else
    ret = TCA9555_ERR;

  if(ret != TCA9555_OK)
    return BOARD_ERR;
  else
    return BOARD_OK;
}

/**
 * @brief Reset dimmer controller
 *
 * @return board error code
 */
BoardErr_t
Board_DimmerReset(void)
{
  tca9555_err_t ret = TCA9555_ERR;
  ret = tca9555_GPIO_init(DIMMER_RESET, GPIO_OUTPUT);
  if (ret != TCA9555_OK) return BOARD_ERR;

  ret = tca9555_GPIO_write(DIMMER_RESET, GPIO_PIN_SET);
  if (ret != TCA9555_OK) return BOARD_ERR;
//  ret = tca9555_GPIO_write(DIMMER_RESET, GPIO_PIN_RESET);
//  if (ret != TCA9555_OK) return BOARD_ERR;
  //! SDK has not a delay function, so use loop delay
  return BOARD_OK;
}
BoardErr_t
Board_DimmerReset_Part2(void)
{
  tca9555_err_t ret = TCA9555_ERR;
  ret = tca9555_GPIO_write(DIMMER_RESET, GPIO_PIN_RESET);
  if (ret != TCA9555_OK) return BOARD_ERR;
  //! SDK has not a delay function, so use loop delay
  return BOARD_OK;
}

/**
 * @brief Set dimmer level
 *
 * @param ch - dimmer channel
 * @param level - dimmer level
 * @return board error code
 */
BoardErr_t
Board_DimmerSet(OutputCh_t ch, uint8_t level, uint8_t duration)
{
  stm32_dimmer_err_t ret = STM32F0_DIMMER_ERR;

  switch(ch) {
    case OUTPUT_CH_1:
      ret = stm32_dimmer_ch_set(DIMMER_OUT_1, level, duration);
      break;
    case OUTPUT_CH_2:
      ret = stm32_dimmer_ch_set(DIMMER_OUT_2, level, duration);
      break;
    case OUTPUT_CH_3:
      ret = stm32_dimmer_ch_set(DIMMER_OUT_3, level, duration);
      break;
    case OUTPUT_CH_4:
      ret = stm32_dimmer_ch_set(DIMMER_OUT_4, level, duration);
      break;
    case OUTPUT_CH_5:
      ret = stm32_dimmer_ch_set(DIMMER_OUT_5, level, duration);
      break;
    case OUTPUT_CH_6:
      ret = stm32_dimmer_ch_set(DIMMER_OUT_6, level, duration);
      break;
    default:
      break;
  }

  if(ret != STM32F0_DIMMER_OK)
    return BOARD_ERR;
  else
    return BOARD_OK;
}

/**
 * @brief Get dimmer level
 *
 * @param ch - dimmer channel
 * @param level - dimmer level
 * @return board error code
 */
BoardErr_t
Board_DimmerGet(OutputCh_t ch, uint8_t *level, uint8_t *duration)
{
  stm32_dimmer_err_t ret;

  if (!level || !duration)
    goto __err;

  switch(ch) {
    case OUTPUT_CH_1:
      ret = stm32_dimmer_ch_get(DIMMER_OUT_1, level, duration);
      break;
    case OUTPUT_CH_2:
      ret = stm32_dimmer_ch_get(DIMMER_OUT_2, level, duration);
      break;
    case OUTPUT_CH_3:
      ret = stm32_dimmer_ch_get(DIMMER_OUT_3, level, duration);
      break;
    case OUTPUT_CH_4:
      ret = stm32_dimmer_ch_get(DIMMER_OUT_4, level, duration);
      break;
    case OUTPUT_CH_5:
      ret = stm32_dimmer_ch_get(DIMMER_OUT_5, level, duration);
      break;
    case OUTPUT_CH_6:
      ret = stm32_dimmer_ch_get(DIMMER_OUT_6, level, duration);
      break;
    default:
      goto __err;
      break;
  }

  if (ret != STM32F0_DIMMER_OK)
    goto __err;

  return BOARD_OK;
__err:
  return BOARD_ERR;
}

/**
 * @brief Blinds open
 *
 * @param ch1 - channel 1
 * @param ch2 - channel 2
 * @return board error code
 */
BoardErr_t
Board_BlindsOpen(OutputCh_t ch1, OutputCh_t ch2)
{
  BoardErr_t ret = BOARD_OK;

  ret = Board_BlindsStop(ch1, ch2);
  ERROR_CHECK(ret == BOARD_OK, BOARD_ERR);

  ret = Board_RelaySet(ch1, RELAY_STATE_ACTIVE);
  ERROR_CHECK(ret == BOARD_OK, BOARD_ERR);

  return ret;
}

/**
 * @brief Blinds close
 *
 * @param ch1 - channel 1
 * @param ch2 - channel 2
 * @return board error code
 */
BoardErr_t
Board_BlindsClose(OutputCh_t ch1, OutputCh_t ch2)
{
  BoardErr_t ret = BOARD_OK;

  ret = Board_BlindsStop(ch1, ch2);
  ERROR_CHECK(ret == BOARD_OK, BOARD_ERR);

  ret = Board_RelaySet(ch2, RELAY_STATE_ACTIVE);
  ERROR_CHECK(ret == BOARD_OK, BOARD_ERR);

  return ret;
}

/**
 * @brief Stop blinds motion
 *
 * @param ch1 - channel 1
 * @param ch2 - channel 2
 * @return board error code
 */
BoardErr_t
Board_BlindsStop(OutputCh_t ch1, OutputCh_t ch2)
{
  BoardErr_t ret = BOARD_OK;

  ret = Board_RelaySet(ch1, RELAY_STATE_INACTIVE);
  ERROR_CHECK(ret == BOARD_OK, BOARD_ERR);

  ret = Board_RelaySet(ch2, RELAY_STATE_INACTIVE);
  ERROR_CHECK(ret == BOARD_OK, BOARD_ERR);

  return ret;
}

/**
 * @brief Activate boiler water heater
 *
 * @param ch1 - channel 1
 * @param ch2 - channel 2
 * @return board error code
 */
BoardErr_t
Board_BoilerActivate(OutputCh_t ch1, OutputCh_t ch2)
{
  BoardErr_t ret = BOARD_OK;

  ret = Board_RelaySet(ch1, RELAY_STATE_ACTIVE);
  ERROR_CHECK(ret == BOARD_OK, BOARD_ERR);

  ret = Board_RelaySet(ch2, RELAY_STATE_ACTIVE);
  ERROR_CHECK(ret == BOARD_OK, BOARD_ERR);

  return ret;
}
BoardErr_t
Board_FanActivate(OutputCh_t ch1, OutputCh_t ch2,RelayState_t Val_ch1,RelayState_t Val_ch2)
{
  BoardErr_t ret = BOARD_OK;

  ret = Board_RelaySet(ch1, Val_ch1);
  ERROR_CHECK(ret == BOARD_OK, BOARD_ERR);

  ret = Board_RelaySet(ch2, Val_ch2);
  ERROR_CHECK(ret == BOARD_OK, BOARD_ERR);

  return ret;
}

/**
 * @brief Deactivateboiler water heater
 *
 * @param ch1 - channel 1
 * @param ch2 - channel 2
 * @return board error code
 */
BoardErr_t
Board_BoilerDeactivate(OutputCh_t ch1, OutputCh_t ch2)
{
  BoardErr_t ret = BOARD_OK;

  ret = Board_RelaySet(ch1, RELAY_STATE_INACTIVE);
  ERROR_CHECK(ret == BOARD_OK, BOARD_ERR);

  ret = Board_RelaySet(ch2, RELAY_STATE_INACTIVE);
  ERROR_CHECK(ret == BOARD_OK, BOARD_ERR);

  return ret;
}

/**
 * @brief LED timer callback
 * @details Used to LED blink effect
 *
 * @param pTimer - pointer to the timer object
 */
void
vLedTimerCallback(SSwTimer* pTimer)
{
  UNUSED(pTimer);
  for(int led = 0; led < WS2812B_LED_COUNT; ++led) {
    //! Is LED blink ?
    if(LedConfig_List[led].reloadBlinkTime == 0)
      continue;

    if(LedConfig_List[led].blinkTime == 0) {
      LedConfig_List[led].blinkTime = LedConfig_List[led].reloadBlinkTime;
      LedConfig_List[led].isLedOn = !LedConfig_List[led].isLedOn;
    }

    if(LedConfig_List[led].blinkTime <= LED_TIMER_PERIOD){
      LedConfig_List[led].blinkTime = 0;
    }
    else {
      LedConfig_List[led].blinkTime -= LED_TIMER_PERIOD;
    }

    if(LedConfig_List[led].isLedOn)
      ws2812b_set_color(led, LedConfig_List[led].red, LedConfig_List[led].green,
                        LedConfig_List[led].blue);
    else {
      ws2812b_set_color(led, 0, 0, 0);
    }
  }
  ws2812b_update();
}

/**
 * @brief Get RGB color from BacklightColor_t enumeration
 *
 * @param color - BacklightColor_t color enumeration
 * @param red - pointer to the return red value
 * @param green - pointer to the return green value
 * @param blue - pointer to the return blue value
 * @return board error code
 */
static BoardErr_t
Board_LedGetRGB(BacklightColor_t color, uint8_t *red, uint8_t *green,
                uint8_t *blue)
{
  if(!NON_NULL(red) || !NON_NULL(green) || !NON_NULL(blue))
    return BOARD_ERR;

  switch (color) {
    case BACKLIGHT_COLOR_RED:
      *red = 255; *green = 0; *blue = 0;
      break;
    case BACKLIGHT_COLOR_GREEN:
      *red = 0; *green = 255; *blue = 0;
      break;
    case BACKLIGHT_COLOR_BLUE:
      *red = 0; *green = 0; *blue = 255;
      break;
    case BACKLIGHT_COLOR_YELLOW:
      *red = 150; *green = 150; *blue = 0;
      //*red =  255; *green = 128; *blue = 0;
      break;
    case BACKLIGHT_COLOR_MAGENTA:
      *red = 255; *green = 0; *blue = 255;
      break;
    case BACKLIGHT_COLOR_CYAN:
      *red = 0; *green = 255; *blue = 255;
      break;
    case BACKLIGHT_COLOR_WHITE:
      *red = 255; *green = 255; *blue = 255;
      break;
    case BACKLIGHT_COLOR_BLACK:
    default:
      *red = 0; *green = 0; *blue = 0;
      break;
  }

  return BOARD_OK;
}

/**
 * @brief Set color and blink period to the button LED
 *
 * @param btn - number of button
 * @param color - color
 * @param blinkPeriod - period of blink
 * @return board error code
 */
BoardErr_t
Board_LedSet(BtnNumber_t btn, BacklightColor_t color, uint16_t blinkPeriod)
{
  if(btn >= BTN_NUMBER_MAX || btn == BTN_NUMBER_NONE)
    return BOARD_ERR;

  uint8_t ledNum = 0;
  uint8_t red = 0;
  uint8_t green = 0;
  uint8_t blue = 0;

  if(Board_LedGetRGB(color, &red, &green, &blue) != BOARD_OK)
    return BOARD_ERR;

  switch (btn) {
    case BTN_NUMBER_1:
      ledNum = BUTTON_1_LED_NUM;
      break;
    case BTN_NUMBER_2:
      ledNum = BUTTON_2_LED_NUM;
      break;
    case BTN_NUMBER_3:
      ledNum = BUTTON_3_LED_NUM;
      break;
    case BTN_NUMBER_4:
      ledNum = BUTTON_4_LED_NUM;
      break;
    case BTN_NUMBER_5:
      ledNum = BUTTON_5_LED_NUM;
      break;
    case BTN_NUMBER_6:
      ledNum = BUTTON_6_LED_NUM;
      break;
    case BTN_NUMBER_7:
      ledNum = BUTTON_7_LED_NUM;
      break;
    case BTN_NUMBER_8:
      ledNum = BUTTON_8_LED_NUM;
      break;
    case BTN_NUMBER_9:
      ledNum = BUTTON_9_LED_NUM;
      break;
    default:
      break;
  }
  LedConfig_List[ledNum].red = red>>FadeOutFactor;
  LedConfig_List[ledNum].green = green>>FadeOutFactor;
  LedConfig_List[ledNum].blue = blue>>FadeOutFactor;
  LedConfig_List[ledNum].reloadBlinkTime = blinkPeriod;
  ws2812b_set_color(ledNum, red>>FadeOutFactor, green>>FadeOutFactor, blue>>FadeOutFactor);
  return BOARD_OK;
}

/**
 * @brief Update changes caused by Board_LedSetColor() and
 *        Board_LedSetBlinkPeriod()
 */
void
Board_LedUpdate(void)
{
  if (LearnModeActive)
    {
      ws2812b_Green_All();
    }
  else
    {
      if (blinker_cycles_Count>0)
        {
          if (on_time_ms_Count>0)
            ws2812b_Cyan_All();
          else
            ws2812b_Off_All();
        }
      else
        {
          ws2812b_update();
        }
    }

  if(!isLedTimerCreated) {
    isLedTimerCreated = AppTimerRegister(&ledTimerHandle, true,
                                         vLedTimerCallback);
    TimerStart(&ledTimerHandle, LED_TIMER_PERIOD);
  }
  else {
    for(int ledNum = 0; ledNum < WS2812B_LED_COUNT; ledNum++) {
      LedConfig_List[ledNum].blinkTime = 0;
      LedConfig_List[ledNum].isLedOn = false;
    }

    TimerRestart(&ledTimerHandle);
  }
}

/**
 * @brief Board default handler
 */
void
Board_Default_Handler(void)
{
  /**
   * Critical error!
   */
#if (HARDWARE_SETUP == DEVELOPER_KIT_SETUP)
  while(1) {
    GPIO_PinOutSet(gpioPortF, 4);
    GPIO_PinOutClear(gpioPortF, 5);
    for(int i = 0; i < 1000000; i++)
      ;
    GPIO_PinOutSet(gpioPortF, 5);
    GPIO_PinOutClear(gpioPortF, 4);
    for(int i = 0; i < 1000000; i++)
      ;
  }
#elif (HARDWARE_SETUP == DEVICE_REV_1_SETUP)
  //! Dead loop
  while(1) {
    for(int led = 0; led < 9; led++) {
      ws2812b_set_color(led, 255, 0, 0);
      ws2812b_update();
    }
    for(int i = 0; i < 10000000; i++);
    for(int led = 0; led < 9; led++) {
      ws2812b_set_color(led, 0, 0, 0);
      ws2812b_update();
    }
    for(int i = 0; i < 10000000; i++);
  }
#endif
}
