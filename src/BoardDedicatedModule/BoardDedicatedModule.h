/**
 * @file BoardDedicatedModule
 * @brief Module helps control board periphery
 *
 * @author Alexander Grin
 * @copyright (C) 2020 Alnicko Development OU. All rights reserved.
 */
#ifndef BOARD_DEDICATED_MODULE_H
#define BOARD_DEDICATED_MODULE_H

#include "stdint.h"
#include "ConfigurationModule.h"

typedef enum
{
  BOARD_OK = 0,
  BOARD_ERR = -1
} BoardErr_t;

typedef enum
{
  RELAY_STATE_ACTIVE = 1,
  RELAY_STATE_INACTIVE = 0
} RelayState_t;

/**
 * @brief Get board address
 *
 * @return Board address
 */
uint8_t
Board_AddressGet(void);

/**
 * @brief Initialization of relays channels
 *
 * @return board error code
 */
BoardErr_t
Board_RelayInit(void);

/**
 * @brief Set relay state
 *
 * @param ch - relay channel
 * @param state - relay state
 * @return board error code
 */
BoardErr_t
Board_RelaySet(OutputCh_t ch, RelayState_t state);

/**
 * @brief Reset dimmer controller
 *
 * @return board error code
 */
BoardErr_t
Board_DimmerReset(void);
BoardErr_t
Board_DimmerReset_Part2(void);

/**
 * @brief Set dimmer level
 *
 * @param ch - dimmer channel
 * @param level - dimmer level
 * @param duration - dimmer duration
 * @return board error code
 */
BoardErr_t
Board_DimmerSet(OutputCh_t ch, uint8_t level, uint8_t duration);

/**
 * @brief Get dimmer level
 *
 * @param ch - dimmer channel
 * @param level - dimmer level
 * @param duration - dimmer duration
 * @return board error code
 */
BoardErr_t
Board_DimmerGet(OutputCh_t ch, uint8_t *level, uint8_t *duration);

/**
 * @brief Blinds open
 *
 * @param ch1 - channel 1
 * @param ch2 - channel 2
 * @return board error code
 */
BoardErr_t
Board_BlindsOpen(OutputCh_t ch1, OutputCh_t ch2);

/**
 * @brief Blinds close
 *
 * @param ch1 - channel 1
 * @param ch2 - channel 2
 * @return board error code
 */
BoardErr_t
Board_BlindsClose(OutputCh_t ch1, OutputCh_t ch2);

/**
 * @brief Stop blinds motion
 *
 * @param ch1 - channel 1
 * @param ch2 - channel 2
 * @return board error code
 */
BoardErr_t
Board_BlindsStop(OutputCh_t ch1, OutputCh_t ch2);

/**
 * @brief Activate boiler water heater
 *
 * @param ch1 - channel 1
 * @param ch2 - channel 2
 * @return board error code
 */
BoardErr_t
Board_BoilerActivate(OutputCh_t ch1, OutputCh_t ch2);

/**
 * @brief Deactivateboiler water heater
 *
 * @param ch1 - channel 1
 * @param ch2 - channel 2
 * @return board error code
 */
BoardErr_t
Board_BoilerDeactivate(OutputCh_t ch1, OutputCh_t ch2);
BoardErr_t
Board_FanActivate(OutputCh_t ch1, OutputCh_t ch2,RelayState_t Val_ch1,RelayState_t Val_ch2);

/**
 * @brief Set color to the button LED
 *
 * @param btn - number of button
 * @param color - color
 * @param blinkPeriod - period of blink
 * @return board error code
 */
BoardErr_t
Board_LedSet(BtnNumber_t btn, BacklightColor_t color, uint16_t blinkPeriod);

/**
 * @brief Update changes caused by Board_LedSet()
 */
void
Board_LedUpdate();

/**
 * @brief Board default handler
 */
void
Board_Default_Handler(void);

#endif //!BOARD_DEDICATED_MODULE_H
