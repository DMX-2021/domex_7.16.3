################################################################################
# Automatically-generated file. Do not edit!
################################################################################

S79_SRCS := 
OBJ_SRCS := 
S_SRCS := 
ASM_SRCS := 
C_SRCS := 
S79_UPPER_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
EXECUTABLES := 
OBJS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
Device \
Gecko/platform/common/toolchain/src \
Gecko/platform/driver/i2cspm/src \
Gecko/platform/emdrv/dmadrv/src \
Gecko/platform/emdrv/spidrv/src \
Gecko/platform/emdrv/ustimer/src \
Gecko/platform/emlib/src \
Gecko/platform/service/sleeptimer/src \
Gecko/platform/service/udelay/src \
ZAF_ApplicationUtilities \
ZAF_ApplicationUtilities_Actuator \
ZAF_ApplicationUtilities_EventHandling \
ZAF_ApplicationUtilities_PowerManagement \
ZAF_ApplicationUtilities_TrueStatusEngine \
ZAF_ApplicationUtilities_commonIF \
ZAF_CommandClasses_Association \
ZAF_CommandClasses_Basic \
ZAF_CommandClasses_BinarySwitch \
ZAF_CommandClasses_CentralScene \
ZAF_CommandClasses_Common \
ZAF_CommandClasses_Configuration \
ZAF_CommandClasses_DeviceResetLocally \
ZAF_CommandClasses_FirmwareUpdate \
ZAF_CommandClasses_Indicator \
ZAF_CommandClasses_ManufacturerSpecific \
ZAF_CommandClasses_MultiChan \
ZAF_CommandClasses_MultilevelSwitch \
ZAF_CommandClasses_PowerLevel \
ZAF_CommandClasses_Security \
ZAF_CommandClasses_Supervision \
ZAF_CommandClasses_ZWavePlusInfo \
emdrv \
emlib \
src \
src/BoardDedicatedModule \
src/ConfigurationModule \
src/EndpointModule \
src/SwitchTypes \
src/TouchController \
src/cpt212b \
src/drv2603 \
src/i2c \
src/opt3002 \
src/stm32_dimmer \
src/tca9555 \
src/ws2812b \

